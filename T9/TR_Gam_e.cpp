#include <iostream>
#include <cmath>

const double PI = std::atan(1.0)*4;

using namespace std;
//dies ist ein TR Programm um die partielle leptonenbreite zu bestimmen

int main(){
//Variablen definieren in GeV

	double mZ=91.161;
	double mZ_err=0.0154;
	double Gam_Z=2.55567;
	double Gam_Z_err=0.0402;
	double oZ_had=39.9191*2.58*pow(10,-6);
	double oZ_had_err=0.6063*2.58*pow(10,-6);
	double oZ_e=2.09665*2.58*pow(10,-6);
	double oZ_e_err=0.1180*2.58*pow(10,-6);
	double Gf=1.166*pow(10,-5);
	double alph = (pow(mZ,3)*Gf)/(24*sqrt(2)*PI);
	double alph_err = Gf/(8*sqrt(2)*PI)*mZ*mZ*mZ_err;

//erste Abschaetzung Gamma_e mit Fehler

	double Gam_e_a=pow((pow(Gam_Z,2.)*oZ_e*(pow(mZ,2.)/(12.*PI))),0.5);
	double Gam_e_a_err= Gam_e_a*sqrt(pow(Gam_Z_err/Gam_Z,2)+pow(mZ_err/mZ,2)+pow(oZ_e_err/(2*oZ_e),2));
	cout << "Gam_e_a = " << Gam_e_a << " +/- " << Gam_e_a_err <<"GeV" << endl;

	double Gam_nu = alph*2;
	cout << "Gam_nu= " << Gam_nu << "GeV" << endl;
	double Gam_nu_err=alph_err*2;
//zweite abschaetzung fuer Gamma_e	
	double srt = sqrt(pow(3*Gam_nu-Gam_Z,2)/36-pow(Gam_Z,2)*pow(mZ,2)/(36*PI)*oZ_had);
//	cout <<srt<<endl;
	double Gam_e_b_1 = (Gam_Z-3*Gam_nu)/6+srt;
	double Gam_e_b_2 = (Gam_Z-3*Gam_nu)/6-srt;
	
	double dGam_e_dGam_Z_p=1./6. * ((Gam_Z-3*Gam_nu-Gam_Z*mZ*mZ*oZ_had/PI)/sqrt(pow(Gam_Z-3*Gam_nu,2)-Gam_Z*Gam_Z*mZ*mZ*oZ_had/PI)+1);
	
	double dGam_e_dGam_nu_p=1./6. *((3*(3*Gam_nu-Gam_Z))/sqrt(pow(Gam_Z-3*Gam_nu,2)-Gam_Z*Gam_Z*mZ*mZ*oZ_had/PI)-3);
	
	double dGam_e_dmZ_p = -1./6. *(mZ*oZ_had*Gam_Z*Gam_Z/PI)/sqrt(pow(Gam_Z-3*Gam_nu,2)-Gam_Z*Gam_Z*mZ*mZ*oZ_had/PI);

	double dGam_e_doZ_p=-1./6. *(mZ*mZ*Gam_Z*Gam_Z/PI)/sqrt(pow(Gam_Z-3*Gam_nu,2)-Gam_Z*Gam_Z*mZ*mZ*oZ_had/PI); 
	
	double dGam_e_dGam_Z_m=1./6. * ((-Gam_Z+3*Gam_nu+Gam_Z*mZ*mZ*oZ_had/PI)/sqrt(pow(Gam_Z-3*Gam_nu,2)-Gam_Z*Gam_Z*mZ*mZ*oZ_had/PI)+1);

        double dGam_e_dGam_nu_m=1./6. *((-3*(3*Gam_nu-Gam_Z))/sqrt(pow(Gam_Z-3*Gam_nu,2)-Gam_Z*Gam_Z*mZ*mZ*oZ_had/PI)-3);

        double dGam_e_dmZ_m = 1./6. *(mZ*oZ_had*Gam_Z*Gam_Z/PI)/sqrt(pow(Gam_Z-3*Gam_nu,2)-Gam_Z*Gam_Z*mZ*mZ*oZ_had/PI);

        double dGam_e_doZ_m=1./6. *(mZ*mZ*Gam_Z*Gam_Z/PI)/sqrt(pow(Gam_Z-3*Gam_nu,2)-Gam_Z*Gam_Z*mZ*mZ*oZ_had/PI);



//	double temp1 =1/2-(Gam_Z-(pow(mZ,2)*oZ_had*Gam_Z/(6*PI))-Gam_nu)/srt;
//	cout<< "temp1= "<<temp1 << "Gan_err= "<< Gam_Z_err<< endl;
//	cout<<sqrt(pow(temp1,2)*pow(Gam_Z_err,2))<<endl;
// 	double temp2 =pow(Gam_Z,2)*mZ*oZ_had/(6*PI)/srt;
//	cout<< temp2 <<endl;
	double Gam_e_b_1_err=sqrt(pow(dGam_e_dGam_Z_p*Gam_Z_err,2)+pow(dGam_e_dGam_nu_p*Gam_nu_err,2)+pow(dGam_e_dmZ_p*mZ_err,2)+pow(dGam_e_doZ_p*oZ_had_err,2));
	
	double Gam_e_b_2_err=sqrt(pow(dGam_e_dGam_Z_m*Gam_Z_err,2)+pow(dGam_e_dGam_nu_m*Gam_nu_err,2)+pow(dGam_e_dmZ_m*mZ_err,2)+pow(dGam_e_doZ_m*oZ_had_err,2));

	cout << "Gam_e_b_1= " << Gam_e_b_1<<" +/-" <<Gam_e_b_1_err  << "GeV"<< endl;
	cout << "Gam_e_b_2= " << Gam_e_b_2 <<" +/- " <<Gam_e_b_2_err <<"GeV"<< endl;

//berechnung elektroschwacher mischungswinkel

//mit 1.abschaetzung fuer gam_e
	double sin_t21_a=1./4.*(1+sqrt((24*sqrt(2)*PI*Gam_e_a)/(pow(mZ,3)*Gf)-1));
	
	double sin_t22_a=1./4.*(1-sqrt((24*sqrt(2)*PI*Gam_e_a)/(pow(mZ,3)*Gf)-1));
	
	double prefac_a_sin=24*sqrt(2)*PI*Gam_e_a/(8*mZ*mZ*mZ*Gf*sqrt((24*sqrt(2)*PI*Gam_e_a)/(pow(mZ,3)*Gf)-1));
	double sin_t2_a_err=prefac_a_sin*sqrt(pow(Gam_e_a_err/Gam_e_a,2)+pow(3*mZ_err/mZ,2));	
	
	cout << "sin_21_a= " << sin_t21_a << "+/-"<< sin_t2_a_err<< endl;
	cout << "sin_22_a= " << sin_t22_a << "+/-"<< sin_t2_a_err<< endl;


//mit 2.abschaetzung fuer gam_e



// nicht relevant da gam_e_b_1 unsinn ist
/*
	double sin_t21_b_1=1./4.*(1+sqrt((24*sqrt(2)*PI*Gam_e_b_1)/(pow(mZ,3)*Gf)-1));

        double sin_t22_b_1=1./4.*(1-sqrt((24*sqrt(2)*PI*Gam_e_b_1)/(pow(mZ,3)*Gf)-1));

        double prefac_b_1_sin=24*sqrt(2)*PI*Gam_e_b_1/(8*mZ*mZ*mZ*Gf*sqrt((24*sqrt(2)*PI*Gam_e_b_1)/(pow(mZ,3)*Gf)-1));
        double sin_t2_b_1_err=prefac_b_1_sin*sqrt(pow(Gam_e_b_1_err/Gam_e_b_1,2)+pow(3*mZ_err/mZ,2));
        
        cout << "sin_21_b_1= " << sin_t21_b_1 << "+/-"<< sin_t2_b_1_err<< endl;
        cout << "sin_22_b_1= " << sin_t22_b_1 << "+/-"<< sin_t2_b_1_err<< endl;
*/	
	

// gam_e_b_2 verschieben um 1 sigma da sonst keine loesung
	double sin_t21_b_2=1./4.*(1+sqrt((24*sqrt(2)*PI*(Gam_e_b_2+Gam_e_b_2_err))/(pow(mZ,3)*Gf)-1));

        double sin_t22_b_2=1./4.*(1-sqrt((24*sqrt(2)*PI*(Gam_e_b_2+Gam_e_b_2_err))/(pow(mZ,3)*Gf)-1));

        double prefac_b_2_sin=24*sqrt(2)*PI*(Gam_e_b_2+Gam_e_b_2_err)/(8*mZ*mZ*mZ*Gf*sqrt((24*sqrt(2)*PI*(Gam_e_b_2+Gam_e_b_2_err))/(pow(mZ,3)*Gf)-1));
        double sin_t2_b_2_err=prefac_b_2_sin*sqrt(pow(Gam_e_b_2_err/(Gam_e_b_2+Gam_e_b_2_err),2)+pow(3*mZ_err/mZ,2));
        ;
	cout<<endl;
	cout<<"Gam_e_b_2 um 1 sigma nach oben verschoben da sonst nan"<<endl;
        cout << "sin_21_b_2= " << sin_t21_b_2 << "+/-"<< sin_t2_b_2_err<< endl;
        cout << "sin_22_b_2= " << sin_t22_b_2 << "+/-"<< sin_t2_b_2_err<< endl;



// mit mittelung aus 1 und 2

	double Gam_e_gem=(Gam_e_a/pow(Gam_e_a_err,2) + Gam_e_b_2/pow(Gam_e_b_2_err,2))/(1./pow(Gam_e_a_err,2)+1./pow(Gam_e_b_2_err,2));
	double Gam_e_gem_err=sqrt(1./(1./pow(Gam_e_a_err,2)+1./pow(Gam_e_b_2_err,2)));

	cout << "Gam_e_gem="<<Gam_e_gem<<"+/-"<<Gam_e_gem_err<<endl;

        double sin_t21_gem=1./4.*(1+sqrt((24*sqrt(2)*PI*Gam_e_gem)/(pow(mZ,3)*Gf)-1));

        double sin_t22_gem=1./4.*(1-sqrt((24*sqrt(2)*PI*Gam_e_gem)/(pow(mZ,3)*Gf)-1));

        double prefac_gem_sin=24*sqrt(2)*PI*Gam_e_gem/(8*mZ*mZ*mZ*Gf*sqrt((24*sqrt(2)*PI*Gam_e_gem)/(pow(mZ,3)*Gf)-1));
        double sin_t2_gem_err=prefac_gem_sin*sqrt(pow(Gam_e_gem_err/Gam_e_gem,2)+pow(3*mZ_err/mZ,2));
        ;
        cout << "sin_21_gem= " << sin_t21_gem << "+/-"<< sin_t2_gem_err<< endl;
        cout << "sin_22_gem= " << sin_t22_gem << "+/-"<< sin_t2_gem_err<< endl;



/*
	double sin_t_err=1/(16+Gam_e_a/alph-2)*(pow(Gam_e_a_err/alph,2)+pow(Gam_e_a/alph*alph_err,2));
//	double t_err=cos(t)*sin_t_err*180/PI;
//	cout << "sin(theta)= " << sin_t << " +/- "<<sin_t_err<<endl;
	cout << "theta_w = " << t <<" +/- "<<t_err<< endl;
*/
//	Farbfaktor ueber Gam_had bestimmen
// mit Gam_e_a und sin_a
	double Gam_had_oZ_a=oZ_had*Gam_Z*Gam_Z/Gam_e_a*mZ*mZ/(12*PI);
	double Gam_had_oZ_a_err=Gam_had_oZ_a*sqrt(pow(oZ_had_err/oZ_had,2)+pow(2*mZ_err/mZ,2)+pow(2*Gam_Z_err/Gam_Z,2)+pow(Gam_e_a_err/Gam_e_a,2));	

	double Gam_had_exp_a=Gam_Z-3*Gam_e_a-3*Gam_nu;
	double Gam_had_exp_a_err=sqrt(pow(Gam_Z_err,2)+pow(3*Gam_e_a_err,2)+pow(3*Gam_nu_err,2));	

	double Gam_u_wb1_a=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*(1+pow(1-4*2./3.*sin_t21_a,2));
	double Gam_u_wb1_a_err=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*sqrt(pow(mZ_err/mZ * (1+pow(1-4*2./3.*sin_t21_a,2)),2)+pow(8*2./3.*sin_t2_a_err*(1-4*2./3.*sin_t21_a),2));

	double Gam_u_wb2_a=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*(1+pow(1-4*2./3.*sin_t22_a,2));	
	double Gam_u_wb2_a_err=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*sqrt(pow(mZ_err/mZ * (1+pow(1-4*2./3.*sin_t22_a,2)),2)+pow(8*2./3.*sin_t2_a_err*(1-4*2./3.*sin_t22_a),2));


	double Gam_d_wb1_a=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*(1+pow(1-4*1./3.*sin_t21_a,2));
	double Gam_d_wb1_a_err=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*sqrt(pow(mZ_err/mZ * (1+pow(1-4*1./3.*sin_t21_a,2)),2)+pow(8*1./3.*sin_t2_a_err*(1-4*1./3.*sin_t21_a),2));


	double Gam_d_wb2_a=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*(1+pow(1-4*1./3.*sin_t22_a,2));
	double Gam_d_wb2_a_err=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*sqrt(pow(mZ_err/mZ * (1+pow(1-4*1./3.*sin_t22_a,2)),2)+pow(8*1./3.*sin_t2_a_err*(1-4*1./3.*sin_t22_a),2));



	double Gam_had_wb1_a=2*Gam_u_wb1_a+3*Gam_d_wb1_a;
	double Gam_had_wb1_a_err=sqrt(pow(2*Gam_u_wb1_a_err,2)+pow(3*Gam_d_wb1_a_err,2));

	double Gam_had_wb2_a=2*Gam_u_wb2_a+3*Gam_d_wb2_a;
	double Gam_had_wb2_a_err=sqrt(pow(2*Gam_u_wb2_a_err,2)+pow(3*Gam_d_wb2_a_err,2));


	double K=1.04;

	double Nc_oZ1_a =Gam_had_oZ_a/Gam_had_wb1_a * 1./K;
	double Nc_oZ1_a_err=Nc_oZ1_a*sqrt(pow(Gam_had_oZ_a_err/Gam_had_oZ_a,2)+pow(Gam_had_wb1_a_err/Gam_had_wb1_a,2));
	
        double Nc_oZ2_a = Gam_had_oZ_a/Gam_had_wb2_a * 1./K;
        double Nc_oZ2_a_err=Nc_oZ2_a*sqrt(pow(Gam_had_oZ_a_err/Gam_had_oZ_a,2)+pow(Gam_had_wb2_a_err/Gam_had_wb2_a,2));


	double Nc_wb1_a = Gam_had_exp_a/Gam_had_wb1_a *1./K;
	double Nc_wb1_a_err=Nc_wb1_a*sqrt(pow(Gam_had_wb1_a_err/Gam_had_wb1_a,2)+pow(Gam_had_exp_a_err/Gam_had_exp_a,2));

	double Nc_wb2_a = Gam_had_exp_a/Gam_had_wb2_a * 1./K;
	double Nc_wb2_a_err=Nc_wb2_a*sqrt(pow(Gam_had_wb2_a_err/Gam_had_wb2_a,2)+pow(Gam_had_exp_a_err/Gam_had_exp_a,2));

	cout << "Nc_oZ1_a= " << Nc_oZ1_a<< "+/-" <<Nc_oZ1_a_err<< endl;
        cout << "Nc_oZ2_a= " << Nc_oZ2_a<< "+/-" <<Nc_oZ2_a_err<< endl;



	cout << "Nc_wb1_a= " << Nc_wb1_a<< "+/-" <<Nc_wb1_a_err <<endl;
	cout << "Nc_wb2_a= " << Nc_wb2_a<< "+/-" <<Nc_wb2_a_err<< endl;

// mit (Gam_e_b_2+Gam_e_b_2_err) und sin_b_2
        double Gam_had_oZ_b_2=oZ_had*Gam_Z*Gam_Z/(Gam_e_b_2+Gam_e_b_2_err)*mZ*mZ/(12*PI);
        double Gam_had_oZ_b_2_err=Gam_had_oZ_b_2*sqrt(pow(oZ_had_err/oZ_had,2)+pow(2*mZ_err/mZ,2)+pow(2*Gam_Z_err/Gam_Z,2)+pow(Gam_e_b_2_err/(Gam_e_b_2+Gam_e_b_2_err),2));

        double Gam_had_exp_b_2=Gam_Z-3*(Gam_e_b_2+Gam_e_b_2_err)-3*Gam_nu;
        double Gam_had_exp_b_2_err=sqrt(pow(Gam_Z_err,2)+pow(3*Gam_e_b_2_err,2)+pow(3*Gam_nu_err,2));

        double Gam_u_wb1_b_2=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*(1+pow(1-4*2./3.*sin_t21_b_2,2));
        double Gam_u_wb1_b_2_err=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*sqrt(pow(mZ_err/mZ * (1+pow(1-4*2./3.*sin_t21_b_2,2)),2)+pow(8*2./3.*sin_t2_b_2_err*(1-4*2./3.*sin_t21_b_2),2));

        double Gam_u_wb2_b_2=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*(1+pow(1-4*2./3.*sin_t22_b_2,2));
        double Gam_u_wb2_b_2_err=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*sqrt(pow(mZ_err/mZ * (1+pow(1-4*2./3.*sin_t22_b_2,2)),2)+pow(8*2./3.*sin_t2_b_2_err*(1-4*2./3.*sin_t22_b_2),2));


        double Gam_d_wb1_b_2=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*(1+pow(1-4*1./3.*sin_t21_b_2,2));
        double Gam_d_wb1_b_2_err=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*sqrt(pow(mZ_err/mZ * (1+pow(1-4*1./3.*sin_t21_b_2,2)),2)+pow(8*1./3.*sin_t2_b_2_err*(1-4*1./3.*sin_t21_b_2),2));


        double Gam_d_wb2_b_2=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*(1+pow(1-4*1./3.*sin_t22_b_2,2));
        double Gam_d_wb2_b_2_err=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*sqrt(pow(mZ_err/mZ * (1+pow(1-4*1./3.*sin_t22_b_2,2)),2)+pow(8*1./3.*sin_t2_b_2_err*(1-4*1./3.*sin_t22_b_2),2));



        double Gam_had_wb1_b_2=2*Gam_u_wb1_b_2+3*Gam_d_wb1_b_2;
        double Gam_had_wb1_b_2_err=sqrt(pow(2*Gam_u_wb1_b_2_err,2)+pow(3*Gam_d_wb1_b_2_err,2));

        double Gam_had_wb2_b_2=2*Gam_u_wb2_b_2+3*Gam_d_wb2_b_2;
        double Gam_had_wb2_b_2_err=sqrt(pow(2*Gam_u_wb2_b_2_err,2)+pow(3*Gam_d_wb2_b_2_err,2));


//        double K=1.04;

        double Nc_oZ1_b_2 =Gam_had_oZ_b_2/Gam_had_wb1_b_2 * 1./K;
        double Nc_oZ1_b_2_err=Nc_oZ1_b_2*sqrt(pow(Gam_had_oZ_b_2_err/Gam_had_oZ_b_2,2)+pow(Gam_had_wb1_b_2_err/Gam_had_wb1_b_2,2));

        double Nc_oZ2_b_2 = Gam_had_oZ_b_2/Gam_had_wb2_b_2 * 1./K;
        double Nc_oZ2_b_2_err=Nc_oZ2_b_2*sqrt(pow(Gam_had_oZ_b_2_err/Gam_had_oZ_b_2,2)+pow(Gam_had_wb2_b_2_err/Gam_had_wb2_b_2,2));


        double Nc_wb1_b_2 = Gam_had_exp_b_2/Gam_had_wb1_b_2 *1./K;
        double Nc_wb1_b_2_err=Nc_wb1_b_2*sqrt(pow(Gam_had_wb1_b_2_err/Gam_had_wb1_b_2,2)+pow(Gam_had_exp_b_2_err/Gam_had_exp_b_2,2));

        double Nc_wb2_b_2 = Gam_had_exp_b_2/Gam_had_wb2_b_2 * 1./K;
        double Nc_wb2_b_2_err=Nc_wb2_b_2*sqrt(pow(Gam_had_wb2_b_2_err/Gam_had_wb2_b_2,2)+pow(Gam_had_exp_b_2_err/Gam_had_exp_b_2,2));

        cout << "Nc_oZ1_b_2= " << Nc_oZ1_b_2<< "+/-" <<Nc_oZ1_b_2_err<< endl;
	cout << "Nc_oZ2_b_2= " << Nc_oZ2_b_2<< "+/-" <<Nc_oZ2_b_2_err<< endl;


        cout << "Nc_wb1_b_2= " << Nc_wb1_b_2<< "+/-" <<Nc_wb1_b_2_err <<endl;
        cout << "Nc_wb2_b_2= " << Nc_wb2_b_2<< "+/-" <<Nc_wb2_b_2_err<< endl;




// mit Gam_e_gem und sin_gem
        double Gam_had_oZ_gem=oZ_had*Gam_Z*Gam_Z/Gam_e_gem*mZ*mZ/(12*PI);
        double Gam_had_oZ_gem_err=Gam_had_oZ_gem*sqrt(pow(oZ_had_err/oZ_had,2)+pow(2*mZ_err/mZ,2)+pow(2*Gam_Z_err/Gam_Z,2)+pow(Gam_e_gem_err/Gam_e_gem,2));

        double Gam_had_exp_gem=Gam_Z-3*Gam_e_gem-3*Gam_nu;
        double Gam_had_exp_gem_err=sqrt(pow(Gam_Z_err,2)+pow(3*Gam_e_gem_err,2)+pow(3*Gam_nu_err,2));

        double Gam_u_wb1_gem=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*(1+pow(1-4*2./3.*sin_t21_gem,2));
        double Gam_u_wb1_gem_err=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*sqrt(pow(mZ_err/mZ * (1+pow(1-4*2./3.*sin_t21_gem,2)),2)+pow(8*2./3.*sin_t2_gem_err*(1-4*2./3.*sin_t21_gem),2));

        double Gam_u_wb2_gem=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*(1+pow(1-4*2./3.*sin_t22_gem,2));
        double Gam_u_wb2_gem_err=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*sqrt(pow(mZ_err/mZ * (1+pow(1-4*2./3.*sin_t22_gem,2)),2)+pow(8*2./3.*sin_t2_gem_err*(1-4*2./3.*sin_t22_gem),2));


        double Gam_d_wb1_gem=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*(1+pow(1-4*1./3.*sin_t21_gem,2));
        double Gam_d_wb1_gem_err=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*sqrt(pow(mZ_err/mZ * (1+pow(1-4*1./3.*sin_t21_gem,2)),2)+pow(8*1./3.*sin_t2_gem_err*(1-4*1./3.*sin_t21_gem),2));


        double Gam_d_wb2_gem=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*(1+pow(1-4*1./3.*sin_t22_gem,2));
        double Gam_d_wb2_gem_err=Gf*pow(mZ,3)/(24*sqrt(2)*PI)*sqrt(pow(mZ_err/mZ * (1+pow(1-4*1./3.*sin_t22_gem,2)),2)+pow(8*1./3.*sin_t2_gem_err*(1-4*1./3.*sin_t22_gem),2));



        double Gam_had_wb1_gem=2*Gam_u_wb1_gem+3*Gam_d_wb1_gem;
        double Gam_had_wb1_gem_err=sqrt(pow(2*Gam_u_wb1_gem_err,2)+pow(3*Gam_d_wb1_gem_err,2));

        double Gam_had_wb2_gem=2*Gam_u_wb2_gem+3*Gam_d_wb2_gem;
        double Gam_had_wb2_gem_err=sqrt(pow(2*Gam_u_wb2_gem_err,2)+pow(3*Gam_d_wb2_gem_err,2));


//        double K=1.04;

        double Nc_oZ1_gem = Gam_had_oZ_gem/Gam_had_wb1_gem * 1./K;
        double Nc_oZ1_gem_err=Nc_oZ1_gem*sqrt(pow(Gam_had_oZ_gem_err/Gam_had_oZ_gem,2)+pow(Gam_had_wb1_gem_err/Gam_had_wb1_gem,2));

	double Nc_oZ2_gem = Gam_had_oZ_gem/Gam_had_wb2_gem * 1./K;
        double Nc_oZ2_gem_err=Nc_oZ2_gem*sqrt(pow(Gam_had_oZ_gem_err/Gam_had_oZ_gem,2)+pow(Gam_had_wb2_gem_err/Gam_had_wb2_gem,2));



        double Nc_exp1_gem = Gam_had_exp_gem/Gam_had_wb1_gem *1./K;
        double Nc_exp1_gem_err=Nc_exp1_gem*sqrt(pow(Gam_had_wb1_gem_err/Gam_had_wb1_gem,2)+pow(Gam_had_exp_gem_err/Gam_had_exp_gem,2));

        double Nc_exp2_gem = Gam_had_exp_gem/Gam_had_wb2_gem * 1./K;
        double Nc_exp2_gem_err=Nc_exp2_gem*sqrt(pow(Gam_had_wb2_gem_err/Gam_had_wb2_gem,2)+pow(Gam_had_exp_gem_err/Gam_had_exp_gem,2));
	
	cout <<endl<< "Gam_had_oZ_gem="<< Gam_had_oZ_gem<<"+/-"<<Gam_had_oZ_gem_err<<endl;
	cout << "Gam_had_exp_gem="<< Gam_had_exp_gem<<"+/-"<<Gam_had_exp_gem_err<<endl;
	cout << "Gam_had_wb2_gem="<< Gam_had_wb2_gem<<"+/-"<<Gam_had_wb2_gem_err<<endl;
	cout << "Gam_nu="<<Gam_nu<<"+/-"<<Gam_nu_err<<endl;
	cout << "Gam_e_gem="<<Gam_e_gem<<"+/-"<<Gam_e_gem_err<<endl;


        cout <<endl<< "Nc_oZ1_gem= " << Nc_oZ1_gem<< "+/-" <<Nc_oZ1_gem_err<< endl;
	cout << "Nc_oZ2_gem= " << Nc_oZ2_gem<< "+/-" <<Nc_oZ2_gem_err<< endl;

        cout << "Nc_exp1_gem= " << Nc_exp1_gem<< "+/-" <<Nc_exp1_gem_err <<endl;
        cout << "Nc_exp2_gem= " << Nc_exp2_gem<< "+/-" <<Nc_exp2_gem_err<< endl;









//	Fehler auf Farbfaktor
/*
	double e1= pow(Gam_Z,2)/Gam_e_a*pow(mZ,2)/(12*PI);
	double e2= 2*oZ_had*Gam_Z/Gam_e_a*pow(mZ,2)/(12*PI);
	double e3= oZ_had*pow(Gam_Z,2)/pow(Gam_e_a,2)*pow(mZ,2)/(12*PI);
	double e4= 2*pow(Gam_Z,2)/Gam_e_a*mZ/(12*PI);
	double Gam_had_1_err = e1*e1*pow(oZ_had_err,2)+e2*e2*pow(Gam_Z_err,2)+
						e3*e3*pow(Gam_e_a_err,2)+e4*e4*pow(mZ_err,2);
	Gam_had_1_err=sqrt(Gam_had_1_err);
	cout << Gam_had_1_err<<endl;
	double Gam_had_2_err=pow(Gam_Z_err,2)+pow(Gam_e_a_err,2)+pow(Gam_nu_err,2);
	Gam_had_2_err=sqrt(Gam_had_2_err);
	cout << Gam_had_2 << endl;
	double Nc_err=Nc*sqrt(pow(Gam_had_1_err/Gam_had_1,2)+pow(Gam_had_2_err/Gam_had_2,2));
	Nc_err=sqrt(Nc_err);
	cout <<"sig Nc= "<<Nc_err << endl;
*/	

return 0;
}
