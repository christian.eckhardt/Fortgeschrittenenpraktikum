{
//
//  T. Hebbeker 1.0  2004-10-07
//              1.1  2008-03-26   print in different formats
//                                remove labeling x and y axes
//
// start this macro in root with   .x  ./plot.C
//

// say hello

   cout << endl 
        << " >>> plot.C <<<  version 1.1  T.H.  2008-03-26 " 
        << endl << endl;

// some general options 
   gStyle->SetOptStat(0);
   gStyle->SetOptFit(1);
// open frame, white color
   canvas1 = new TCanvas("canvas1", "plot");
   canvas1->SetFillColor(10);
   text1 =new TText(40.,0.05,"L3");
   text1->SetTextColor(2);
   text1->Draw();

   text2 = new TLatex(12.6,0.05,"Monte Carlo");
   text2->SetTextColor(3);
   text2->Draw();


// define input root file with L3 data 
//   file_L3 = new TFile("91gev_n.root");
//	file_L3=new TFile("91mu.root");
   file_L3=new TFile("91_cos.root");
// plot data histogram, after normalization, with some options
   N_histo->SetNormFactor(1.);
   N_histo->SetMarkerStyle(20);
   N_histo->SetMarkerSize(1.0);
   N_histo->SetMarkerColor(2);
   N_histo->Draw("P"); 
  
// define input root file with Monte Carlo
   file_MC = new TFile("mu_cos.root");
//   file_MC= new TFile("hadrons_n.root");
// plot MC histogram, after normalization, with some options
   N_histo->SetNormFactor(1.);
   N_histo->SetLineColor(3);
   N_histo->SetLineWidth(2);
   N_histo->Draw("same"); 

// add some text

//
//   texty = new TLatex(-58.,0.20,"number of muons");
//   texty->SetTextAngle(90.); 
//   texty->Draw();
//
//   textx = new TLatex(32.3,-0.04,"x / GeV");
//   textx->Draw();
//

   
// print plot as jpg file
   canvas1->Print("plot_n.jpg");
   canvas1->Print("plot_n.gif");
   canvas1->Print("plot_n.png");


//
//// open frame, white color
   canvas2 = new TCanvas("canvas2", "Energie");
   canvas2->SetFillColor(10);
//
//// define input root file with L3 data 
   file_L4 = new TFile("91_mu.root");
//
//// plot data histogram, after normalization, with some options
   E_histo->SetNormFactor(1.);
   E_histo->SetMarkerStyle(20);
   E_histo->SetMarkerSize(1.0);
   E_histo->SetMarkerColor(2);
   E_histo->Draw("P"); 
//
//// define input root file with Monte Carlo
   file_MC2 = new TFile("muons.root");
//
// plot MC histogram, after normalization, with some options
   E_histo->SetNormFactor(1.);
   E_histo->SetLineColor(3);
   E_histo->SetLineWidth(2);
   E_histo->Draw("same"); 
//
//// add some text
//
////
//   texty = new TLatex(-58.,0.20,"number of muons");
//   texty->SetTextAngle(90.); 
////   texty->Draw();
////
////   textx = new TLatex(32.3,-0.04,"x / GeV");
////   textx->Draw();
////
//
   text3 = new TLatex(11.2,0.35,"L3");
   text3->SetTextColor(2);
   text3->Draw();
//
   text4 = new TLatex(12.6,0.2,"Monte Carlo");
   text4->SetTextColor(3);
   text4->Draw();
//
//// print plot as jpg file
    canvas2->Print("plot_e.jpg");
       canvas2->Print("plot_e.gif");
          canvas2->Print("plot_e.png");
//
//

}
