#include <iostream>
#include "read_mca.h"
#include <cmath>

using namespace std;

//n = Anzahl Werte
double* RMS(int n, double* vals){
	double mean=0;
	double var=0;
	for(int i=0;i<n;i++) mean+=vals[i];
	mean=mean/(n+0.0);
	for(int i=0;i<n;i++)var+=pow((vals[i]-mean),2);
	var=var/(n-1.0);
	double* toret = new double[2];
	toret[0]=mean;
	toret[1]=var;
	return toret;
}

double RMS(int n, double* vals, double mean){
	double var=0;
	double sig=0;
	for(int i=0;i<n;i++) var+=pow((vals[i]-mean),2);
	var=var/(n+0.0);
	sig=pow(var,0.5);
	return sig;
}

//Funktion berechnet aus ergebnissen des T9 programms den sysfehler auf sigma
double sig_sys(int n,double* ns_o, double* ns_M, double n_o){
	double L=134.4;//Luminositaet bei 91,33 GeV
	double mean=1/L*n_o/ns_M[0]*ns_o[0];//Mittelwert - originalwerte stehen im 0. Element
	cout << "sig mean = " << mean << endl;
	double* sigs=new double[n-1];
	for(int i=1; i<n; i++){
		sigs[i-1]=1/L*n_o/ns_M[i]*ns_o[i];//sigmas ausrechnen und dann mit RMS sigma bestimmen
		cout << sigs[i] << endl;
	}
	double sys=0;
	sys=RMS(n-1,sigs,mean);
	cout<<"sys= "<<sys<<endl;
	return sys;
}

int main(){
/*
	double n_o=9969;
	int n = int(read_mca("ns_o")[0]);
	double* ns_o = &read_mca("ns_o")[1];
	double* ns_M = &read_mca("ns_M")[1];
	//for(int i=0;i<n;i++) cout << ns_o[i] << " " << ns_M[i] << " " << endl;
	double sig_xsec = sig_sys(n, ns_o, ns_M, n_o);
	cout <<"std abweichung sigmas: "<<sig_xsec<<endl;
*/

	int n1 = int(read_mca("ms")[0]);
	double m = read_mca("ms")[1];
	double* ms = &read_mca("ms")[2];
	double sig_m = RMS(n1-1, ms, m);
	cout <<"std abweichung m: "<<sig_m<<endl;

	double sigh = read_mca("sigs2")[1];
	double* sighs = &read_mca("sigs2")[2];
	double sig_sig = RMS(n1-1, sighs, sigh);
	cout <<"std abweichung sig_sig: "<<sig_sig<<endl;

	double Gam_Z = read_mca("Gams_Z")[1];
	double* Gam_Zs = &read_mca("Gams_Z")[2];
	double sig_Gam_Z = RMS(n1-1, Gam_Zs, Gam_Z);
	cout <<"std abweichung Gam_Z: "<<sig_Gam_Z<<endl;
/*
	double x1 = read_mca("xsec")[1];
	double* xs = &read_mca("xsec")[2];
	double sig_x = RMS(n1-1, xs, x1);
	cout <<"std abweichung sig_mu: "<<sig_x<<endl;
*/
	double x2 = read_mca("xsec_var")[1];
	double* xvs = &read_mca("xsec_var")[2];
	double sig_xv = RMS(n1-1, xvs, x2);
	cout <<"std abweichung sig_mu_var: "<<sig_xv<<endl;

	return 0;
}

