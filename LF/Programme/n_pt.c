#include "TROOT.h"
#include "read_mca.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TLegend.h"
#include "TLine.h"
#include <vector>
#include "TMath.h"
void n_pt(){

        gStyle->SetOptFit(1);

        double* Stickstoff=read_mca("../Daten/Stickstoff.txt");
        double* Pt_params=read_mca("../Daten/kalib_params_Pt");

        double p0=Pt_params[1];
        double p0_err=Pt_params[2];
        double p1=Pt_params[3];
        double p1_err=Pt_params[4];


        vector<double> temp_Cu_N;
        vector<double> temp_Ta_N;
        vector<double> temp_Si_N;
        vector<double> temp_Ko_N;
        vector<double> temp_Pt_N;

        for(int i=1;i<=Stickstoff[0];i++){
                if(i%7==1){
                        temp_Pt_N.push_back(Stickstoff[i]);
                }


                if(i%7==2){
                        temp_Ko_N.push_back(Stickstoff[i]);

                }

                if(i%7==3){
                        temp_Cu_N.push_back(Stickstoff[i]);
                }

                if(i%7==4){
                        temp_Ta_N.push_back(Stickstoff[i]);
                }
                if(i%7==5){
                        temp_Si_N.push_back(Stickstoff[i]);
                }
        }

        int size=(temp_Cu_N.size()-1)/4;
        double* Cu_N = new double[size];
        double* Ta_N = new double[size];
        double* Si_N = new double[size];
        double* Ko_N = new double[size];
        double* Pt_N = new double[size];



        for(int i=0;i<=temp_Cu_N.size()-2;i=i+4){
                Cu_N[i/4]=(temp_Cu_N[i]+temp_Cu_N[i+1]+temp_Cu_N[i+2]+temp_Cu_N[i+3])/4;
                Ta_N[i/4]=(temp_Ta_N[i]+temp_Ta_N[i+1]+temp_Ta_N[i+2]+temp_Ta_N[i+3])/4;
                Si_N[i/4]=(temp_Si_N[i]+temp_Si_N[i+1]+temp_Si_N[i+2]+temp_Si_N[i+3])/4;
                Ko_N[i/4]=(temp_Ko_N[i]+temp_Ko_N[i+1]+temp_Ko_N[i+2]+temp_Ko_N[i+3])/4;
                Pt_N[i/4]=(temp_Pt_N[i]+temp_Pt_N[i+1]+temp_Pt_N[i+2]+temp_Pt_N[i+3])/4;
        }



        double* Cu_N_err = new double[size];
        double* Ta_N_err = new double[size];
        double* Si_N_err = new double[size];
        double* Ko_N_err = new double[size];
        double* Pt_N_err = new double[size];
        int n_points=0;


        for(int i=0;i<=temp_Cu_N.size()-2;i=i+4){
                Cu_N_err[i/4]=sqrt((pow(Cu_N[i/4]-temp_Cu_N[i],2)+pow(Cu_N[i/4]-temp_Cu_N[i+1],2)+pow(Cu_N[i/4]-temp_Cu_N[i+2],2)+pow(Cu_N[i/4]-temp_Cu_N[i+3],2))/3);
                Ta_N_err[i/4]=sqrt((pow(Ta_N[i/4]-temp_Ta_N[i],2)+pow(Ta_N[i/4]-temp_Ta_N[i+1],2)+pow(Ta_N[i/4]-temp_Ta_N[i+2],2)+pow(Ta_N[i/4]-temp_Ta_N[i+3],2))/3);
                Si_N_err[i/4]=sqrt((pow(Si_N[i/4]-temp_Si_N[i],2)+pow(Si_N[i/4]-temp_Si_N[i+1],2)+pow(Si_N[i/4]-temp_Si_N[i+2],2)+pow(Si_N[i/4]-temp_Si_N[i+3],2))/3);
                Ko_N_err[i/4]=sqrt((pow(Ko_N[i/4]-temp_Ko_N[i],2)+pow(Ko_N[i/4]-temp_Ko_N[i+1],2)+pow(Ko_N[i/4]-temp_Ko_N[i+2],2)+pow(Ko_N[i/4]-temp_Ko_N[i+3],2))/3);
                Pt_N_err[i/4]=sqrt((pow(Pt_N[i/4]-temp_Pt_N[i],2)+pow(Pt_N[i/4]-temp_Pt_N[i+1],2)+pow(Pt_N[i/4]-temp_Pt_N[i+2],2)+pow(Pt_N[i/4]-temp_Pt_N[i+3],2))/3);
                n_points++;

        }


        double* T=new double[n_points];
        double* ln_T=new double[n_points];

        double* Cu=new double[n_points];
        double* Cu_err=new double[n_points];

        double* Ta=new double[n_points];
        double* Ta_err=new double[n_points];

        double* ln_Cu=new double[n_points];
        double* ln_Cu_err=new double[n_points];

        double* ln_Ta=new double[n_points];
        double* ln_Ta_err=new double[n_points];

        double* Si=new double[n_points];
        double* Si_err=new double[n_points];


        double* T_err=new double[n_points];
        double* ln_T_err=new double[n_points];




        int Si_counter=0;
        for(int i=0;i<n_points;i++){

                //if(){
                        T[i]=(Pt_N[i]-p0)/p1;//+sqrt(pow(p0_err/p1,2)+pow(p1_err*(Pt_N[i]-p0)/(p1*p1),2));
                        T_err[i]=Pt_N_err[i]/p1 ;
                        ln_T[i]=log(T[i]);
                        ln_T_err[i]=T_err[i]/T[i];

                Cu[i]=Cu_N[i];
                Cu_err[i]=Cu_N_err[i];

                ln_Cu[i]=log(Cu_N[i]);
                ln_Cu_err[i]=Cu_N_err[i]/Cu_N[i];

                Ta[i]=Ta_N[i];
                Ta_err[i]=Ta_N_err[i];

                ln_Ta[i]=log(Ta_N[i]);
                ln_Ta_err[i]=Ta_N_err[i]/Ta_N[i];

                Si[i]=Si_N[i];
                Si_err[i]=Si_N_err[i];


                if(Si[i]<1e10){Si_counter++;}

        }

        TGraphErrors rawdata_Cu(n_points,T,Cu,T_err,Cu_err);
	rawdata_Cu.SetTitle("Rawdata of R_{Cu} with platinum (N);T [K]; R_{Cu} [#Omega]");
	rawdata_Cu.SetMarkerStyle(8);
	rawdata_Cu.SetMarkerSize(0.7);


        TGraphErrors lin_Cu(n_points,T,Cu,T_err,Cu_err);
	lin_Cu.SetTitle("Linear Fit;T [K]; R_{Cu} [#Omega]");
        lin_Cu.GetXaxis()->SetLimits(79.,300.);
	lin_Cu.SetMarkerStyle(8);
	lin_Cu.SetMarkerSize(0.7);



        TCanvas* c_Cu=new TCanvas("Rawdata Cu_N_Pt","Rawdata Cu_N_Pt",900,600);

        rawdata_Cu.DrawClone("APE");


        TCanvas* c_Cu_lin=new TCanvas("Linear Fit of Cu (platinum)","Linear Fit of Cu (platinum)",900,600);


        c_Cu_lin->Divide(1,2,0,0);
        c_Cu_lin->cd(1);
        c_Cu_lin->GetPad(1)->SetPad(0.,0.4,1.,1.);

        c_Cu_lin->GetPad(1)->SetBottomMargin(0.012);
        c_Cu_lin->GetPad(1)->SetTopMargin(0.07);
        c_Cu_lin->GetPad(1)->SetRightMargin(0.018);
 //       c_Cu_lin->GetPad(1)->SetLeftMargin(0.08);

        lin_Cu.GetYaxis()->SetLabelSize(0.052);
        lin_Cu.GetYaxis()->SetTitleSize(0.052);
        lin_Cu.GetYaxis()->SetTitleOffset(0.8);

        TF1* f_lin_Cu=new TF1("Linear Law","[0]*(1+(x-273.15)*[1])",77.,300.0);
        f_lin_Cu->SetParameter(0,20);
        f_lin_Cu->SetParameter(1,0.004);


        auto Fitpara_lin_Cu=lin_Cu.Fit(f_lin_Cu,"S","q",80.,300.);


        lin_Cu.DrawClone("APE");
        TLegend* leg_lin_Cu= new TLegend(0.1,0.6,0.4,0.9);
        leg_lin_Cu->AddEntry("lin_Cu","Data","EP");
        leg_lin_Cu->AddEntry(f_lin_Cu,"Fit: p0*[1+p1*(T-273.15K)]","L");
        leg_lin_Cu->SetFillColor(0);
        leg_lin_Cu->Draw("same");

        double res_Cu_lin[n_points];
        double res_Cu_lin_err[n_points];

        for (int i=0;i<n_points;i++){
                res_Cu_lin[i]=Cu[i]-f_lin_Cu->Eval(T[i]);
                res_Cu_lin_err[i]=sqrt(pow(Cu_err[i],2)+pow(T_err[i]*f_lin_Cu->GetParameter(1)*f_lin_Cu->GetParameter(0),2));
        }


        c_Cu_lin->cd();
        c_Cu_lin->cd(2);

        c_Cu_lin->GetPad(2)->SetPad(0.,0.,1.,0.4);
        TGraphErrors res_lin_Cu(n_points,T,res_Cu_lin,nullptr,res_Cu_lin_err);

        res_lin_Cu.SetTitle("");
        res_lin_Cu.GetXaxis()->SetLimits(79.,300.);

        c_Cu_lin->GetPad(2)->SetGridy();
        res_lin_Cu.GetXaxis()->SetLabelSize(0.08);
        res_lin_Cu.GetXaxis()->SetTitle("T [K]");
        res_lin_Cu.GetXaxis()->SetTitleSize(0.08);
        res_lin_Cu.GetYaxis()->SetTitle("Residuum");
        res_lin_Cu.GetYaxis()->SetLabelSize(0.08);
        res_lin_Cu.GetYaxis()->SetTitleSize(0.08);
        res_lin_Cu.GetYaxis()->SetTitleOffset(0.4);
        res_lin_Cu.SetMarkerStyle(8);
        res_lin_Cu.SetMarkerSize(0.4);
        c_Cu_lin->GetPad(2)->SetFrameFillColor(0);
        c_Cu_lin->GetPad(2)->SetFrameBorderMode(0);
        c_Cu_lin->GetPad(2)->SetBottomMargin(0.17);
        c_Cu_lin->GetPad(2)->SetTopMargin(0.04);
	c_Cu_lin->GetPad(2)->SetRightMargin(0.018);
        res_lin_Cu.DrawClone("APE");
        TLine *line = new TLine(80,0,300,0);
        line->SetLineColor(kRed);
        line->Draw("same");
        TLegend* leg_lin_Cu_2= new TLegend(0.1,0.8,0.3,0.95);
        leg_lin_Cu_2->AddEntry("res_lin_Cu","Residuum+Fehler","EP");
        leg_lin_Cu_2->SetFillColor(0);
        leg_lin_Cu_2->Draw("same");




/*
        TGraphErrors data_ln_Cu(n_points,ln_T,ln_Cu,ln_T_err,ln_Cu_err);


        TCanvas* c_ln_Cu=new TCanvas("ln(Cu)-ln(T)","double logarithm",900,600);

        TF1* f_beta_Cu=new TF1("linlaw","[1]*x+[0]",3.5,4.5);
        f_beta_Cu->SetParameter(0,-10);
        f_beta_Cu->SetParameter(1,5);
        auto Fitpara_beta_Cu=data_ln_Cu.Fit(f_beta_Cu,"S","q",3.5,4.);

        data_ln_Cu.DrawClone("APE");

*/
        TGraphErrors rawdata_Ta(n_points,T,Ta,T_err,Ta_err);
	rawdata_Ta.SetMarkerStyle(8);
        rawdata_Ta.SetMarkerSize(0.7);


	rawdata_Ta.SetTitle("Rawdata of R_{Ta} with platinum (N);T [K];R_{Ta} [#Omega]");


        TGraphErrors lin_Ta(n_points,T,Ta,T_err,Ta_err);
        lin_Ta.SetTitle("Linear Fit;T [K]; R_{Ta} [#Omega]");
        lin_Ta.GetXaxis()->SetLimits(79.,300.);
	lin_Ta.SetMarkerStyle(8);
	lin_Ta.SetMarkerSize(0.7);

        TCanvas* c_Ta=new TCanvas("Rawdata Ta_N_Pt","Rawdata Ta_N_Pt",900,600);

        rawdata_Ta.DrawClone("APE");
        
	
        TCanvas* c_Ta_lin= new TCanvas("Linear Fit of Ta (platinum)","Linear Fit of Ta (platinum)",900,600);
        c_Ta_lin->Divide(1,2,0,0);
        c_Ta_lin->cd(1);
        c_Ta_lin->GetPad(1)->SetPad(0.,0.4,1.,1.);

        c_Ta_lin->GetPad(1)->SetBottomMargin(0.012);
        c_Ta_lin->GetPad(1)->SetTopMargin(0.07);
        c_Ta_lin->GetPad(1)->SetRightMargin(0.018);
 //       c_Ta_lin->GetPad(1)->SetLeftMargin(0.08);

        lin_Ta.GetYaxis()->SetLabelSize(0.052);
        lin_Ta.GetYaxis()->SetTitleSize(0.052);
        lin_Ta.GetYaxis()->SetTitleOffset(0.8);

	
	TF1* f_lin_Ta=new TF1("Linear Law","[0]*(1+(x-273.15)*[1])",77.,300.0);
        f_lin_Ta->SetParameter(0,54);
        f_lin_Ta->SetParameter(1,0.004);


        auto Fitpara_lin_Ta=lin_Ta.Fit(f_lin_Ta,"S","q",80.,300.);

	lin_Ta.DrawClone("APE");

        TLegend* leg_lin_Ta= new TLegend(0.1,0.6,0.4,0.9);
        leg_lin_Ta->AddEntry("lin_Ta","Data","EP");
        leg_lin_Ta->AddEntry(f_lin_Ta,"Fit: p0*[1+p1*(T-273.15K)]","L");
        leg_lin_Ta->SetFillColor(0);
        leg_lin_Ta->Draw("same");

        double res_Ta_lin[n_points];
        double res_Ta_lin_err[n_points];

        for (int i=0;i<n_points;i++){
                res_Ta_lin[i]=Ta[i]-f_lin_Ta->Eval(T[i]);
                res_Ta_lin_err[i]=sqrt(pow(Ta_err[i],2)+pow(T_err[i]*f_lin_Ta->GetParameter(1)*f_lin_Ta->GetParameter(0),2));
        }


        c_Ta_lin->cd();
        c_Ta_lin->cd(2);

        c_Ta_lin->GetPad(2)->SetPad(0.,0.,1.,0.4);
        TGraphErrors res_lin_Ta(n_points,T,res_Ta_lin,nullptr,res_Ta_lin_err);

        res_lin_Ta.SetTitle("");
        res_lin_Ta.GetXaxis()->SetLimits(80.,300.);

        c_Ta_lin->GetPad(2)->SetGridy();
        res_lin_Ta.GetXaxis()->SetLabelSize(0.08);
        res_lin_Ta.GetXaxis()->SetTitle("T [K]");
        res_lin_Ta.GetXaxis()->SetTitleSize(0.08);
        res_lin_Ta.GetYaxis()->SetTitle("Residuum");
        res_lin_Ta.GetYaxis()->SetLabelSize(0.08);
        res_lin_Ta.GetYaxis()->SetTitleSize(0.08);
        res_lin_Ta.GetYaxis()->SetTitleOffset(0.4);
        res_lin_Ta.SetMarkerStyle(8);
        res_lin_Ta.SetMarkerSize(0.4);
        c_Ta_lin->GetPad(2)->SetFrameFillColor(0);
        c_Ta_lin->GetPad(2)->SetFrameBorderMode(0);
        c_Ta_lin->GetPad(2)->SetBottomMargin(0.17);
        c_Ta_lin->GetPad(2)->SetTopMargin(0.04);
	c_Ta_lin->GetPad(2)->SetRightMargin(0.018);

        res_lin_Ta.DrawClone("APE");
        TLine *line2 = new TLine(80,0,300,0);
        line2->SetLineColor(kRed);
        line2->Draw("same");
        TLegend* leg_lin_Ta_2= new TLegend(0.1,0.8,0.3,0.95);
        leg_lin_Ta_2->AddEntry("res_lin_Ta","Residuum+Fehler","EP");
        leg_lin_Ta_2->SetFillColor(0);
        leg_lin_Ta_2->Draw("same");





/*

        TGraphErrors data_ln_Ta(n_points,ln_T,ln_Ta,ln_T_err,ln_Ta_err);
        TCanvas* c_ln_Ta=new TCanvas("ln(Ta)-ln(T)","double logarithm",900,600);


        TF1* f_beta_Ta=new TF1("Linear Law","[0]+x*[1]",3.5,4.);
        f_beta_Ta->SetParameter(0,-20);
        f_beta_Ta->SetParameter(1,5);


        auto Fitpara_beta_Ta=data_ln_Ta.Fit(f_beta_Ta,"S","q",3.45,4.);


        data_ln_Ta.DrawClone("APE");
*/
        double* ln_inv_Si=new double[Si_counter];
        double* ln_inv_Si_err=new double[Si_counter];


        double* inv_T=new double[Si_counter];
        double* inv_T_err=new double[Si_counter];

        double* ln_T_Si= new double[Si_counter];
        double* ln_T_Si_err= new double[Si_counter];


	double* inv_Si= new double[Si_counter];
	double* inv_Si_err= new double [Si_counter];

      for(int i=0;i<Si_counter;i++){


                ln_T_Si[i]=log(T[i]);
                ln_T_Si_err[i]=T_err[i]/T[i];
                inv_T[i]=1/T[i];
                inv_T_err[i]=T_err[i]/(T[i]*T[i]);


                ln_inv_Si[i]=log(1/Si_N[i]);
                ln_inv_Si_err[i]=Si_N_err[i]/Si_N[i];

		inv_Si[i]=1./Si[i];
		inv_Si_err[i]=Si_err[i]/(Si[i]*Si[i]);

        }


        TGraphErrors rawdata_Si(n_points,T,Si,T_err,Si_err);
	rawdata_Si.SetTitle("Rawdata of R_{Si} with platinum (N);T [K];R_{Si} [#Omega]");	
	rawdata_Si.SetMarkerStyle(8);
	rawdata_Si.SetMarkerSize(0.7);



        TCanvas* c_Si=new TCanvas("Rawdata Si_N_Pt","Rawdata Si_N_Pt",900,600);
        rawdata_Si.DrawClone("APE");

        TGraphErrors data_inv_Si(Si_counter,T,inv_Si,T_err,inv_Si_err);
        data_inv_Si.SetTitle("trafo of R_{Si} with platinum (N);T [K];1/R_{Si} [1/#Omega]");
        data_inv_Si.SetMarkerStyle(8);
        data_inv_Si.SetMarkerSize(0.7);
	data_inv_Si.GetXaxis()->SetLimits(150,250);


        TF1* gap_real= new TF1("gap_energy_real","[0]*exp(-[1]/x)+[2]",100,300);

        gap_real->SetParameter(0,0.0012);
        gap_real->SetParameter(1,1380.4);
        gap_real->SetParameter(2,1749e-6);

        auto FitPara_gap_real=data_inv_Si.Fit(gap_real,"S","q",150,250);


        TCanvas* c_inv_Si=new TCanvas("inv data Si_N_Pt","Rawdata Si_N_Pt",900,600);
        c_inv_Si->Divide(1,2,0,0);
        c_inv_Si->cd(1);
        c_inv_Si->GetPad(1)->SetPad(0.,0.4,1.,1.);

        c_inv_Si->GetPad(1)->SetBottomMargin(0.012);
        c_inv_Si->GetPad(1)->SetTopMargin(0.07);
        c_inv_Si->GetPad(1)->SetRightMargin(0.018);
 //       c_inv_Si->GetPad(1)->SetLeftMargin(0.08);

        data_inv_Si.GetYaxis()->SetLabelSize(0.052);
        data_inv_Si.GetYaxis()->SetTitleSize(0.052);
        data_inv_Si.GetYaxis()->SetTitleOffset(0.8);



        data_inv_Si.DrawClone("APE");
        TLegend* leg_inv_Si= new TLegend(0.1,0.6,0.4,0.9);
        leg_inv_Si->AddEntry("data_inv_Si","Data","EP");
        leg_inv_Si->AddEntry(gap_real,"Fit: p0*exp( -p1/T) + p2","L");
        leg_inv_Si->SetFillColor(0);
        leg_inv_Si->Draw("same");

        double res_Si_inv[Si_counter];
        double res_Si_inv_err[Si_counter];

        for (int i=0;i<Si_counter;i++){
                res_Si_inv[i]=inv_Si[i]-gap_real->Eval(T[i]);
                res_Si_inv_err[i]=sqrt(pow(inv_Si_err[i],2)+pow(T_err[i]*(gap_real->Eval(T[i])-gap_real->GetParameter(2))*gap_real->GetParameter(1)/(T[i]*T[i]),2));
        }


        c_inv_Si->cd();
        c_inv_Si->cd(2);

        c_inv_Si->GetPad(2)->SetPad(0.,0.,1.,0.4);
        TGraphErrors res_inv_Si(Si_counter,T,res_Si_inv,nullptr,res_Si_inv_err);

        res_inv_Si.SetTitle("");
        res_inv_Si.GetXaxis()->SetLimits(150,250);

        c_inv_Si->GetPad(2)->SetGridy();
        res_inv_Si.GetXaxis()->SetLabelSize(0.08);
        res_inv_Si.GetXaxis()->SetTitle("T [K]");
        res_inv_Si.GetXaxis()->SetTitleSize(0.08);
        res_inv_Si.GetYaxis()->SetTitle("Residuum");
        res_inv_Si.GetYaxis()->SetLabelSize(0.08);
        res_inv_Si.GetYaxis()->SetTitleSize(0.08);
        res_inv_Si.GetYaxis()->SetTitleOffset(0.4);
        res_inv_Si.SetMarkerStyle(8);
        res_inv_Si.SetMarkerSize(0.4);
        c_inv_Si->GetPad(2)->SetFrameFillColor(0);
        c_inv_Si->GetPad(2)->SetFrameBorderMode(0);
        c_inv_Si->GetPad(2)->SetBottomMargin(0.17);
        c_inv_Si->GetPad(2)->SetTopMargin(0.04);
        c_inv_Si->GetPad(2)->SetRightMargin(0.018);

        res_inv_Si.DrawClone("APE");
        TLine *line_inv = new TLine(150,0,250,0);
        line_inv->SetLineColor(kRed);
        line_inv->Draw("same");
        TLegend* leg_inv_Si_2= new TLegend(0.1,0.8,0.3,0.95);
        leg_inv_Si_2->AddEntry("res_inv_Si","Residuum+Fehler","EP");
        leg_inv_Si_2->SetFillColor(0);
        leg_inv_Si_2->Draw("same");







        TGraphErrors data_ln_Si_inv_all(Si_counter,T,inv_Si,T_err,inv_Si_err);
	data_ln_Si_inv_all.SetTitle("transformed Rawdata of R_{Si} with platinum (N);T [K];1/R_{Si} [1/#Omega]");
        data_ln_Si_inv_all.SetMarkerStyle(8);
        data_ln_Si_inv_all.SetMarkerSize(0.7);


        TCanvas* c_Si_inv_all=new TCanvas("Rawdata Si_N_Pt ln all","Rawdata Si_N_Pt",900,600);
        data_ln_Si_inv_all.DrawClone("APE");



        TGraphErrors data_ln_Si_inv(Si_counter,inv_T,ln_inv_Si,inv_T_err,ln_inv_Si_err);
	data_ln_Si_inv.SetTitle("Linear Fit; 1/T [1/K]; ln(1/R_{Si}) [ln(1/#Omega)]");	
	data_ln_Si_inv.GetXaxis()->SetLimits(0.0035,0.006);


        TCanvas* c_ln_Si_inv=new TCanvas("ln(1/Si)-1/T"," logarithm inv",900,600);

	c_ln_Si_inv->Divide(1,2,0,0);
        c_ln_Si_inv->cd(1);
        c_ln_Si_inv->GetPad(1)->SetPad(0.,0.4,1.,1.);

        c_ln_Si_inv->GetPad(1)->SetBottomMargin(0.012);
        c_ln_Si_inv->GetPad(1)->SetTopMargin(0.07);
        c_ln_Si_inv->GetPad(1)->SetRightMargin(0.018);
 //       c_ln_Si_inv->GetPad(1)->SetLeftMargin(0.08);

        data_ln_Si_inv.GetYaxis()->SetLabelSize(0.052);
        data_ln_Si_inv.GetYaxis()->SetTitleSize(0.052);
        data_ln_Si_inv.GetYaxis()->SetTitleOffset(0.8);



        TF1* gap= new TF1("gap_energy","[1]*x+[0]",0.002,0.006);

        gap->SetParameter(0,1);
        gap->SetParameter(1,1);

        auto FitPara_gap=data_ln_Si_inv.Fit(gap,"S","q",0.0035,0.006);

        data_ln_Si_inv.DrawClone("APE");


        TLegend* leg_ln_Si_inv= new TLegend(0.1,0.6,0.4,0.9);
        leg_ln_Si_inv->AddEntry("data_ln_Si_inv","Data","EP");
        leg_ln_Si_inv->AddEntry(gap,"Fit: p1* 1/T + p0","L");
        leg_ln_Si_inv->SetFillColor(0);
        leg_ln_Si_inv->Draw("same");

        double res_Si[Si_counter];
        double res_Si_err[Si_counter];

        for (int i=0;i<Si_counter;i++){
                res_Si[i]=ln_inv_Si[i]-gap->Eval(inv_T[i]);
                res_Si_err[i]=sqrt(pow(ln_inv_Si_err[i],2)+pow(inv_T_err[i]*gap->GetParameter(1),2));
        }


        c_ln_Si_inv->cd();
        c_ln_Si_inv->cd(2);

        c_ln_Si_inv->GetPad(2)->SetPad(0.,0.,1.,0.4);
        TGraphErrors res_ln_Si_inv(Si_counter,inv_T,res_Si,nullptr,res_Si_err);

        res_ln_Si_inv.SetTitle("");
        res_ln_Si_inv.GetXaxis()->SetLimits(0.0035,0.006);

        c_ln_Si_inv->GetPad(2)->SetGridy();
        res_ln_Si_inv.GetXaxis()->SetLabelSize(0.08);
        res_ln_Si_inv.GetXaxis()->SetTitle("1/T [1/K]");
        res_ln_Si_inv.GetXaxis()->SetTitleSize(0.08);
        res_ln_Si_inv.GetYaxis()->SetTitle("Residuum");
        res_ln_Si_inv.GetYaxis()->SetLabelSize(0.08);
        res_ln_Si_inv.GetYaxis()->SetTitleSize(0.08);
        res_ln_Si_inv.GetYaxis()->SetTitleOffset(0.4);
        res_ln_Si_inv.SetMarkerStyle(8);
        res_ln_Si_inv.SetMarkerSize(0.4);
        c_ln_Si_inv->GetPad(2)->SetFrameFillColor(0);
        c_ln_Si_inv->GetPad(2)->SetFrameBorderMode(0);
        c_ln_Si_inv->GetPad(2)->SetBottomMargin(0.17);
        c_ln_Si_inv->GetPad(2)->SetTopMargin(0.04);
	c_ln_Si_inv->GetPad(2)->SetRightMargin(0.018);	

        res_ln_Si_inv.DrawClone("APE");
        TLine *line3 = new TLine(0.0035,0,0.006,0);
        line3->SetLineColor(kRed);
        line3->Draw("same");
        TLegend* leg_ln_Si_inv_2= new TLegend(0.1,0.8,0.3,0.95);
        leg_ln_Si_inv_2->AddEntry("res_ln_Si_inv","Residuum+Fehler","EP");
        leg_ln_Si_inv_2->SetFillColor(0);
        leg_ln_Si_inv_2->Draw("same");


/*
        TCanvas* c_ln_Si=new TCanvas("ln(1/Si)-ln(T)","double logaritm",900,600);
        TGraphErrors data_ln_Si(Si_counter,ln_T_Si,ln_inv_Si,ln_T_Si_err,ln_inv_Si_err);
        TF1* mob= new TF1("mobility","[0]+[1]*x",3.6,4.5);

        mob->SetParameter(0,1);
        mob->SetParameter(1,1.5);

        auto Fitpara_mob=data_ln_Si.Fit(mob,"S","q",3.6,4.5);


        data_ln_Si.DrawClone("APE");*/
}

