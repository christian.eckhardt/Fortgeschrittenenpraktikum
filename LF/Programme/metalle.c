#include "TROOT.h"
#include "read_mca.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TLegend.h"
#include "TLine.h"
#include <vector>

void metalle(){

	gStyle->SetOptFit(1);

	double* Stickstoff=read_mca("../Daten/Stickstoff.txt");	
	double* Helium=read_mca("../Daten/Helium.txt");
	double* Ko_params=read_mca("../Daten/kalib_params_Ko");
	double* Pt_params=read_mca("../Daten/kalib_params_Pt");
	double p0=Ko_params[1];
	double p0_err=Ko_params[2];
	double p1=Ko_params[3];
	double p1_err=Ko_params[4];
	double p2=Ko_params[5];
	double p2_err=Ko_params[6];
        
	double c0=Pt_params[1];
	double c0_err=Pt_params[2];
	double c1=Pt_params[3];
	double c1_err=Pt_params[4];
	double c2=Pt_params[5];
	double c2_err=Pt_params[6];
        

	vector<double> temp_Cu_He;
	vector<double> temp_Ta_He;
	vector<double> temp_Si_He;
	vector<double> temp_Ko_He;
	vector<double> temp_Pt_He;	

        vector<double> temp_Cu_N;
        vector<double> temp_Ta_N;
        vector<double> temp_Si_N;
        vector<double> temp_Ko_N;
	vector<double> temp_Pt_N;

        for(int i=1;i<=Helium[0];i++){
		if(i%7==1){
			temp_Pt_He.push_back(Helium[i]);
		}


                if(i%7==2){
                        temp_Ko_He.push_back(Helium[i]);
                        
                }
		
		if(i%7==3){
			temp_Cu_He.push_back(Helium[i]);	
		}

		if(i%7==4){
			temp_Ta_He.push_back(Helium[i]);
		}
		if(i%7==5){
			temp_Si_He.push_back(Helium[i]);
		}
        }

//	for(int i=0;i<temp_Cu_He.size();i++){cout<<temp_Cu_He[i]<<endl;}
	int size=(temp_Cu_He.size()-2)/4;
	double* Cu_He = new double[size];
	double* Ta_He = new double[size];
	double* Si_He = new double[size];
        double* Ko_He = new double[size];
	double* Pt_He = new double[size];


	for(int i=0;i<=temp_Cu_He.size()-3;i=i+4){
		Cu_He[i/4]=(temp_Cu_He[i]+temp_Cu_He[i+1]+temp_Cu_He[i+2]+temp_Cu_He[i+3])/4;
		Ta_He[i/4]=(temp_Ta_He[i]+temp_Ta_He[i+1]+temp_Ta_He[i+2]+temp_Ta_He[i+3])/4;
		Si_He[i/4]=(temp_Si_He[i]+temp_Si_He[i+1]+temp_Si_He[i+2]+temp_Si_He[i+3])/4;
		Ko_He[i/4]=(temp_Ko_He[i]+temp_Ko_He[i+1]+temp_Ko_He[i+2]+temp_Ko_He[i+3])/4;
		Pt_He[i/4]=(temp_Pt_He[i]+temp_Pt_He[i+1]+temp_Pt_He[i+2]+temp_Pt_He[i+3])/4;
	}	



	double* Cu_He_err = new double[size];
	double* Ta_He_err = new double[size];
	double* Si_He_err = new double[size];
	double* Ko_He_err = new double[size];
	double* Pt_He_err = new double[size];
	int n_points=0;


	for(int i=0;i<=temp_Cu_He.size()-3;i=i+4){
		Cu_He_err[i/4]=sqrt((pow(Cu_He[i/4]-temp_Cu_He[i],2)+pow(Cu_He[i/4]-temp_Cu_He[i+1],2)+pow(Cu_He[i/4]-temp_Cu_He[i+2],2)+pow(Cu_He[i/4]-temp_Cu_He[i+3],2))/3);
                Ta_He_err[i/4]=sqrt((pow(Ta_He[i/4]-temp_Ta_He[i],2)+pow(Ta_He[i/4]-temp_Ta_He[i+1],2)+pow(Ta_He[i/4]-temp_Ta_He[i+2],2)+pow(Ta_He[i/4]-temp_Ta_He[i+3],2))/3);
                Si_He_err[i/4]=sqrt((pow(Si_He[i/4]-temp_Si_He[i],2)+pow(Si_He[i/4]-temp_Si_He[i+1],2)+pow(Si_He[i/4]-temp_Si_He[i+2],2)+pow(Si_He[i/4]-temp_Si_He[i+3],2))/3);
                Ko_He_err[i/4]=sqrt((pow(Ko_He[i/4]-temp_Ko_He[i],2)+pow(Ko_He[i/4]-temp_Ko_He[i+1],2)+pow(Ko_He[i/4]-temp_Ko_He[i+2],2)+pow(Ko_He[i/4]-temp_Ko_He[i+3],2))/3);
		Pt_He_err[i/4]=sqrt((pow(Pt_He[i/4]-temp_Pt_He[i],2)+pow(Pt_He[i/4]-temp_Pt_He[i+1],2)+pow(Pt_He[i/4]-temp_Pt_He[i+2],2)+pow(Pt_He[i/4]-temp_Pt_He[i+3],2))/3);
		n_points++;

	}
	
	double* T=new double[n_points-1];
	double* ln_T=new double[n_points-1];

	double* Cu=new double[n_points-1];
	double* Cu_err=new double[n_points-1];

	double* Ta=new double[n_points-1];
	double* Ta_err=new double[n_points-1];

	double* ln_Cu=new double[n_points-1];
	double* ln_Cu_err=new double[n_points-1];

	double* ln_Ta=new double[n_points-1];
	double* ln_Ta_err=new double[n_points-1];

	double* Si=new double[n_points-1];
	double* Si_err=new double[n_points-1];


	double* T_err=new double[n_points-1];
	double* ln_T_err=new double[n_points-1];	


	int Si_counter=0;
	for(int i=0;i<n_points-1;i++){

		//if(){
			T[i]=log((Ko_He[i+1]-p2)/p0)/p1;//+sqrt(pow(log((Ko_He[i+1]-p2)/p0)*p1_err/(p1*p1),2)+pow(p0_err/(p1*p0),2)+pow(p2_err/(p1*(Ko_He[i+1]-p2)),2));
			T_err[i]=1/p1 * 1/(Ko_He[i+1]-p2)*Ko_He_err[i+1];
			ln_T[i]=log(T[i]);
			ln_T_err[i]=T_err[i]/T[i];

		Cu[i]=Cu_He[i+1];
		Cu_err[i]=Cu_He_err[i+1];

		ln_Cu[i]=log(Cu_He[i+1]);
		ln_Cu_err[i]=Cu_He_err[i+1]/Cu_He[i+1];

		Ta[i]=Ta_He[i+1];
		Ta_err[i]=Ta_He_err[i+1];

		ln_Ta[i]=log(Ta_He[i+1]);
		ln_Ta_err[i]=Ta_He_err[i+1]/Ta_He[i+1];

		Si[i]=Si_He[i+1];
		Si_err[i]=Si_He_err[i+1];


		if(Si[i]<1e10){Si_counter++;}

	}
	
	TGraphErrors rawdata_Cu(n_points-1,T,Cu,T_err,Cu_err);
	rawdata_Cu.SetTitle("Rawdata of R_{Cu} with coal (He);T [K];R_{Cu} [#Omega]");
	rawdata_Cu.SetMarkerStyle(8);
	rawdata_Cu.SetMarkerSize(0.7);

	TGraphErrors lin_Cu(n_points-1,T,Cu,T_err,Cu_err);
	lin_Cu.SetTitle("Linear Fit;T [K]; R_{Cu} [#Omega]");
	lin_Cu.GetXaxis()->SetLimits(75.,121.);
	lin_Cu.SetMarkerStyle(8);
	lin_Cu.SetMarkerSize(0.7);

	

	TCanvas* c_Cu=new TCanvas("Rawdata Cu_He_Ko","Rawdata Cu_He_Ko",900,600);	
	rawdata_Cu.DrawClone("APE");

	TCanvas* c_Cu_lin= new TCanvas("Linear Fit of Cu (coal)","Linear Fit of Cu (coal)",900,600);
	c_Cu_lin->Divide(1,2,0,0);
	c_Cu_lin->cd(1);
	c_Cu_lin->GetPad(1)->SetPad(0.,0.4,1.,1.);

        c_Cu_lin->GetPad(1)->SetBottomMargin(0.012);
        c_Cu_lin->GetPad(1)->SetTopMargin(0.07);
        c_Cu_lin->GetPad(1)->SetRightMargin(0.01);
 //       c_Cu_lin->GetPad(1)->SetLeftMargin(0.08);

	lin_Cu.GetYaxis()->SetLabelSize(0.052);
        lin_Cu.GetYaxis()->SetTitleSize(0.052);
        lin_Cu.GetYaxis()->SetTitleOffset(0.8);



        TF1* f_lin_Cu=new TF1("Linear Law","[0]*(1+(x-273.15)*[1])",75.,130.0);
        f_lin_Cu->SetParameter(0,20);
        f_lin_Cu->SetParameter(1,0.004);

      
	auto Fitpara_lin_Cu=lin_Cu.Fit(f_lin_Cu,"S","q",75.,120.);


	lin_Cu.DrawClone("APE");
	TLegend* leg_lin_Cu= new TLegend(0.1,0.6,0.4,0.9);
	leg_lin_Cu->AddEntry("lin_Cu","Data","EP");
	leg_lin_Cu->AddEntry(f_lin_Cu,"Fit: p0*[1+p1*(T-273.15K)]","L");
	leg_lin_Cu->SetFillColor(0);
	leg_lin_Cu->Draw("same");

        double res_Cu_lin[n_points-1];
        double res_Cu_lin_err[n_points-1];

        for (int i=0;i<n_points-1;i++){
                res_Cu_lin[i]=Cu[i]-f_lin_Cu->Eval(T[i]);
                res_Cu_lin_err[i]=sqrt(pow(Cu_err[i],2)+pow(T_err[i]*f_lin_Cu->GetParameter(1)*f_lin_Cu->GetParameter(0),2));
        }


	c_Cu_lin->cd();
	c_Cu_lin->cd(2);
	
	c_Cu_lin->GetPad(2)->SetPad(0.,0.,1.,0.4);
	TGraphErrors res_lin_Cu(n_points-1,T,res_Cu_lin,nullptr,res_Cu_lin_err);
	
	res_lin_Cu.SetTitle("");
	res_lin_Cu.GetXaxis()->SetLimits(75.,121.);

	c_Cu_lin->GetPad(2)->SetGridy();
	res_lin_Cu.GetXaxis()->SetLabelSize(0.08);
        res_lin_Cu.GetXaxis()->SetTitle("T [K]");
        res_lin_Cu.GetXaxis()->SetTitleSize(0.08);
        res_lin_Cu.GetYaxis()->SetTitle("Residuum");
        res_lin_Cu.GetYaxis()->SetLabelSize(0.08);
        res_lin_Cu.GetYaxis()->SetTitleSize(0.08);
        res_lin_Cu.GetYaxis()->SetTitleOffset(0.4);
        res_lin_Cu.SetMarkerStyle(8);
        res_lin_Cu.SetMarkerSize(0.4);
        c_Cu_lin->GetPad(2)->SetFrameFillColor(0);
        c_Cu_lin->GetPad(2)->SetFrameBorderMode(0);
        c_Cu_lin->GetPad(2)->SetBottomMargin(0.17);
        c_Cu_lin->GetPad(2)->SetTopMargin(0.04);

	c_Cu_lin->GetPad(2)->SetRightMargin(0.01);
	res_lin_Cu.DrawClone("APE");
        TLine *line = new TLine(75,0,120,0);
        line->SetLineColor(kRed);
        line->Draw("same");
        TLegend* leg_lin_Cu_2= new TLegend(0.1,0.8,0.3,0.95);
        leg_lin_Cu_2->AddEntry("res_lin_Cu","Residuum+Fehler","EP");
        leg_lin_Cu_2->SetFillColor(0);
        leg_lin_Cu_2->Draw("same");


	TGraphErrors data_ln_Cu_all(n_points-1,ln_T,ln_Cu,ln_T_err,ln_Cu_err);

	data_ln_Cu_all.SetTitle("logarithms of rawdata of R_{Cu};ln(T) [ln(K)];ln(R_{Cu}) [ln(#Omega)]");
	data_ln_Cu_all.SetMarkerStyle(8);
	TCanvas* c_ln_Cu_all=new TCanvas("ln(Cu)-ln(T) all data ","double logarithm",900,600);
	data_ln_Cu_all.SetMarkerSize(0.7);	
//        c_ln_Cu_all->SetBottomMargin(0.012);
        c_ln_Cu_all->SetTopMargin(0.07);
        c_ln_Cu_all->SetRightMargin(0.01);

        data_ln_Cu_all.GetYaxis()->SetLabelSize(0.052);
        data_ln_Cu_all.GetYaxis()->SetTitleSize(0.052);
        data_ln_Cu_all.GetYaxis()->SetTitleOffset(0.8);
	data_ln_Cu_all.DrawClone("APE");


//	TGraphErrors data_ln_Cu(n_points-1,ln_T,ln_Cu,ln_T_err,ln_Cu_err);
	TGraphErrors data_ln_Cu(n_points-1,T,Cu,T_err,Cu_err);
	data_ln_Cu.SetTitle("T^#beta Fit;T [K];R_{Cu} [#Omega]");
        data_ln_Cu.GetXaxis()->SetLimits(30,77);
	data_ln_Cu.SetMarkerStyle(8);
	data_ln_Cu.SetMarkerSize(0.7);	



	TCanvas* c_ln_Cu=new TCanvas("Cu-T","beta-plot",900,600);

	c_ln_Cu->Divide(1,2,0,0);
        c_ln_Cu->cd(1);
        c_ln_Cu->GetPad(1)->SetPad(0.,0.4,1.,1.);

        c_ln_Cu->GetPad(1)->SetBottomMargin(0.012);
        c_ln_Cu->GetPad(1)->SetTopMargin(0.07);
	c_ln_Cu->GetPad(1)->SetRightMargin(0.01);	

        data_ln_Cu.GetYaxis()->SetLabelSize(0.052);
        data_ln_Cu.GetYaxis()->SetTitleSize(0.052);
        data_ln_Cu.GetYaxis()->SetTitleOffset(0.8);



	TF1* f_beta_Cu=new TF1("linlaw","[1]*pow(x,[2])+[0]",30,77);
	f_beta_Cu->SetParameter(0,0.2);
	f_beta_Cu->SetParameter(1,5);
	f_beta_Cu->SetParameter(2,5);
	auto Fitpara_beta_Cu=data_ln_Cu.Fit(f_beta_Cu,"S","q",30,77);	

	data_ln_Cu.DrawClone("APE");

	TLegend* leg_ln_Cu= new TLegend(0.1,0.6,0.4,0.9);
        leg_ln_Cu->AddEntry("data_ln_Cu","Data","EP");
        leg_ln_Cu->AddEntry(f_beta_Cu,"Fit: p1*T^p2+p0","L");
        leg_ln_Cu->SetFillColor(0);
        leg_ln_Cu->Draw("same");

	double res_Cu_ln[n_points-1];
        double res_Cu_ln_err[n_points-1];

        for (int i=0;i<n_points-1;i++){
                res_Cu_ln[i]=Cu[i]-f_beta_Cu->Eval(T[i]);
                res_Cu_ln_err[i]=sqrt(pow(Cu_err[i],2)+pow(T_err[i]*f_beta_Cu->GetParameter(1)*f_beta_Cu->GetParameter(2)*pow(T[i],f_beta_Cu->GetParameter(2)-1),2));
        }


        c_ln_Cu->cd();
        c_ln_Cu->cd(2);

        c_ln_Cu->GetPad(2)->SetPad(0.,0.,1.,0.4);
        TGraphErrors res_ln_Cu(n_points-1,T,res_Cu_ln,nullptr,res_Cu_ln_err);

        res_ln_Cu.SetTitle("");
        res_ln_Cu.GetXaxis()->SetLimits(30,77);

        c_ln_Cu->GetPad(2)->SetGridy();
        res_ln_Cu.GetXaxis()->SetLabelSize(0.08);
        res_ln_Cu.GetXaxis()->SetTitle("T [K]");
        res_ln_Cu.GetXaxis()->SetTitleSize(0.08);
        res_ln_Cu.GetYaxis()->SetTitle("Residuum");
        res_ln_Cu.GetYaxis()->SetLabelSize(0.08);
        res_ln_Cu.GetYaxis()->SetTitleSize(0.08);
        res_ln_Cu.GetYaxis()->SetTitleOffset(0.4);
        res_ln_Cu.SetMarkerStyle(8);
        res_ln_Cu.SetMarkerSize(0.4);
        c_ln_Cu->GetPad(2)->SetFrameFillColor(0);
        c_ln_Cu->GetPad(2)->SetFrameBorderMode(0);
        c_ln_Cu->GetPad(2)->SetBottomMargin(0.17);
        c_ln_Cu->GetPad(2)->SetTopMargin(0.04);
	c_ln_Cu->GetPad(2)->SetRightMargin(0.01);

        res_ln_Cu.DrawClone("APE");
        TLine *line1 = new TLine(45,0,77,0);
        line1->SetLineColor(kRed);
        line1->Draw("same");
        TLegend* leg_ln_Cu_2= new TLegend(0.1,0.8,0.3,0.95);
        leg_ln_Cu_2->AddEntry("res_ln_Cu","Residuum+Fehler","EP");
        leg_ln_Cu_2->SetFillColor(0);
        leg_ln_Cu_2->Draw("same");




	TGraphErrors rawdata_Ta(n_points-1,T,Ta,T_err,Ta_err);
	rawdata_Ta.SetTitle("Rawdata of R_{Ta} with coal;T [K];R_{Ta} [#Omega]");
        rawdata_Ta.SetMarkerStyle(8);
        rawdata_Ta.SetMarkerSize(0.7);


        TGraphErrors lin_Ta(n_points-1,T,Ta,T_err,Ta_err);
        lin_Ta.SetTitle("Linear Fit;T [K]; R_{Ta} [#Omega]");
        lin_Ta.GetXaxis()->SetLimits(75.,121.);
	lin_Ta.SetMarkerStyle(8);
        lin_Ta.SetMarkerSize(0.7);




	TCanvas* c_Ta=new TCanvas("Rawdata Ta_He","Rawdata Ta_He",900,600);

	rawdata_Ta.DrawClone("APE");

        TCanvas* c_Ta_lin= new TCanvas("Linear Fit of Ta (coal)","Linear Fit of Ta (coal)",900,600);
        c_Ta_lin->Divide(1,2,0,0);
        c_Ta_lin->cd(1);
        c_Ta_lin->GetPad(1)->SetPad(0.,0.4,1.,1.);

        c_Ta_lin->GetPad(1)->SetBottomMargin(0.012);
        c_Ta_lin->GetPad(1)->SetTopMargin(0.07);
        c_Ta_lin->GetPad(1)->SetRightMargin(0.01);
 //       c_Ta_lin->GetPad(1)->SetLeftMargin(0.08);

        lin_Ta.GetYaxis()->SetLabelSize(0.052);
        lin_Ta.GetYaxis()->SetTitleSize(0.052);
        lin_Ta.GetYaxis()->SetTitleOffset(0.8);


        TF1* f_lin_Ta=new TF1("Linear Law_Ta","[0]*(1+(x-273.15)*[1])",75.,120.0);
        f_lin_Ta->SetParameter(0,20);
        f_lin_Ta->SetParameter(1,0.004);


        auto Fitpara_lin_Ta=lin_Ta.Fit(f_lin_Ta,"S","q",75.,120.);

	lin_Ta.DrawClone("APE");
//        f_lin_Ta->Draw("same");

	TLegend* leg_lin_Ta= new TLegend(0.1,0.6,0.4,0.9);
        leg_lin_Ta->AddEntry("lin_Ta","Data","EP");
        leg_lin_Ta->AddEntry(f_lin_Ta,"Fit: p0*[1+p1*(T-273.15K)]","L");
        leg_lin_Ta->SetFillColor(0);
        leg_lin_Ta->Draw("same");

        double res_Ta_lin[n_points-1];
        double res_Ta_lin_err[n_points-1];

        for (int i=0;i<n_points-1;i++){
                res_Ta_lin[i]=Ta[i]-f_lin_Ta->Eval(T[i]);
                res_Ta_lin_err[i]=sqrt(pow(Ta_err[i],2)+pow(T_err[i]*f_lin_Ta->GetParameter(1)*f_lin_Ta->GetParameter(0),2));
        }


        c_Ta_lin->cd();
        c_Ta_lin->cd(2);

        c_Ta_lin->GetPad(2)->SetPad(0.,0.,1.,0.4);
        TGraphErrors res_lin_Ta(n_points-1,T,res_Ta_lin,nullptr,res_Ta_lin_err);

        res_lin_Ta.SetTitle("");
        res_lin_Ta.GetXaxis()->SetLimits(75.,121.);

        c_Ta_lin->GetPad(2)->SetGridy();
        res_lin_Ta.GetXaxis()->SetLabelSize(0.08);
        res_lin_Ta.GetXaxis()->SetTitle("T [K]");
        res_lin_Ta.GetXaxis()->SetTitleSize(0.08);
        res_lin_Ta.GetYaxis()->SetTitle("Residuum");
        res_lin_Ta.GetYaxis()->SetLabelSize(0.08);
        res_lin_Ta.GetYaxis()->SetTitleSize(0.08);
        res_lin_Ta.GetYaxis()->SetTitleOffset(0.4);
        res_lin_Ta.SetMarkerStyle(8);
        res_lin_Ta.SetMarkerSize(0.4);
        c_Ta_lin->GetPad(2)->SetFrameFillColor(0);
        c_Ta_lin->GetPad(2)->SetFrameBorderMode(0);
        c_Ta_lin->GetPad(2)->SetBottomMargin(0.17);
        c_Ta_lin->GetPad(2)->SetTopMargin(0.04);
	
	c_Ta_lin->GetPad(2)->SetRightMargin(0.01);
        res_lin_Ta.DrawClone("APE");
        TLine *line2 = new TLine(75,0,120,0);
        line2->SetLineColor(kRed);
        line2->Draw("same");
        TLegend* leg_lin_Ta_2= new TLegend(0.1,0.8,0.3,0.95);
        leg_lin_Ta_2->AddEntry("res_lin_Ta","Residuum+Fehler","EP");
        leg_lin_Ta_2->SetFillColor(0);
        leg_lin_Ta_2->Draw("same");


        TGraphErrors data_ln_Ta_all(n_points-1,ln_T,ln_Ta,ln_T_err,ln_Ta_err);

        data_ln_Ta_all.SetTitle("logarithms of rawdata of R_{Ta};ln(T) [ln(K)];ln(R_{Ta}) [ln(#Omega)]");
        data_ln_Ta_all.SetMarkerStyle(8);
        TCanvas* c_ln_Ta_all=new TCanvas("ln(Ta)-ln(T) all data ","double logarithm",900,600);
        data_ln_Ta_all.SetMarkerSize(0.7);
//        c_ln_Ta_all->SetBottomMargin(0.012);
        c_ln_Ta_all->SetTopMargin(0.07);
        c_ln_Ta_all->SetRightMargin(0.01);

        data_ln_Ta_all.GetYaxis()->SetLabelSize(0.052);
        data_ln_Ta_all.GetYaxis()->SetTitleSize(0.052);
        data_ln_Ta_all.GetYaxis()->SetTitleOffset(0.8);
        data_ln_Ta_all.DrawClone("APE");



	TGraphErrors data_ln_Ta(n_points-1,T,Ta,T_err,Ta_err);
	data_ln_Ta.SetTitle("T^#beta Fit;T [K];R_{Ta} [#Omega]");
        data_ln_Ta.GetXaxis()->SetLimits(40,77);
        data_ln_Ta.SetMarkerStyle(8);
        data_ln_Ta.SetMarkerSize(0.7);



	TCanvas* c_ln_Ta=new TCanvas("Ta-T","beta-fit",900,600);

        c_ln_Ta->Divide(1,2,0,0);
        c_ln_Ta->cd(1);
        c_ln_Ta->GetPad(1)->SetPad(0.,0.4,1.,1.);

        c_ln_Ta->GetPad(1)->SetBottomMargin(0.012);
        c_ln_Ta->GetPad(1)->SetTopMargin(0.07);

        c_ln_Ta->GetPad(1)->SetRightMargin(0.01);
        data_ln_Ta.GetYaxis()->SetLabelSize(0.052);
        data_ln_Ta.GetYaxis()->SetTitleSize(0.052);
        data_ln_Ta.GetYaxis()->SetTitleOffset(0.8);


        TF1* f_beta_Ta=new TF1("Linear Law","[0]+pow(x,[2])*[1]",40,77);
        f_beta_Ta->SetParameter(0,0.2);
        f_beta_Ta->SetParameter(1,5);
	f_beta_Ta->SetParameter(2,5);

        auto Fitpara_beta_Ta=data_ln_Ta.Fit(f_beta_Ta,"S","q",40,77);


	data_ln_Ta.DrawClone("APE");



        TLegend* leg_ln_Ta= new TLegend(0.1,0.6,0.4,0.9);
        leg_ln_Ta->AddEntry("data_ln_Ta","Data","EP");
        leg_ln_Ta->AddEntry(f_beta_Ta,"Fit: p1*T^p2+p0","L");
        leg_ln_Ta->SetFillColor(0);
        leg_ln_Ta->Draw("same");

        double res_Ta_ln[n_points-1];
        double res_Ta_ln_err[n_points-1];

        for (int i=0;i<n_points-1;i++){
                res_Ta_ln[i]=Ta[i]-f_beta_Ta->Eval(T[i]);
                res_Ta_ln_err[i]=sqrt(pow(Ta_err[i],2)+pow(T_err[i]*f_beta_Ta->GetParameter(1)*f_beta_Ta->GetParameter(2)*pow(T[i],f_beta_Ta->GetParameter(2)-1),2));
        }


        c_ln_Ta->cd();
        c_ln_Ta->cd(2);

        c_ln_Ta->GetPad(2)->SetPad(0.,0.,1.,0.4);
        TGraphErrors res_ln_Ta(n_points-1,T,res_Ta_ln,nullptr,res_Ta_ln_err);

        res_ln_Ta.SetTitle("");
        res_ln_Ta.GetXaxis()->SetLimits(40,77);

        c_ln_Ta->GetPad(2)->SetGridy();
        res_ln_Ta.GetXaxis()->SetLabelSize(0.08);
        res_ln_Ta.GetXaxis()->SetTitle("T [K]");
        res_ln_Ta.GetXaxis()->SetTitleSize(0.08);
        res_ln_Ta.GetYaxis()->SetTitle("Residuum");
        res_ln_Ta.GetYaxis()->SetLabelSize(0.08);
        res_ln_Ta.GetYaxis()->SetTitleSize(0.08);
        res_ln_Ta.GetYaxis()->SetTitleOffset(0.4);
        res_ln_Ta.SetMarkerStyle(8);
        res_ln_Ta.SetMarkerSize(0.4);
        c_ln_Ta->GetPad(2)->SetFrameFillColor(0);
        c_ln_Ta->GetPad(2)->SetFrameBorderMode(0);
        c_ln_Ta->GetPad(2)->SetBottomMargin(0.17);
        c_ln_Ta->GetPad(2)->SetTopMargin(0.04);

        c_ln_Ta->GetPad(2)->SetRightMargin(0.01);
        res_ln_Ta.DrawClone("APE");
        TLine *line3 = new TLine(40,0,77,0);
        line3->SetLineColor(kRed);
        line3->Draw("same");
        TLegend* leg_ln_Ta_2= new TLegend(0.1,0.8,0.3,0.95);
        leg_ln_Ta_2->AddEntry("res_ln_Ta","Residuum+Fehler","EP");
        leg_ln_Ta_2->SetFillColor(0);
        leg_ln_Ta_2->Draw("same");




        double* ln_inv_Si=new double[Si_counter];
        double* ln_inv_Si_err=new double[Si_counter];


	double* inv_T=new double[Si_counter];
        double* inv_T_err=new double[Si_counter];

	double* ln_T_Si= new double[Si_counter];
	double* ln_T_Si_err= new double[Si_counter];


      for(int i=0;i<Si_counter;i++){

                
		ln_T_Si[i]=log(T[i]);
                ln_T_Si_err[i]=T_err[i]/T[i];
                inv_T[i]=1/T[i];
                inv_T_err[i]=T_err[i]/(T[i]*T[i]);


	        ln_inv_Si[i]=log(1/Si_He[i+1]);
		ln_inv_Si_err[i]=Si_He_err[i+1]/Si_He[i+1];

        }


	
	TGraphErrors rawdata_Si(n_points-1,T,Si,T_err,Si_err);
	rawdata_Si.SetTitle("Rawdata of R_{Si} with coal (He);T [K]; R_{Si}");
        rawdata_Si.SetMarkerStyle(8);
        rawdata_Si.SetMarkerSize(0.7);
	



	TCanvas* c_Si=new TCanvas("Rawdata Si_He","Rawdata Si_He",900,600);
	rawdata_Si.DrawClone();


	TGraphErrors rawdata_Si_cut(Si_counter,T,Si,T_err,Si_err);
        rawdata_Si_cut.SetTitle("Mobility Fit;T [K]; R_{Si}");
        rawdata_Si_cut.SetMarkerStyle(8);
        rawdata_Si_cut.SetMarkerSize(0.7);
	rawdata_Si_cut.GetXaxis()->SetLimits(55,80);

        TCanvas* c_Si_cut=new TCanvas("Rawdata Si_He cut","Rawdata Si_He cut",900,600);
        c_Si_cut->Divide(1,2,0,0);
        c_Si_cut->cd(1);
        c_Si_cut->GetPad(1)->SetPad(0.,0.4,1.,1.);

        c_Si_cut->GetPad(1)->SetBottomMargin(0.012);
        c_Si_cut->GetPad(1)->SetTopMargin(0.07);
        c_Si_cut->GetPad(1)->SetRightMargin(0.01);
 //       c_Si_cut->GetPad(1)->SetLeftMargin(0.08);

        rawdata_Si_cut.GetYaxis()->SetLabelSize(0.052);
        rawdata_Si_cut.GetYaxis()->SetTitleSize(0.052);
        rawdata_Si_cut.GetYaxis()->SetTitleOffset(0.8);

        TF1* mob_cut= new TF1("mobility_cut","[0]+[1]*pow(x,[2])",55,80);

        mob_cut->SetParameter(0,1);
        mob_cut->SetParameter(1,1.5);
	mob_cut->SetParameter(2,1.5);

        auto Fitpara_mob_cut=rawdata_Si_cut.Fit(mob_cut,"S","q",55,80);

        

        rawdata_Si_cut.DrawClone("APE");

        TLegend* leg_Si_cut= new TLegend(0.1,0.6,0.4,0.9);
        leg_Si_cut->AddEntry("rawdata_Si_cut","Data","EP");
        leg_Si_cut->AddEntry(mob_cut,"Fit: p1*T^p2 + p0","L");
        leg_Si_cut->SetFillColor(0);
        leg_Si_cut->Draw("same");

        double res_Si_cut[Si_counter];
        double res_Si_cut_err[Si_counter];

        for (int i=0;i<Si_counter;i++){
                res_Si_cut[i]=Si[i]-mob_cut->Eval(T[i]);
                res_Si_cut_err[i]=sqrt(pow(Si_err[i],2)+pow(T_err[i]*mob_cut->GetParameter(1)*mob_cut->GetParameter(2)*pow(T[i],mob_cut->GetParameter(2)-1),2));
        }


        c_Si_cut->cd();
        c_Si_cut->cd(2);

        c_Si_cut->GetPad(2)->SetPad(0.,0.,1.,0.4);
        TGraphErrors res_Si_cut_plot(Si_counter,T,res_Si_cut,nullptr,res_Si_cut_err);

        res_Si_cut_plot.SetTitle("");
        res_Si_cut_plot.GetXaxis()->SetLimits(55,80);

        c_Si_cut->GetPad(2)->SetGridy();
        res_Si_cut_plot.GetXaxis()->SetLabelSize(0.08);
        res_Si_cut_plot.GetXaxis()->SetTitle("T [K]");
        res_Si_cut_plot.GetXaxis()->SetTitleSize(0.08);
        res_Si_cut_plot.GetYaxis()->SetTitle("Residuum");
        res_Si_cut_plot.GetYaxis()->SetLabelSize(0.08);
        res_Si_cut_plot.GetYaxis()->SetTitleSize(0.08);
        res_Si_cut_plot.GetYaxis()->SetTitleOffset(0.4);
        res_Si_cut_plot.SetMarkerStyle(8);
        res_Si_cut_plot.SetMarkerSize(0.4);
        c_Si_cut->GetPad(2)->SetFrameFillColor(0);
        c_Si_cut->GetPad(2)->SetFrameBorderMode(0);
        c_Si_cut->GetPad(2)->SetBottomMargin(0.17);
        c_Si_cut->GetPad(2)->SetTopMargin(0.04);

        c_Si_cut->GetPad(2)->SetRightMargin(0.01);
        res_Si_cut_plot.DrawClone("APE");
        TLine *line_cut = new TLine(55,0,80,0);
        line_cut->SetLineColor(kRed);
        line_cut->Draw("same");
        TLegend* leg_Si_cut_2= new TLegend(0.1,0.8,0.3,0.95);
        leg_Si_cut_2->AddEntry("res_Si_cut_plot","Residuum+Fehler","EP");
        leg_Si_cut_2->SetFillColor(0);
        leg_Si_cut_2->Draw("same");


	TGraphErrors data_ln_Si_inv(Si_counter,inv_T,ln_inv_Si,inv_T_err,ln_inv_Si_err);
	data_ln_Si_inv.SetTitle("transformed rawdata of R_{Si} with coal; 1/T [1/K]; ln(1/R_{Si}) [ln(1/#Omega)]");
	data_ln_Si_inv.SetMarkerStyle(8);
	data_ln_Si_inv.SetMarkerSize(0.7);

        TCanvas* c_Si_inv=new TCanvas("trans Rawdata Si_He","trans Rawdata Si_He",900,600);
        data_ln_Si_inv.DrawClone("APE");


/*        data_ln_Si_inv.GetXaxis()->SetLimits(0.0035,0.006);


        c_ln_Si_inv->Divide(1,2,0,0);
        c_ln_Si_inv->cd(1);
        c_ln_Si_inv->GetPad(1)->SetPad(0.,0.4,1.,1.);

        c_ln_Si_inv->GetPad(1)->SetBottomMargin(0.012);
        c_ln_Si_inv->GetPad(1)->SetTopMargin(0.07);
 //       c_ln_Si_inv->GetPad(1)->SetRightMargin(0.01);
 //       c_ln_Si_inv->GetPad(1)->SetLeftMargin(0.08);

        data_ln_Si_inv.GetYaxis()->SetLabelSize(0.052);
        data_ln_Si_inv.GetYaxis()->SetTitleSize(0.052);
        data_ln_Si_inv.GetYaxis()->SetTitleOffset(0.8);



	TCanvas* c_ln_Si_inv=new TCanvas("ln(1/Si)-1/T"," logarithm inv",900,600);
	TF1* gap= new TF1("gap_energy","[1]*x+[0]",0.006,0.0012);
	
	gap->SetParameter(0,1);
	gap->SetParameter(1,1);

	auto FitPara_gap=data_ln_Si_inv.Fit(gap,"S","q",0.006,0.012);

	data_ln_Si_inv.DrawClone("APE");


        TLegend* leg_ln_Si_inv= new TLegend(0.1,0.6,0.4,0.9);
        leg_ln_Si_inv->AddEntry("data_ln_Si_inv","Data","EP");
        leg_ln_Si_inv->AddEntry(gap,"Fit: p1* 1/T + p0","L");
        leg_ln_Si_inv->SetFillColor(0);
        leg_ln_Si_inv->Draw("same");

        double res_Si[Si_counter];
        double res_Si_err[Si_counter];

        for (int i=0;i<Si_counter;i++){
                res_Si[i]=ln_inv_Si[i]-gap->Eval(inv_T[i]);
                res_Si_err[i]=sqrt(pow(ln_inv_Si_err[i],2)+pow(inv_T_err[i]*gap->GetParameter(1),2));
        }


        c_ln_Si_inv->cd();
        c_ln_Si_inv->cd(2);

        c_ln_Si_inv->GetPad(2)->SetPad(0.,0.,1.,0.4);
        TGraphErrors res_ln_Si_inv(Si_counter,inv_T,res_Si,nullptr,res_Si_err);

        res_ln_Si_inv.SetTitle("");
        res_ln_Si_inv.GetXaxis()->SetLimits(0.0035,0.006);

        c_ln_Si_inv->GetPad(2)->SetGridy();
        res_ln_Si_inv.GetXaxis()->SetLabelSize(0.08);
        res_ln_Si_inv.GetXaxis()->SetTitle("1/T [1/K]");
        res_ln_Si_inv.GetXaxis()->SetTitleSize(0.08);
        res_ln_Si_inv.GetYaxis()->SetTitle("Residuum");
        res_ln_Si_inv.GetYaxis()->SetLabelSize(0.08);
        res_ln_Si_inv.GetYaxis()->SetTitleSize(0.08);
        res_ln_Si_inv.GetYaxis()->SetTitleOffset(0.4);
        res_ln_Si_inv.SetMarkerStyle(8);
        res_ln_Si_inv.SetMarkerSize(0.4);
        c_ln_Si_inv->GetPad(2)->SetFrameFillColor(0);
        c_ln_Si_inv->GetPad(2)->SetFrameBorderMode(0);
        c_ln_Si_inv->GetPad(2)->SetBottomMargin(0.17);
        c_ln_Si_inv->GetPad(2)->SetTopMargin(0.04);

        res_ln_Si_inv.DrawClone("APE");
        TLine *line3 = new TLine(0.0035,0,0.006,0);
        line3->SetLineColor(kRed);
        line3->Draw("same");
        TLegend* leg_ln_Si_inv_2= new TLegend(0.1,0.8,0.3,0.95);
        leg_ln_Si_inv_2->AddEntry("res_ln_Si_inv","Residuum+Fehler","EP");
        leg_ln_Si_inv_2->SetFillColor(0);
        leg_ln_Si_inv_2->Draw("same");






*/
	TGraphErrors data_ln_Si_all(Si_counter,ln_T_Si,ln_inv_Si,ln_T_Si_err,ln_inv_Si_err);
        data_ln_Si_all.SetTitle("logarithms rawdata of R_{Si} with coal ; ln(T) [ln(K)]; ln(1/R_{Si}) [ln(1/#Omega)]");
	data_ln_Si_all.SetMarkerStyle(8);
	data_ln_Si_all.SetMarkerSize(0.7);
//        data_ln_Si_all.GetXaxis()->SetLimits(4.05,4.4);



        TCanvas* c_ln_Si_all=new TCanvas("ln(1/Si)-ln(T) all","double logaritm all",900,600);
        c_ln_Si_all->SetTopMargin(0.07);
        c_ln_Si_all->SetRightMargin(0.01);

        data_ln_Si_all.GetYaxis()->SetLabelSize(0.052);
        data_ln_Si_all.GetYaxis()->SetTitleSize(0.052);
        data_ln_Si_all.GetYaxis()->SetTitleOffset(0.8);


	data_ln_Si_all.DrawClone("APE");



	TGraphErrors data_ln_Si(Si_counter,ln_T_Si,ln_inv_Si,ln_T_Si_err,ln_inv_Si_err);
	data_ln_Si.SetTitle("Linear Fit; ln(T) [ln(K)]; ln(1/R_{Si}) [ln(1/#Omega)]");
        data_ln_Si.GetXaxis()->SetLimits(4.05,4.4);


	data_ln_Si.SetMarkerStyle(8);
        data_ln_Si.SetMarkerSize(0.7);

	TCanvas* c_ln_Si=new TCanvas("ln(1/Si)-ln(T)","double logaritm",900,600);

        c_ln_Si->Divide(1,2,0,0);
        c_ln_Si->cd(1);
        c_ln_Si->GetPad(1)->SetPad(0.,0.4,1.,1.);

        c_ln_Si->GetPad(1)->SetBottomMargin(0.012);
        c_ln_Si->GetPad(1)->SetTopMargin(0.07);
        c_ln_Si->GetPad(1)->SetRightMargin(0.01);
 //       c_ln_Si->GetPad(1)->SetLeftMargin(0.08);

        data_ln_Si.GetYaxis()->SetLabelSize(0.052);
        data_ln_Si.GetYaxis()->SetTitleSize(0.052);
        data_ln_Si.GetYaxis()->SetTitleOffset(0.8);


	TF1* mob= new TF1("mobility","[0]+[1]*x",4.05,4.4);
	
	mob->SetParameter(0,1);
        mob->SetParameter(1,1.5);

	auto Fitpara_mob=data_ln_Si.Fit(mob,"S","q",4.05,4.4);

	data_ln_Si.DrawClone("APE");


        TLegend* leg_ln_Si= new TLegend(0.1,0.6,0.4,0.9);
        leg_ln_Si->AddEntry("data_ln_Si","Data","EP");
        leg_ln_Si->AddEntry(mob,"Fit: p1* ln(T) + p0","L");
        leg_ln_Si->SetFillColor(0);
        leg_ln_Si->Draw("same");

        double res_Si[Si_counter];
        double res_Si_err[Si_counter];

        for (int i=0;i<Si_counter;i++){
                res_Si[i]=ln_inv_Si[i]-mob->Eval(ln_T[i]);
                res_Si_err[i]=sqrt(pow(ln_inv_Si_err[i],2)+pow(ln_T_err[i]*mob->GetParameter(1),2));
        }


        c_ln_Si->cd();
        c_ln_Si->cd(2);

        c_ln_Si->GetPad(2)->SetPad(0.,0.,1.,0.4);
        TGraphErrors res_ln_Si(Si_counter,ln_T,res_Si,nullptr,res_Si_err);

        res_ln_Si.SetTitle("");
        res_ln_Si.GetXaxis()->SetLimits(4.05,4.4);

        c_ln_Si->GetPad(2)->SetGridy();
        res_ln_Si.GetXaxis()->SetLabelSize(0.08);
        res_ln_Si.GetXaxis()->SetTitle("ln(T) [ln(K)]");
        res_ln_Si.GetXaxis()->SetTitleSize(0.08);
        res_ln_Si.GetYaxis()->SetTitle("Residuum");
        res_ln_Si.GetYaxis()->SetLabelSize(0.08);
        res_ln_Si.GetYaxis()->SetTitleSize(0.08);
        res_ln_Si.GetYaxis()->SetTitleOffset(0.4);
        res_ln_Si.SetMarkerStyle(8);
        res_ln_Si.SetMarkerSize(0.4);
        c_ln_Si->GetPad(2)->SetFrameFillColor(0);
        c_ln_Si->GetPad(2)->SetFrameBorderMode(0);
        c_ln_Si->GetPad(2)->SetBottomMargin(0.17);
        c_ln_Si->GetPad(2)->SetTopMargin(0.04);

        c_ln_Si->GetPad(2)->SetRightMargin(0.01);
        res_ln_Si.DrawClone("APE");
        TLine *line4 = new TLine(4.05,0,4.4,0);
        line4->SetLineColor(kRed);
        line4->Draw("same");
        TLegend* leg_ln_Si_2= new TLegend(0.1,0.8,0.3,0.95);
        leg_ln_Si_2->AddEntry("res_ln_Si","Residuum+Fehler","EP");
        leg_ln_Si_2->SetFillColor(0);
        leg_ln_Si_2->Draw("same");




}
	


