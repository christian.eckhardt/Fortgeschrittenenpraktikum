#include "TROOT.h"
#include "read_mca.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TLegend.h"
#include "TLine.h"
#include <vector>

void stickstoff(){

        gStyle->SetOptFit(1);

        double* Stickstoff=read_mca("../Daten/Stickstoff.txt");
        double* Helium=read_mca("../Daten/Helium.txt");
        double* Ko_params=read_mca("../Daten/kalib_params_Ko");
        double* Pt_params=read_mca("../Daten/kalib_params_Pt");
        double p0=Ko_params[1];
        double p0_err=Ko_params[2];
        double p1=Ko_params[3];
        double p1_err=Ko_params[4];
        double p2=Ko_params[5];
        double p2_err=Ko_params[6];

        double c0=Pt_params[1];
        double c0_err=Pt_params[2];
        double c1=Pt_params[3];
        double c1_err=Pt_params[4];
        double c2=Pt_params[5];
        double c2_err=Pt_params[6];


        vector<double> temp_Cu_He;
        vector<double> temp_Ta_He;
        vector<double> temp_Si_He;
        vector<double> temp_Ko_He;
        vector<double> temp_Pt_He;

        vector<double> temp_Cu_N;
        vector<double> temp_Ta_N;
        vector<double> temp_Si_N;
        vector<double> temp_Ko_N;
        vector<double> temp_Pt_N;

        for(int i=1;i<=Stickstoff[0];i++){
                if(i%7==1){
                        temp_Pt_N.push_back(Stickstoff[i]);
                }


                if(i%7==2){
                        temp_Ko_N.push_back(Stickstoff[i]);
                 
                }

                if(i%7==3){
                        temp_Cu_N.push_back(Stickstoff[i]);
                }

                if(i%7==4){
                        temp_Ta_N.push_back(Stickstoff[i]);
                }
                if(i%7==5){
                        temp_Si_N.push_back(Stickstoff[i]);
                }

	}

//      for(int i=0;i<temp_Cu_N.size();i++){cout<<temp_Cu_N[i]<<endl;}
        int size=(temp_Cu_N.size()-1)/4;
        double* Cu_N = new double[size];
        double* Ta_N = new double[size];
        double* Si_N = new double[size];
        double* Ko_N = new double[size];
        double* Pt_N = new double[size];


        for(int i=0;i<=temp_Cu_N.size()-2;i=i+4){
                Cu_N[i/4]=(temp_Cu_N[i]+temp_Cu_N[i+1]+temp_Cu_N[i+2]+temp_Cu_N[i+3])/4;
                Ta_N[i/4]=(temp_Ta_N[i]+temp_Ta_N[i+1]+temp_Ta_N[i+2]+temp_Ta_N[i+3])/4;
                Si_N[i/4]=(temp_Si_N[i]+temp_Si_N[i+1]+temp_Si_N[i+2]+temp_Si_N[i+3])/4;
                Ko_N[i/4]=(temp_Ko_N[i]+temp_Ko_N[i+1]+temp_Ko_N[i+2]+temp_Ko_N[i+3])/4;
                Pt_N[i/4]=(temp_Pt_N[i]+temp_Pt_N[i+1]+temp_Pt_N[i+2]+temp_Pt_N[i+3])/4;
        }



        double* Cu_N_err = new double[size];
        double* Ta_N_err = new double[size];
        double* Si_N_err = new double[size];
        double* Ko_N_err = new double[size];
        double* Pt_N_err = new double[size];
        int n_points=0;


        for(int i=0;i<=temp_Cu_N.size()-2;i=i+4){
                Cu_N_err[i/4]=sqrt((pow(Cu_N[i/4]-temp_Cu_N[i],2)+pow(Cu_N[i/4]-temp_Cu_N[i+1],2)+pow(Cu_N[i/4]-temp_Cu_N[i+2],2)+pow(Cu_N[i/4]-temp_Cu_N[i+3],2))/3);
                Ta_N_err[i/4]=sqrt((pow(Ta_N[i/4]-temp_Ta_N[i],2)+pow(Ta_N[i/4]-temp_Ta_N[i+1],2)+pow(Ta_N[i/4]-temp_Ta_N[i+2],2)+pow(Ta_N[i/4]-temp_Ta_N[i+3],2))/3);
                Si_N_err[i/4]=sqrt((pow(Si_N[i/4]-temp_Si_N[i],2)+pow(Si_N[i/4]-temp_Si_N[i+1],2)+pow(Si_N[i/4]-temp_Si_N[i+2],2)+pow(Si_N[i/4]-temp_Si_N[i+3],2))/3);
                Ko_N_err[i/4]=sqrt((pow(Ko_N[i/4]-temp_Ko_N[i],2)+pow(Ko_N[i/4]-temp_Ko_N[i+1],2)+pow(Ko_N[i/4]-temp_Ko_N[i+2],2)+pow(Ko_N[i/4]-temp_Ko_N[i+3],2))/3);
                Pt_N_err[i/4]=sqrt((pow(Pt_N[i/4]-temp_Pt_N[i],2)+pow(Pt_N[i/4]-temp_Pt_N[i+1],2)+pow(Pt_N[i/4]-temp_Pt_N[i+2],2)+pow(Pt_N[i/4]-temp_Pt_N[i+3],2))/3);
                n_points++;

        }



        double* T=new double[n_points];
        double* ln_T=new double[n_points];

        double* Cu=new double[n_points];
        double* Cu_err=new double[n_points];

        double* Ta=new double[n_points];
        double* Ta_err=new double[n_points];

        double* ln_Cu=new double[n_points];
        double* ln_Cu_err=new double[n_points];

        double* ln_Ta=new double[n_points];
        double* ln_Ta_err=new double[n_points];

        double* Si=new double[n_points];
        double* Si_err=new double[n_points];


        double* T_err=new double[n_points];
        double* ln_T_err=new double[n_points];


        int Si_counter=0;
        for(int i=0;i<n_points;i++){

                //if(){
                        T[i]=log((Ko_N[i]-p2)/p0)/p1;
                        T_err[i]=1/p1 * 1/(Ko_N[i]-p2)*Ko_N_err[i];
                        ln_T[i]=log(T[i]);
                        ln_T_err[i]=T_err[i]/T[i];

                Cu[i]=Cu_N[i];
                Cu_err[i]=Cu_N_err[i];

                ln_Cu[i]=log(Cu_N[i]);
                ln_Cu_err[i]=Cu_N_err[i]/Cu_N[i];

                Ta[i]=Ta_N[i];
                Ta_err[i]=Ta_N_err[i];

                ln_Ta[i]=log(Ta_N[i]);
                ln_Ta_err[i]=Ta_N_err[i]/Ta_N[i];

                Si[i]=Si_N[i];
                Si_err[i]=Si_N_err[i];


                if(Si[i]<1e10){Si_counter++;}

        }

        TGraphErrors rawdata_Cu(n_points,T,Cu,T_err,Cu_err);
        TGraphErrors data_ln_Cu(n_points,ln_T,ln_Cu,ln_T_err,ln_Cu_err);



        TCanvas* c_Cu=new TCanvas("Rawdata Cu_N","Rawdata Cu_N",900,600);


        TF1* f_lin_Cu=new TF1("Linear Law","[0]*(1+(x-273.15)*[1])",80.,130.0);
        f_lin_Cu->SetParameter(0,20);
        f_lin_Cu->SetParameter(1,0.004);


        auto Fitpara_lin_Cu=rawdata_Cu.Fit(f_lin_Cu,"S","q",80.,120.);


        rawdata_Cu.DrawClone("APE");


        TCanvas* c_ln_Cu=new TCanvas("ln(Cu)-ln(T)","double logarithm",900,600);

        TF1* f_beta_Cu=new TF1("linlaw","[1]*x+[0]",4,4.5);
        f_beta_Cu->SetParameter(0,-10);
        f_beta_Cu->SetParameter(1,5);
        auto Fitpara_beta_Cu=data_ln_Cu.Fit(f_beta_Cu,"S","q",4,4.6);

        data_ln_Cu.DrawClone("APE");


        TGraphErrors rawdata_Ta(n_points,T,Ta,T_err,Ta_err);
        TGraphErrors data_ln_Ta(n_points,ln_T,ln_Ta,ln_T_err,ln_Ta_err);


        TCanvas* c_Ta=new TCanvas("Rawdata Ta_N","Rawdata Ta_N",900,600);

        TF1* f_lin_Ta=new TF1("Linear Law","[0]*(1+(x-273.15)*[1])",80.,120.0);
        f_lin_Ta->SetParameter(0,20);
        f_lin_Ta->SetParameter(1,0.004);


        auto Fitpara_lin_Ta=rawdata_Ta.Fit(f_lin_Ta,"S","q",80.,120.);
        rawdata_Ta.DrawClone("APE");




        TCanvas* c_ln_Ta=new TCanvas("ln(Ta)-ln(T)","double logarithm",900,600);


        TF1* f_beta_Ta=new TF1("Linear Law","[0]+x*[1]",3.5,4.6);
        f_beta_Ta->SetParameter(0,-20);
        f_beta_Ta->SetParameter(1,5);


        auto Fitpara_beta_Ta=data_ln_Ta.Fit(f_beta_Ta,"S","q",4.,4.6);


        data_ln_Ta.DrawClone("APE");



        double* ln_inv_Si=new double[Si_counter];
        double* ln_inv_Si_err=new double[Si_counter];


        double* inv_T=new double[Si_counter];
        double* inv_T_err=new double[Si_counter];

        double* ln_T_Si= new double[Si_counter];
        double* ln_T_Si_err= new double[Si_counter];


      for(int i=0;i<Si_counter;i++){


                ln_T_Si[i]=log(T[i]);
                ln_T_Si_err[i]=T_err[i]/T[i];
                inv_T[i]=1/T[i];
                inv_T_err[i]=T_err[i]/(T[i]*T[i]);


                ln_inv_Si[i]=log(1/Si_N[i]);
                ln_inv_Si_err[i]=Si_N_err[i]/Si_N[i];

        }



        TGraphErrors rawdata_Si(n_points,T,Si,T_err,Si_err);
        TGraphErrors data_ln_Si_inv(Si_counter,inv_T,ln_inv_Si,inv_T_err,ln_inv_Si_err);
        TGraphErrors data_ln_Si(Si_counter,ln_T_Si,ln_inv_Si,ln_T_Si_err,ln_inv_Si_err);

        TCanvas* c_Si=new TCanvas("Rawdata Si_N","Rawdata Si_N",900,600);
        rawdata_Si.DrawClone();

        TCanvas* c_ln_Si_inv=new TCanvas("ln(1/Si)-1/T"," logarithm inv",900,600);
        TF1* gap= new TF1("gap_energy","[1]*x+[0]",0.008,0.0012);

        gap->SetParameter(0,1);
        gap->SetParameter(1,1);

        auto FitPara_gap=data_ln_Si_inv.Fit(gap,"S","q",0.008,0.011);

        data_ln_Si_inv.DrawClone("APE");
/*
        TCanvas* c_ln_Si=new TCanvas("ln(1/Si)-ln(T)","double logaritm",900,600);
        TF1* mob= new TF1("mobility","[0]+[1]*x",4.05,4.4);

        mob->SetParameter(0,1);
        mob->SetParameter(1,1.5);

        auto Fitpara_mob=data_ln_Si.Fit(mob,"S","q",4.05,4.4);


        data_ln_Si.DrawClone("APE");*/
}


