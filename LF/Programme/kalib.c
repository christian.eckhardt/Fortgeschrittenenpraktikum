#include "TROOT.h"
#include "read_mca.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TLegend.h"
#include "TLine.h"
#include <vector>
#include "TMath.h"
void kalib(){

	gStyle->SetOptFit(1);

	double* TRaum=read_mca("../Daten/Temp_Raum.txt");
	double* RRaum=read_mca("../Daten/Raumtemp.txt");
	double* Helium=read_mca("../Daten/Helium.txt");
	double* Stickstoff=read_mca("../Daten/Stickstoff.txt");

	
	double temp_TR=0;
	for(int i=1;i<=TRaum[0];i++){
		temp_TR+=TRaum[i];
	}
	double TR=temp_TR/TRaum[0];
	
	temp_TR=0;
	for(int i=1;i<=TRaum[0];i++){
		temp_TR+=pow(TR-TRaum[i],2);
	}

	double TR_err=sqrt(temp_TR/(TRaum[0]-1));
	cout<<"Raumtemperatur="<<TR<<"+/-"<<TR_err<<endl;

	double temp_Pt_RR=0;
	double anzahl_Pt_RR=0;

	for(int i=1;i<=RRaum[0];i++){
		if(i%7==1){
			temp_Pt_RR+=RRaum[i];
//			cout<<RRaum[i]<<endl;
			anzahl_Pt_RR++;
		}
	}
	double Pt_RR=temp_Pt_RR/anzahl_Pt_RR;

	temp_Pt_RR=0;
	for(int i=1;i<=RRaum[0];i++){
		if(i%7==1){
			temp_Pt_RR+=pow(Pt_RR-RRaum[i],2);
		}
	}

	double Pt_RR_err=sqrt(temp_Pt_RR/(anzahl_Pt_RR-1));
	cout<<"Platin_R_TRaum="<<Pt_RR<<"+/-"<<Pt_RR_err<<endl;

        double temp_Ko_RR=0;
        double anzahl_Ko_RR=0;

        for(int i=1;i<=RRaum[0];i++){
                if(i%7==2){
                        temp_Ko_RR+=RRaum[i];
//                      cout<<RRaum[i]<<endl;
                        anzahl_Ko_RR++;
                }
        }
        double Ko_RR=temp_Ko_RR/anzahl_Ko_RR;

        temp_Ko_RR=0;
        for(int i=1;i<=RRaum[0];i++){
                if(i%7==2){
                        temp_Ko_RR+=pow(Ko_RR-RRaum[i],2);
                }
        }
	double Ko_RR_err=sqrt(temp_Ko_RR/(anzahl_Ko_RR-1));
	cout<<"Kohle_R_TRaum="<<Ko_RR<<"+/-"<<Ko_RR_err<<endl;



	double temp_Ko_He=0;	
	double anzahl_Ko_He=0;

	for(int i=Helium[0]-18*7;i<=Helium[0];i++){
		if(i%7==2){
			temp_Ko_He+=Helium[i];
			anzahl_Ko_He++;
		}
	}

	double Ko_He=temp_Ko_He/anzahl_Ko_He;

	temp_Ko_He=0;
	for(int i=Helium[0]-18*7;i<=Helium[0];i++){
		if(i%7==2){
			temp_Ko_He+=pow(Ko_He-Helium[i],2);
		}
	}
	
	double Ko_He_err=sqrt(temp_Ko_He/(anzahl_Ko_He-1));
	cout<<"Ko_Helium="<<Ko_He<<"+/-"<<Ko_He_err<<endl;

	double temp_Ko_N=0;
	double anzahl_Ko_N=0;

	for(int i=Stickstoff[0]-7*5;i<=Stickstoff[0];i++){
		if(i%7==2){
			temp_Ko_N+=Stickstoff[i];
			anzahl_Ko_N++;
		}
	}

	double Ko_N=temp_Ko_N/anzahl_Ko_N;

	temp_Ko_N=0;
	for(int i=Stickstoff[0]-7*5;i<=Stickstoff[0];i++){
		if(i%7==2){
			temp_Ko_N+=pow(Ko_N-Stickstoff[i],2);
		}
	}
	
	double Ko_N_err=sqrt(temp_Ko_N/(anzahl_Ko_N-1));
	cout<<"Ko_Stickstoff="<<Ko_N<<"+/-"<<Ko_N_err<<endl;	


	double temp_Pt_N=0;
        double anzahl_Pt_N=0;

        for(int i=Stickstoff[0]-7*5;i<=Stickstoff[0];i++){
                if(i%7==1){
                        temp_Pt_N+=Stickstoff[i];
                        anzahl_Pt_N++;
                }
        }

        double Pt_N=temp_Pt_N/anzahl_Pt_N;

        temp_Pt_N=0;
        for(int i=Stickstoff[0]-7*5;i<=Stickstoff[0];i++){
                if(i%7==1){
                        temp_Pt_N+=pow(Pt_N-Stickstoff[i],2);
                }
        }

        double Pt_N_err=sqrt(temp_Pt_N/(anzahl_Pt_N-1));
        cout<<"Pt_Stickstoff="<<Pt_N<<"+/-"<<Pt_N_err<<endl;



	double T_Kalib_Pt[2]={TR+273.15,77.35};
	int n_Pt=2;
	double Pt[2]={Pt_RR,Pt_N};
	double Pt_err[2]={Pt_RR_err,Pt_N_err};

	TGraphErrors Pt_fit(n_Pt,T_Kalib_Pt,Pt,nullptr,Pt_err);
	Pt_fit.SetTitle("Resistance-Temperature calibration of platinum;T [K];R_{Pt} [#Omega]");
	
	TCanvas* trafo_Pt=new TCanvas("Kalibration_Pt","Kalibration_Pt",900,600);

 //       Pt_fit.GetYaxis()->SetLabelSize(0.052);
 //       Pt_fit.GetYaxis()->SetTitleSize(0.052);
 //       Pt_fit.GetYaxis()->SetTitleOffset(0.8);
        Pt_fit.SetMarkerStyle(8);
	Pt_fit.SetMarkerSize(1);
	TF1* f_Pt=new TF1("Linear Law","[0]+x*[1]",0.,310);
        f_Pt->SetParameter(0,0);
        f_Pt->SetParameter(1,1);
	
        auto Fitpara_Pt=Pt_fit.Fit(f_Pt,"S","q");
        
	Pt_fit.DrawClone("APE");

		


	double T_Kalib_Ko[3]={TR+273.15,4.22,77.35};
	int n_Ko=3;
	double Ko[3]={Ko_RR,Ko_He,Ko_N};
	double Ko_err[3]={Ko_RR_err,Ko_He_err,Ko_N_err};

        TGraphErrors Ko_fit(n_Ko,T_Kalib_Ko,Ko,nullptr,Ko_err);
        Ko_fit.SetTitle("Resistence-Temperature calibration of coal;T [K];R_{coal} [#Omega]");

        TCanvas* trafo_Ko=new TCanvas("Kalibration_Ko","Kalibration_Ko",900,600);

//        Ko_fit.GetYaxis()->SetLabelSize(0.032);
//        Ko_fit.GetYaxis()->SetTitleSize(0.032);
        Ko_fit.GetYaxis()->SetTitleOffset(1.5);
	Ko_fit.SetMarkerStyle(8);
	Ko_fit.SetMarkerSize(1);
        TF1* f_Ko=new TF1("Exp Law","[0]*exp(x*[1])+[2]",0.,310);
        f_Ko->SetParameter(0,10000);
        f_Ko->SetParameter(1,-1);
	f_Ko->SetParameter(2,Pt_RR);
        auto Fitpara_Ko=Ko_fit.Fit(f_Ko,"S","q");
        Ko_fit.DrawClone("APE");

	double p0_Ko=f_Ko->GetParameter(0);
        double p1_Ko=f_Ko->GetParameter(1);
	double p2_Ko=f_Ko->GetParameter(2);

	double p0_Ko_1=13473.4;
        double p0_Ko_2=13385.6;
        double p0_Ko_3=13473.3;
        double p0_Ko_4=13473.4;

        double p1_Ko_1=-0.0570175;
        double p1_Ko_2=-0.0569328;
        double p1_Ko_3=-0.0570167;
        double p1_Ko_4=-0.0570177;

	double p2_Ko_1=501.613;
        double p2_Ko_2=501.609;
        double p2_Ko_3=501.609;
        double p2_Ko_4=501.609;

        double err_p0_Ko=sqrt((pow(p0_Ko_1-p0_Ko,2)+pow(p0_Ko_2-p0_Ko,2)+pow(p0_Ko_3-p0_Ko,2)+pow(p0_Ko_4-p0_Ko,2))/4);
        double err_p1_Ko=sqrt((pow(p1_Ko_1-p1_Ko,2)+pow(p1_Ko_2-p1_Ko,2)+pow(p1_Ko_3-p1_Ko,2)+pow(p1_Ko_4-p1_Ko,2))/4);

	double err_p2_Ko=sqrt((pow(p2_Ko_1-p2_Ko,2)+pow(p2_Ko_2-p2_Ko,2)+pow(p2_Ko_3-p2_Ko,2)+pow(p2_Ko_4-p2_Ko,2))/4);


        cout<<"Ko_fit_p0="<<p0_Ko<<"+/-"<<err_p0_Ko<<endl;
        cout<<"Ko_fit_p1="<<p1_Ko<<"+/-"<<err_p1_Ko<<endl;
	cout<<"Ko_fit_p2="<<p2_Ko<<"+/-"<<err_p2_Ko<<endl;
//      output als file

        ofstream fitres_Ko;
        fitres_Ko.open("../Daten/kalib_params_Ko");
        fitres_Ko<<"<<DATA>>\n";
        fitres_Ko<<p0_Ko<<"\n"<<err_p0_Ko<<"\n"<<p1_Ko<<"\n"<<err_p1_Ko<<"\n"<<p2_Ko<<"\n"<<err_p2_Ko<<"\n";
        fitres_Ko<<"<<END>>";
        fitres_Ko.close();

	

        double p0_Pt=f_Pt->GetParameter(0);
        double p1_Pt=f_Pt->GetParameter(1);


	double p0_pt_1=-9.85849;
	double p0_pt_2=-9.84027;
	double p0_pt_3=-9.84343;
	double p0_pt_4=-9.85533;

	double p1_pt_1=0.409064;
	double p1_pt_2=0.408828;
	double p1_pt_3=0.408926;
	double p1_pt_4=0.408966;

        double err_p0_Pt=sqrt((pow(p0_pt_1-p0_Pt,2)+pow(p0_pt_2-p0_Pt,2)+pow(p0_pt_3-p0_Pt,2)+pow(p0_pt_4-p0_Pt,2))/4);
        double err_p1_Pt=sqrt((pow(p1_pt_1-p1_Pt,2)+pow(p1_pt_2-p1_Pt,2)+pow(p1_pt_3-p1_Pt,2)+pow(p1_pt_4-p1_Pt,2))/4);

        cout<<"Pt_fit_p0="<<p0_Pt<<"+/-"<<err_p0_Pt<<endl;
        cout<<"Pt_fit_p1="<<p1_Pt<<"+/-"<<err_p1_Pt<<endl;
//      output als file

        ofstream fitres_Pt;
        fitres_Pt.open("../Daten/kalib_params_Pt");
        fitres_Pt<<"<<DATA>>\n";
        fitres_Pt<<p0_Pt<<"\n"<<err_p0_Pt<<"\n"<<p1_Pt<<"\n"<<err_p1_Pt<<"\n";
        fitres_Pt<<"<<END>>";
        fitres_Pt.close();


}
