#include "TCanvas.h"
#include "TROOT.h"
#include "TGraphErrors.h"
#include "TF1.h"
void test(){
	gStyle->SetOptFit(1);
	double z[7]={608,723,838,1009,1180,1351,1522};
	double dx[7]={198,234,269,324,390,475,560};
	double dy[6]={2858,1054,618.3,252.7,450,937.4};//,1215.0};
	double err[7]={1,1,1,1,2,2,3};
	double err_y[6]={20,20,20,20,20,20};
	TF1* para= new TF1("para","[2]*x*x+[1]*x+[0]",0.,20.);
	TGraphErrors* graphx= new TGraphErrors(7,z,dx,nullptr,err);
	
	TGraphErrors* graphy= new TGraphErrors(6,z,dy,nullptr,err_y);

	TCanvas* cx= new TCanvas("cx","cx",900,600);
//	auto fit_dx=graphx->Fit(para,"S","q");
	graphx->Draw();

	
	TCanvas* cy= new TCanvas("cy","cy",900,600);
	auto fit_dy=graphy->Fit(para,"S","q");
	graphy->Draw();
}
