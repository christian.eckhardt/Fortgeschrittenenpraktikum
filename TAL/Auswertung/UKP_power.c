#include "TROOT.h"
#include "read_mca.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TLegend.h"
#include "TLine.h"
#include "TMath.h"

using namespace std;

	double PI = 4*atan(1);
	int UKP_power(){

	gStyle->SetOptStat(0);
	gStyle->SetOptFit(1);

	int n= read_mca("../Data/ukp_P.txt")[0];
	int n_d= read_mca("../Data/Diode_I")[0];
    double* raw = &read_mca("../Data/ukp_P.txt")[1];
	double* P_diode=&read_mca("../Data/Diode_P")[1];
	double* I_diode=&read_mca("../Data/Diode_I")[1];
	double* P_diode_err=&read_mca("../Data/Diode_P_err")[1];
	double* P_d = new double[n/3];
	double* P_d_err = new double[n/3];

    double* P_ukp= new double[n/3];
    double* P_ukp_err=new double[n/3];
    double* I_ukp=new double[n/3];

	for(int i=0;i<n;i++){
        P_ukp[i/3]=raw[i+1];
        P_ukp_err[i/3]=raw[i+2];
		I_ukp[i/3]=raw[i];
        i++;i++;
    }
    n=n/3;
	for(int i=0;i<n;i++){
		for(int j=0;j<n_d;j++){
				
			if(abs(I_ukp[i]-I_diode[j])<0.001){
				P_d[i]=P_diode[j];			
				P_d_err[i]=P_diode_err[j];		
				cout << "I= " << I_ukp[i] << "P= " << P_ukp[i]<<endl;	
			}
		}
	}
		

	TGraphErrors* graph_lin = new TGraphErrors(n,P_d,P_ukp,P_d_err,P_ukp_err);
    graph_lin->SetTitle("Bestimmung der Pumpschwellenleistung via linearer Anpassung");

    TCanvas* plot2=new TCanvas("Zero-Cross","Lin Fit",900,600);

    plot2->Divide(1,2,0,0);
    plot2->cd(1);
    plot2->GetPad(1)->SetPad(0,0.4,1.,1.);
    plot2->GetPad(1)->SetBottomMargin(0.01);
    plot2->GetPad(1)->SetTopMargin(0.07);
    plot2->GetPad(1)->SetRightMargin(0.02);
    plot2->GetPad(1)->SetLeftMargin(0.08);

    graph_lin->GetYaxis()->SetLabelSize(0.052);
    graph_lin->GetYaxis()->SetTitleSize(0.052);
    graph_lin->GetYaxis()->SetTitleOffset(0.8);
    graph_lin->GetYaxis()->SetTitle("Leistung UKP Laser [mW]");
    graph_lin->GetXaxis()->SetTitle("Leistung Diodenlaser [mW]");
    graph_lin->GetYaxis()->SetLabelSize(0.05);
    graph_lin->GetYaxis()->SetTitleSize(0.05);
    graph_lin->GetYaxis()->SetRangeUser(-50,600);
    graph_lin->GetXaxis()->SetLabelSize(0.05);
    graph_lin->GetXaxis()->SetTitleSize(0.05);
    graph_lin->GetXaxis()->SetLimits(0,1600);
	TF1* f=new TF1("Linear","[0]*x+[1]",P_d[0], P_d[n-1]);
    f->SetParameter(0,1);
    f->SetParameter(1,1);

    auto Fitpara_lin=graph_lin->Fit(f,"S","q");
    graph_lin->Draw();
    f->Draw("Same");
	
	TLegend* leg1=new TLegend(0.1,0.7,0.3,0.9);
    leg1->AddEntry("graph","Daten","EP");
    leg1->AddEntry(f,"Fit","L");
    leg1->SetFillColor(0);
    leg1->Draw("same");

//////
//Nulldurchgang berechnen
	double p1 = f->GetParameter(0);
	double p2 = f->GetParameter(1);
	double zero = -p2/p1;


    TLine *line_z = new TLine(zero,-50,zero,600);
    line_z->SetLineColor(kBlue);
    line_z->Draw("same");

    double res[n];
	double res_lin_err[n];

    for(int i=0;i<n;i++){
        res[i]=P_ukp[i]-f->Eval(P_d[i]);
		res_lin_err[i]=sqrt(pow(P_d_err[i],2)+pow(f->GetParameter(0)*P_ukp_err[i],2));
    }

    plot2->cd();
    plot2->cd(2);

    plot2->GetPad(2)->SetPad(0.,0.,1.,0.4);
    TGraphErrors residuen(n,P_d,res,nullptr,res_lin_err);

    plot2->GetPad(2)->SetGridy();
    residuen.GetXaxis()->SetLabelSize(0.08);
    residuen.GetXaxis()->SetTitle("Leistung Pumplaser [mW]");
    residuen.GetXaxis()->SetTitleSize(0.08);
    residuen.GetYaxis()->SetTitle("Residuum");
    residuen.GetYaxis()->SetLabelSize(0.08);
    residuen.GetYaxis()->SetTitleSize(0.08);
    residuen.GetYaxis()->SetTitleOffset(0.55);
    residuen.GetXaxis()->SetLimits(0,1600);
	residuen.SetMarkerStyle(8);
    residuen.SetMarkerSize(0.4);

    plot2->GetPad(2)->SetFrameFillColor(0);
    plot2->GetPad(2)->SetFrameBorderMode(0);
    plot2->GetPad(2)->SetBottomMargin(0.17);
    plot2->GetPad(2)->SetTopMargin(0.04);
    plot2->GetPad(2)->SetRightMargin(0.02);
    plot2->GetPad(2)->SetLeftMargin(0.08);


    residuen.DrawClone("APE");
    TLine *line = new TLine(0,0,1600,0);
    line->SetLineColor(kRed);
    line->Draw("same");
    TLegend* leg2= new TLegend(0.4,0.2,0.6,0.4);
    leg2->AddEntry("residuen","Residuum+Fehler","EP");
    leg2->SetFillColor(0);
    leg2->Draw("same");
	
	return 0;
}

