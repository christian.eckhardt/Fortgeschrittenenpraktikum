#include "TROOT.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TGraphErrors.h"
#include "read_mca.h"
#include <fstream>
#include "TMath.h"
#include "TBox.h"
#include "TLegend.h"
#include "TLine.h"

using namespace std;

void spektren(){

	gStyle->SetOptFit(1);

	const int n1= read_mca("../Data/2Tag/etalon1.txt")[0];
	const int n2= read_mca("../Data/2Tag/etalon2.txt")[0];
	const int n3= read_mca("../Data/2Tag/etalon3.txt")[0];
	const int n4= read_mca("../Data/2Tag/etalon4.txt")[0];
	const int n5= read_mca("../Data/2Tag/etalon5.txt")[0];

	double * etalon1=& read_mca("../Data/2Tag/etalon1.txt")[1];
	double * etalon2=& read_mca("../Data/2Tag/etalon2.txt")[1];
	double * etalon3=& read_mca("../Data/2Tag/etalon3.txt")[1];
	double * etalon4=& read_mca("../Data/2Tag/etalon4.txt")[1];
	double * etalon5=& read_mca("../Data/2Tag/etalon5.txt")[1];


	double intent1 [n1/2];
	double err_intent1 [n1/2];

	double t1 [n1/2];
//	double err_t1 [n1/2];

	double intent2 [n2/2];
	double err_intent2 [n2/2];
	double t2 [n2/2];
//	double err_t2 [n2/2];

	double intent3 [n3/2];
	double err_intent3 [n3/2];
	double t3 [n3/2];
//	double err_t3 [n3/2];

	double intent4 [n4/2];
	double err_intent4 [n4/2];
	double t4 [n4/2];
//	double err_t4 [n4/2];

	double intent5 [n5/2];
	double err_intent5 [n5/2];
	double t5 [n5/2];
//	double err_t5 [n5/2];

        for (int i=0;i<n1;i++){
                intent1[i/2]=etalon1[i+1];
                t1[i/2]=etalon1[i];
		i++;
	}
        for (int i=0;i<n2;i++){        
                intent2[i/2]=etalon2[i+1];
                t2[i/2]=etalon2[i];
		i++;
        }        

	for (int i=0;i<n3;i++){
                intent3[i/2]=etalon3[i+1];
                t3[i/2]=etalon3[i];
                i++;
	}

	for (int i=0;i<n4;i++){
                intent4[i/2]=etalon4[i+1];
                t4[i/2]=etalon4[i];
                i++;
	}
	
	for (int i=0;i<n5;i++){
                intent5[i/2]=etalon5[i+1];
                t5[i/2]=etalon5[i];
		i++;
                
        }



	int begin1 =1967;
	int end1 =2160;
	double temp1=0;
	double var_temp1=0;

	for (int i=begin1;i<end1;i++){
		temp1+=intent1[i]/(end1-begin1-1);
	}

	for (int i=begin1;i<end1;i++){
		var_temp1+=pow(temp1-intent1[i],2)/(end1-begin1-2);
	}

	for (int i=0;i<n1/2;i++){
		err_intent1[i]=sqrt(var_temp1);
//		err_t1[i]=0.001; //zeitfehler abschätzen oder vernchlässigen?
	}
		

	int begin2 =2231;
	int end2 =2449;
	double temp2=0;
	double var_temp2=0;

	for (int i=begin2;i<end2;i++){
		temp2+=intent2[i]/(end2-begin2-1);
	}

	for (int i=begin2;i<end2;i++){
		var_temp2+=pow(temp2-intent2[i],2)/(end2-begin2-2);
	}

	for (int i=0;i<n2/2;i++){
		err_intent2[i]=sqrt(var_temp2);
//		err_t2[i]=0.001; //zeitfehler abschätzen oder vernchlässigen?
	}



	int begin3 =2141;
	int end3 =2577;
	double temp3=0;
	double var_temp3=0;

	for (int i=begin3;i<end3;i++){
		temp3+=intent3[i]/(end3-begin3-1);
	}

	for (int i=begin3;i<end3;i++){
		var_temp3+=pow(temp3-intent3[i],2)/(end3-begin3-2);
	}

	for (int i=0;i<n3/2;i++){
		err_intent3[i]=sqrt(var_temp3);
//		err_t3[i]=0.001; //zeitfehler abschätzen oder vernchlässigen?
	}



	int begin4 =2141;
	int end4 =2560;
	double temp4=0;
	double var_temp4=0;

	for (int i=begin4;i<end4;i++){
		temp4+=intent4[i]/(end4-begin4-1);
	}

	for (int i=begin4;i<end4;i++){
		var_temp4+=pow(temp4-intent4[i],2)/(end4-begin4-2);
	}

	for (int i=0;i<n4/2;i++){
		err_intent4[i]=sqrt(var_temp4);
//		err_t4[i]=0.001; //zeitfehler abschätzen oder vernchlässigen?
	}



	int begin5 =2335;
	int end5 =2575;
	double temp5=0;
	double var_temp5=0;

	for (int i=begin5;i<end5;i++){
		temp5+=intent5[i]/(end5-begin5-1);
	}

	for (int i=begin5;i<end5;i++){
		var_temp5+=pow(temp5-intent5[i],2)/(end5-begin5-2);
	}

	for (int i=0;i<n5/2;i++){
		err_intent5[i]=sqrt(var_temp5);
//		err_t5[i]=0.001; //zeitfehler abschätzen oder vernchlässigen?
	}

	TGraphErrors* eta1= new TGraphErrors(n1/2,t1,intent1,nullptr,err_intent1);
	eta1->SetTitle("Autokorrelator Messung 5");
	eta1->GetXaxis()->SetTitle("Zeit [ps]");
	eta1->GetYaxis()->SetTitle("Autokorrelationsfunktion [a.u.]");

	TF1* gaus1= new TF1("gaus1","[0]*TMath::Gaus(x,[1],[2])+0.085");
	TCanvas* c_eta1= new TCanvas("c_eta1","c_eta1",900,600);
	eta1->Draw("APE");

	TLegend* leg1= new TLegend(0.1,0.7,0.3,0.9);
	leg1->AddEntry("eta1","Daten","EPL");
	leg1->SetFillColor(0);
	leg1->Draw("same");

	c_eta1->SaveAs("../Protokoll/Protokoll_Data/auto_5.pdf","pdf");


	TGraphErrors* eta2= new TGraphErrors(n2/2,t2,intent2,nullptr,err_intent2);
	eta2->SetTitle("Autokorrelator Messung 4");
	eta2->GetXaxis()->SetTitle("Zeit [ps]");
	eta2->GetYaxis()->SetTitle("Autokorrelationsfunktion [a.u.]");

	TF1* gaus2= new TF1("gaus2","[0]*TMath::Gaus(x,[1],[2])+0.1017");
	TCanvas* c_eta2= new TCanvas("c_eta2","c_eta2",900,600);
	eta2->Draw("APE");

	TLegend* leg2= new TLegend(0.1,0.7,0.3,0.9);
	leg2->AddEntry("eta2","Daten","EPL");
	leg2->SetFillColor(0);
	leg2->Draw("same");

	c_eta2->SaveAs("../Protokoll/Protokoll_Data/auto_4.pdf","pdf");

	TGraphErrors* eta3= new TGraphErrors(n3/2,t3,intent3,nullptr,err_intent3);
	eta3->SetTitle("Autokorrelator Messung 3");
	eta3->GetXaxis()->SetTitle("Zeit [ps]");
	eta3->GetYaxis()->SetTitle("Autokorrelationsfunktion [a.u.]");

	TF1* gaus3= new TF1("gaus3","[0]*TMath::Gaus(x,[1],[2])+0.1221");
	TCanvas* c_eta3= new TCanvas("c_eta3","c_eta3",900,600);
	eta3->Draw("APE");


	TLegend* leg3= new TLegend(0.1,0.7,0.3,0.9);
	leg3->AddEntry("eta3","Daten","EPL");
	leg3->SetFillColor(0);
	leg3->Draw("same");

	c_eta3->SaveAs("../Protokoll/Protokoll_Data/auto_3.pdf","pdf");

	TGraphErrors* eta4= new TGraphErrors(n4/2,t4,intent4,nullptr,err_intent4);
	eta4->SetTitle("Autokorrelator Messung 2");
	eta4->GetXaxis()->SetTitle("Zeit [ps]");
	eta4->GetYaxis()->SetTitle("Autokorrelationsfunktion [a.u.]");

	TF1* gaus4= new TF1("gaus4","[0]*TMath::Gaus(x,[1],[2])+0.136");
	TCanvas* c_eta4= new TCanvas("c_eta4","c_eta4",900,600);
	eta4->Draw("APE");

	TLegend* leg4= new TLegend(0.1,0.7,0.3,0.9);
	leg4->AddEntry("eta4","Daten","EPL");
	leg4->SetFillColor(0);
	leg4->Draw("same");

	c_eta4->SaveAs("../Protokoll/Protokoll_Data/auto_2.pdf","pdf");

	TGraphErrors* eta5= new TGraphErrors(n5/2,t5,intent5,nullptr,err_intent5);
	eta5->SetTitle("Autokorrelator Messung 1");
	eta5->GetXaxis()->SetTitle("Zeit [ps]");
	eta5->GetYaxis()->SetTitle("Autokorrelationsfunktion [a.u.]");



	double rbound1=20;
	double lbound1=-16;

	double rbound2=19;
	double lbound2=-15;

	double rbound3=15;
	double lbound3=-11;
	
	double rbound4=15;
	double lbound4=-11;
	
	double rbound5=15;
	double lbound5=-11;

	TF1* gaus5= new TF1("gaus5","[0]*TMath::Gaus(x,[1],[2])+0.1487",lbound5,rbound5);
	TCanvas* c_eta5= new TCanvas("c_eta5","c_eta5",900,600);
	eta5->Draw("APE");


	

	TLine* left5= new TLine(lbound5,0.1156,lbound5,0.424);
	left5->SetLineColor(kRed);
	left5->Draw("same");	

	TLine* right5= new TLine(rbound5,0.1156,rbound5,0.424);
	right5->SetLineColor(kRed);
	right5->Draw("same");

	TBox* box5= new TBox(t5[end5-1],0.1156,t5[begin5],0.424);
	box5->SetFillStyle(3004);
	box5->SetFillColor(4);
	box5->Draw("same");


	TLegend* leg5= new TLegend(0.6,0.6,0.9,0.9);
	leg5->AddEntry("eta5","Daten","EPL");
	leg5->AddEntry(box5,"Rauschbereich zur Abschaetzung der Fehler","F");
	leg5->AddEntry(left5,"Fitbereich","L");
	leg5->SetFillColor(0);
	leg5->Draw("same");


	c_eta5->SaveAs("../Protokoll/Protokoll_Data/auto_1.pdf","pdf");



	gaus1->SetParameter(0,0.11);
	gaus1->SetParameter(1,2);
	gaus1->SetParameter(2,35);
//	gaus1->SetParameter(3,0.7);

	auto fitres1=eta1->Fit(gaus1,"S","q",lbound1,rbound1);


	gaus2->SetParameter(0,0.18);
	gaus2->SetParameter(1,2);
	gaus2->SetParameter(2,30);
//	gaus2->SetParameter(3,0.09);


	auto fitres2=eta2->Fit(gaus2,"S","q",lbound2,rbound2);

	gaus3->SetParameter(0,0.23);
	gaus3->SetParameter(1,2);
	gaus3->SetParameter(2,27);
//	gaus3->SetParameter(3,0.12);

	auto fitres3=eta3->Fit(gaus3,"S","q",lbound3,rbound3);

	gaus4->SetParameter(0,0.24);
	gaus4->SetParameter(1,2);
	gaus4->SetParameter(2,26);
//	gaus4->SetParameter(3,0.13);

	auto fitres4=eta4->Fit(gaus4,"S","q",lbound4,rbound4);

	gaus5->SetParameter(0,0.25);
	gaus5->SetParameter(1,2);
	gaus5->SetParameter(2,25);
//	gaus5->SetParameter(3,0.15);


	TCanvas* bsp_fit= new TCanvas("bsp_fit","bsp_fit",900,600);
	eta5->SetTitle("Gaus-Fit zur Bestimmung der Halbwertsbreite");
	bsp_fit->Divide(1,2,0,0);

	bsp_fit->cd(1);
	bsp_fit->GetPad(1)->SetPad(0.,0.4,1.,1.);

	eta5->GetXaxis()->SetLimits(lbound5-0.1,rbound5+0.1);
	eta5->GetYaxis()->SetRangeUser(0.24,0.45);
	eta5->Draw("APE");	

	TLegend* leg_fit=new TLegend(0.1,0.7,0.3,0.9);
	leg_fit->AddEntry("eta5","Fitdaten","EL");
	leg_fit->AddEntry(gaus5,"Fit");
	leg_fit->SetFillColor(0);
	leg_fit->Draw("same");


	auto fitres5=eta5->Fit(gaus5,"S","q",lbound5,rbound5);

	double res[n5/2];
	double res_err[n5/2];

	for(int i=0;i<n5/2;i++){
		res[i]=intent5[i]-gaus5->Eval(t5[i]);
		res_err[i]=sqrt(pow(err_intent5[i],2));//+pow(,2));
	}

	bsp_fit->cd();
	bsp_fit->cd(2);

	bsp_fit->GetPad(2)->SetPad(0.,0.,1.,0.4);
	TGraphErrors* residuen=new TGraphErrors(n5/2,t5,res,nullptr,res_err);
	
	bsp_fit->GetPad(2)->SetGridy();

	residuen->SetTitle(" ");
	residuen->GetXaxis()->SetTitle("Zeit [ps]");
	residuen->GetYaxis()->SetTitle("Residuum");
	residuen->GetXaxis()->SetLimits(lbound5-0.1,rbound5+0.1);
	residuen->GetXaxis()->SetTitleSize(0.052);
	residuen->GetXaxis()->SetLabelSize(0.052);
	residuen->GetYaxis()->SetTitleSize(0.052);
	residuen->GetYaxis()->SetLabelSize(0.052);
	residuen->GetYaxis()->SetRangeUser(-0.01,0.012);
	residuen->Draw("APE");

	TLine* res_line= new TLine(lbound5-0.1,0,rbound5+0.1,0);
	res_line->SetLineColor(kRed);
	res_line->Draw("same");

	TLegend* leg_res= new TLegend(0.8,0.8,0.98,0.98);
	leg_res->AddEntry("residuen","Residuum+Fehler","EP");
	leg_res->SetFillColor(0);
	leg_res->Draw("same");

	bsp_fit->SaveAs("../Protokoll/Protokoll_Data/bsp_fit.pdf","pdf");

	double breite1= 2*sqrt(2*log(2))*gaus1->GetParameter(2);
	double breite2= 2*sqrt(2*log(2))*gaus2->GetParameter(2);
	double breite3= 2*sqrt(2*log(2))*gaus3->GetParameter(2);
	double breite4= 2*sqrt(2*log(2))*gaus4->GetParameter(2);
	double breite5= 2*sqrt(2*log(2))*gaus5->GetParameter(2);

	double err_breite1=2*sqrt(2*log(2))*gaus1->GetParError(2);
	double err_breite2=2*sqrt(2*log(2))*gaus2->GetParError(2);
	double err_breite3=2*sqrt(2*log(2))*gaus3->GetParError(2);
	double err_breite4=2*sqrt(2*log(2))*gaus4->GetParError(2);
	double err_breite5=2*sqrt(2*log(2))*gaus5->GetParError(2);

	

	double puls1= -2*sqrt(log(2))*gaus1->GetParameter(2);
	double puls2= 2*sqrt(log(2))*gaus2->GetParameter(2);
	double puls3= 2*sqrt(log(2))*gaus3->GetParameter(2);
	double puls4= 2*sqrt(log(2))*gaus4->GetParameter(2);
	double puls5= 2*sqrt(log(2))*gaus5->GetParameter(2);

	double err_puls1=2*sqrt(log(2))*gaus1->GetParError(2);
	double err_puls2=2*sqrt(log(2))*gaus2->GetParError(2);
	double err_puls3=2*sqrt(log(2))*gaus3->GetParError(2);
	double err_puls4=2*sqrt(log(2))*gaus4->GetParError(2);
	double err_puls5=2*sqrt(log(2))*gaus5->GetParError(2);

	double n_points[5]={0,1,2,3,4};
	double puls[5]={puls5,puls4,puls3,puls2,puls1};	
	double err_puls[5]={err_puls5,err_puls4,err_puls3,err_puls2,err_puls1};	
	
	TGraphErrors* verkippung= new TGraphErrors(5,n_points,puls,nullptr,err_puls);
	verkippung->SetTitle("Pulsdauern-Verkippung");
	verkippung->GetYaxis()->SetTitle("Pulsdauer [ps]");
	verkippung->GetXaxis()->SetTitle("Verkippung");
	verkippung->GetXaxis()->SetLimits(-1,5);

	TCanvas* c_verkippung= new TCanvas("c_verkippung","c_Verkippung",900,600);
	verkippung->Draw();
	c_verkippung->SaveAs("../Protokoll/Protokoll_Data/verkippung.pdf","pdf");	
	double p_bar=390;
	double err_p_bar=2;

	double t_rep=9.993e3;
	double err_t_rep=4;

	double p_peak=p_bar*t_rep/(puls5*0.94);
	double err_p_peak=p_peak*sqrt(pow(err_t_rep/t_rep,2)+pow(err_p_bar/p_bar,2)+pow(err_puls5/puls5,2));	
	
	cout<<p_peak<<"+/-"<<err_p_peak<<endl;

	TF1* leistung = new TF1("leistung","[0]*TMath::Gaus(x,[1],[2])",-70,70);
	leistung->SetTitle("Pulsverlauf;Zeit [ps];momentane Leistung [W]");
	leistung->SetParameter(0,p_peak/1000.);
	leistung->SetParameter(1,0);
	leistung->SetParameter(2,puls5/(2*sqrt(2*log(2))));	

	TCanvas* c_leistung= new TCanvas("c_leistung","c_leistung",900,600);
	leistung->Draw();
	c_leistung->SaveAs("../Protokoll/Protokoll_Data/leistung.pdf","pdf");
	const int k1= read_mca("../Data/2Tag/spektrum_etalon1.txt")[0];
	const int k2= read_mca("../Data/2Tag/spektrum_etalon2.txt")[0];
	const int k3= read_mca("../Data/2Tag/spektrum_etalon3.txt")[0];
	const int k4= read_mca("../Data/2Tag/spektrum_etalon4.txt")[0];
	const int k5= read_mca("../Data/2Tag/spektrum_etalon5.txt")[0];

	double * spektren1=& read_mca("../Data/2Tag/spektrum_etalon1.txt")[1];
	double * spektren2=& read_mca("../Data/2Tag/spektrum_etalon2.txt")[1];
	double * spektren3=& read_mca("../Data/2Tag/spektrum_etalon3.txt")[1];
	double * spektren4=& read_mca("../Data/2Tag/spektrum_etalon4.txt")[1];
	double * spektren5=& read_mca("../Data/2Tag/spektrum_etalon5.txt")[1];


	double spt_intent1 [k1/2];
	double err_spt_intent1 [k1/2];
	
	double log_spt1[k1/2];
	double err_log_spt1[k1/2];

	double wl1 [k1/2];
//	double err_wl1 [k1/2];

	double spt_intent2 [k2/2];
	double err_spt_intent2 [k2/2];
	
	double log_spt2[k2/2];
	double err_log_spt2[k2/2];

	double wl2 [k2/2];
//	double err_wl2 [k2/2];

	double spt_intent3 [k3/2];
	double err_spt_intent3 [k3/2];

	double log_spt3[k3/2];
	double err_log_spt3[k3/2];

	double wl3 [k3/2];
//	double err_wl3 [k3/2];

	double spt_intent4 [k4/2];
	double err_spt_intent4 [k4/2];

	double log_spt4[k4/2];
	double err_log_spt4[k4/2];

	double wl4 [k4/2];
//	double err_wl4 [k4/2];

	double spt_intent5 [k5/2];
	double err_spt_intent5 [k5/2];

	double log_spt5[k5/2];
	double err_log_spt5[k5/2];

	double wl5 [k5/2];
//	double err_wl5 [k5/2];

        for (int i=0;i<k1;i++){
                spt_intent1[i/2]=spektren1[i+1];
                wl1[i/2]=spektren1[i];
		log_spt1[i/2]=log10(spektren1[i+1]);
		i++;
	}
        for (int i=0;i<k2;i++){        
                spt_intent2[i/2]=spektren2[i+1];
                wl2[i/2]=spektren2[i];
		log_spt2[i/2]=log10(spektren2[i+1]);
		i++;
        }        

	for (int i=0;i<k3;i++){
                spt_intent3[i/2]=spektren3[i+1];
                wl3[i/2]=spektren3[i];
		log_spt3[i/2]=log10(spektren3[i+1]);
                i++;
	}

	for (int i=0;i<k4;i++){
                spt_intent4[i/2]=spektren4[i+1];
                wl4[i/2]=spektren4[i];
		log_spt4[i/2]=log10(spektren4[i+1]);
                i++;
	}
	
	for (int i=0;i<k5;i++){
                spt_intent5[i/2]=spektren5[i+1];
                wl5[i/2]=spektren5[i];
		log_spt5[i/2]=log10(spektren5[i+1]);
		i++;
                
        }



	begin1 =2750;
	end1 =k1/2;
	temp1=0;
	var_temp1=0;

	for (int i=begin1;i<end1;i++){
		temp1+=spt_intent1[i]/(end1-begin1-1);
	}

	for (int i=begin1;i<end1;i++){
		var_temp1+=pow(temp1-spt_intent1[i],2)/(end1-begin1-2);
	}

	for (int i=0;i<k1/2;i++){
		err_spt_intent1[i]=sqrt(var_temp1);
		err_log_spt1[i]=err_spt_intent1[i]/(spt_intent1[i]*log(10));
//		err_wl1[i]=0.001; //wlfehler abschätzen oder vernchlässigen?
	}
		

	begin2 =2750;
	end2 =k2/2;
	temp2=0;
	var_temp2=0;

	for (int i=begin2;i<end2;i++){
		temp2+=spt_intent2[i]/(end2-begin2-1);
	}

	for (int i=begin2;i<end2;i++){
		var_temp2+=pow(temp2-spt_intent2[i],2)/(end2-begin2-2);
	}

	for (int i=0;i<k2/2;i++){
		err_spt_intent2[i]=sqrt(var_temp2);
		err_log_spt2[i]=err_spt_intent2[i]/(spt_intent2[i]*log(10));
//		err_wl2[i]=0.001; //zeitfehler abschätzen oder vernchlässigen?
	}



	begin3 =2760;
	end3 =k3/2;
	temp3=0;
	var_temp3=0;

	for (int i=begin3;i<end3;i++){
		temp3+=spt_intent3[i]/(end3-begin3-1);
	}

	for (int i=begin3;i<end3;i++){
		var_temp3+=pow(temp3-spt_intent3[i],2)/(end3-begin3-2);
	}

	for (int i=0;i<k3/2;i++){
		err_spt_intent3[i]=sqrt(var_temp3);
		err_log_spt3[i]=err_spt_intent3[i]/(spt_intent3[i]*log(10));
//		err_wl3[i]=0.001; //zeitfehler abschätzen oder vernchlässigen?
	}



	begin4 =2770;
	end4 =k4/2;
	temp4=0;
	var_temp4=0;

	for (int i=begin4;i<end4;i++){
		temp4+=spt_intent4[i]/(end4-begin4-1);
	}

	for (int i=begin4;i<end4;i++){
		var_temp4+=pow(temp4-spt_intent4[i],2)/(end4-begin4-2);
	}

	for (int i=0;i<k4/2;i++){
		err_spt_intent4[i]=sqrt(var_temp4);
		err_log_spt4[i]=err_spt_intent4[i]/(spt_intent4[i]*log(10));
//		err_wl4[i]=0.001; //zeitfehler abschätzen oder vernchlässigen?
	}



	begin5 =2790;
	end5 =k5/2;
	temp5=0;
	var_temp5=0;

	for (int i=begin5;i<end5;i++){
		temp5+=spt_intent5[i]/(end5-begin5-1);
	}

	for (int i=begin5;i<end5;i++){
		var_temp5+=pow(temp5-spt_intent5[i],2)/(end5-begin5-2);
	}

	for (int i=0;i<k5/2;i++){
		err_spt_intent5[i]=sqrt(var_temp5);
		err_log_spt5[i]=err_spt_intent5[i]/(spt_intent5[i]*log(10));
//		err_wl5[i]=0.001; //zeitfehler abschätzen oder vernchlässigen?
	}

	TGraphErrors* spk1= new TGraphErrors(k1/2,wl1,spt_intent1,nullptr,err_spt_intent1);
	spk1->SetTitle("Spektrum Messung 5");
	spk1->GetXaxis()->SetTitle("Wellenlaenge [nm]");
	spk1->GetYaxis()->SetTitle("rel. Intetnsitaet");

	TF1* spk_gaus1= new TF1("spk_gaus1","[0]*TMath::Gaus(x,[1],[2])+0.00315499");
	TCanvas* c_spk1= new TCanvas("c_spk1","c_spk1",900,600);
	spk1->Draw("APE");

	TLegend* leg_spek1= new TLegend(0.1,0.7,0.3,0.9);
	leg_spek1->AddEntry("spk1","Daten","EPL");
	leg_spek1->SetFillColor(0);
	leg_spek1->Draw("same");

	//c_spk1->SaveAs("../Protokoll/Protokoll_Data/spek_5.pdf","pdf");


	TGraphErrors* spk2= new TGraphErrors(k2/2,wl2,spt_intent2,nullptr,err_spt_intent2);
	spk2->SetTitle("Spektrum Messung 4");
	spk2->GetXaxis()->SetTitle("Wellenlaenge [nm]");
	spk2->GetYaxis()->SetTitle("rel. Intetnsitaet");

	TF1* spk_gaus2= new TF1("spk_gaus2","[0]*TMath::Gaus(x,[1],[2])+0.00305165");
	TCanvas* c_spk2= new TCanvas("c_spk2","c_spk2",900,600);
	spk2->Draw("APE");

	TLegend* leg_spek2= new TLegend(0.1,0.7,0.3,0.9);
	leg_spek2->AddEntry("spk2","Daten","EPL");
	leg_spek2->SetFillColor(0);
	leg_spek2->Draw("same");

	//c_spk2->SaveAs("../Protokoll/Protokoll_Data/spek_4.pdf","pdf");

	TGraphErrors* spk3= new TGraphErrors(k3/2,wl3,spt_intent3,nullptr,err_spt_intent3);
	spk3->SetTitle("Spektrum Messung 3");
	spk3->GetXaxis()->SetTitle("Wellenlaenge [nm]");
	spk3->GetYaxis()->SetTitle("rel. Intetnsitaet");

	TF1* spk_gaus3= new TF1("spk_gaus3","[0]*TMath::Gaus(x,[1],[2])+0.00230664");
	TCanvas* c_spk3= new TCanvas("c_spk3","c_spk3",900,600);
	spk3->Draw("APE");


	TLegend* leg_spek3= new TLegend(0.1,0.7,0.3,0.9);
	leg_spek3->AddEntry("spk3","Daten","EPL");
	leg_spek3->SetFillColor(0);
	leg_spek3->Draw("same");

	//c_spk3->SaveAs("../Protokoll/Protokoll_Data/spek_3.pdf","pdf");

	TGraphErrors* spk4= new TGraphErrors(k4/2,wl4,spt_intent4,nullptr,err_spt_intent4);
	spk4->SetTitle("Spektrum Messung 2");
	spk4->GetXaxis()->SetTitle("Wellenlaenge [nm]");
	spk4->GetYaxis()->SetTitle("rel. Intetnsitaet");

	TF1* spk_gaus4= new TF1("spk_gaus4","[0]*TMath::Gaus(x,[1],[2])+0.00267057");
	TCanvas* c_spk4= new TCanvas("c_spk4","c_spk4",900,600);
	spk4->Draw("APE");

	TLegend* leg_spek4= new TLegend(0.1,0.7,0.3,0.9);
	leg_spek4->AddEntry("spk4","Daten","EPL");
	leg_spek4->SetFillColor(0);
	leg_spek4->Draw("same");

	//c_spk4->SaveAs("../Protokoll/Protokoll_Data/spek_2.pdf","pdf");

	TGraphErrors* spk5= new TGraphErrors(k5/2,wl5,spt_intent5,nullptr,err_spt_intent5);
	spk5->SetTitle("Spektrum Messung 1");
	spk5->GetXaxis()->SetTitle("Wellenlaenge [nm]");
	spk5->GetYaxis()->SetTitle("rel. Intetnsitaet");


	rbound1=1064.67;
	lbound1=1064.625;

	rbound2=1064.652;
	lbound2=1064.602;

	rbound3=1064.634;
	lbound3=1064.579;

	rbound4=1064.6;
	lbound4=1064.525;

	rbound5=1064.54;
	lbound5=1064.45;

	TF1* spk_gaus5= new TF1("spk_gaus5","[0]*TMath::Gaus(x,[1],[2])+0.00256277",lbound5,rbound5);
	TCanvas* c_spk5= new TCanvas("c_spk5","c_spk5",900,600);
	spk5->Draw("APE");


	

	TLine* left_spek5= new TLine(lbound5,-0.045,lbound5,0.58);
	left_spek5->SetLineColor(kRed);
	left_spek5->Draw("same");	

	TLine* right_spek5= new TLine(rbound5,-0.045,rbound5,0.58);
	right_spek5->SetLineColor(kRed);
	right_spek5->Draw("same");

	TBox* box_spek5= new TBox(wl5[begin5],-0.05,wl5[end5-1],0.58);
	box_spek5->SetFillStyle(3004);
	box_spek5->SetFillColor(4);
	box_spek5->Draw("same");


	TLegend* leg_spek5= new TLegend(0.1,0.6,0.4,0.9);
	leg_spek5->AddEntry("spk5","Daten","EPL");
	leg_spek5->AddEntry(box_spek5,"Rauschbereich zur Abschaetzung der Fehler","F");
	leg_spek5->AddEntry(left_spek5,"Fitbereich","L");
	leg_spek5->SetFillColor(0);
	leg_spek5->Draw("same");


	c_spk5->SaveAs("../Protokoll/Protokoll_Data/spek_1.pdf","pdf");



	spk_gaus1->SetParameter(0,0.58);
	spk_gaus1->SetParameter(1,1064.65);
	spk_gaus1->SetParameter(2,0.05);
//	spk_gaus1->SetParameter(3,0.7);

	auto fitres_spek1=spk1->Fit(spk_gaus1,"S","q",lbound1,rbound1);


	spk_gaus2->SetParameter(0,0.53);
	spk_gaus2->SetParameter(1,1064.63);
	spk_gaus2->SetParameter(2,0.06);
//	spk_gaus2->SetParameter(3,0.09);


	auto fitres_spek2=spk2->Fit(spk_gaus2,"S","q",lbound2,rbound2);

	spk_gaus3->SetParameter(0,0.498623);
	spk_gaus3->SetParameter(1,1064.61);
	spk_gaus3->SetParameter(2,0.038807);
//	spk_gaus3->SetParameter(3,0.12);

	auto fitres_spek3=spk3->Fit(spk_gaus3,"S","q",lbound3,rbound3);

	spk_gaus4->SetParameter(0,0.5);
	spk_gaus4->SetParameter(1,1064.565);
	spk_gaus4->SetParameter(2,0.067);
//	spk_gaus4->SetParameter(3,0.13);

	auto fitres_spek4=spk4->Fit(spk_gaus4,"S","q",lbound4,rbound4);

	spk_gaus5->SetParameter(0,0.5);
	spk_gaus5->SetParameter(1,1064.5);
	spk_gaus5->SetParameter(2,0.07);
//	spk_gaus5->SetParameter(3,0.15);


	TCanvas* bsp_fit_spek= new TCanvas("bsp_fit_spek","bsp_fit_spek",900,600);

	bsp_fit_spek->Divide(1,2,0,0);
	spk5->SetTitle("Gaus-Fit zur Bestimmung der Bandbreite");
	bsp_fit_spek->cd(1);
	bsp_fit_spek->GetPad(1)->SetPad(0.,0.4,1.,1.);

	spk5->GetXaxis()->SetLimits(lbound5,rbound5);
	spk5->GetYaxis()->SetRangeUser(0.39,0.7);
	spk5->Draw("APE");	

	TLegend* leg_spek_fit=new TLegend(0.1,0.7,0.3,0.9);
	leg_spek_fit->AddEntry("spk5","Fitdaten","EL");
	leg_spek_fit->AddEntry(spk_gaus5,"Fit");
	leg_spek_fit->SetFillColor(0);
	leg_spek_fit->Draw("same");


	auto fitres_spek5=spk5->Fit(spk_gaus5,"S","q",lbound5,rbound5);

	double res_spek[k5/2];
	double res_spek_err[k5/2];

	for(int i=0;i<k5/2;i++){
		res_spek[i]=spt_intent5[i]-spk_gaus5->Eval(wl5[i]);
		res_spek_err[i]=sqrt(pow(err_spt_intent5[i],2));//+pow(,2));
	}

	bsp_fit_spek->cd();
	bsp_fit_spek->cd(2);

	bsp_fit_spek->GetPad(2)->SetPad(0.,0.,1.,0.4);
	TGraphErrors* residuen_spek=new TGraphErrors(k5/2,wl5,res_spek,nullptr,res_spek_err);
	
	bsp_fit_spek->GetPad(2)->SetGridy();

//	cout<<temp1<<endl<<temp2<<endl<<temp3<<endl<<temp4<<endl<<temp5<<endl;

	residuen_spek->SetTitle(" ");
	residuen_spek->GetXaxis()->SetTitle("Wellenlaenge [nm]");
	residuen_spek->GetYaxis()->SetTitle("Residuum");
	
	residuen_spek->GetXaxis()->SetLimits(lbound5,rbound5);
	residuen_spek->GetXaxis()->SetTitleSize(0.052);
	residuen_spek->GetXaxis()->SetLabelSize(0.052);
	residuen_spek->GetYaxis()->SetTitleSize(0.052);
	residuen_spek->GetYaxis()->SetLabelSize(0.052);
	residuen_spek->GetYaxis()->SetRangeUser(-0.01,0.01);
	residuen_spek->Draw("APE");

	TLine* res_line_spek= new TLine(lbound5,0,rbound5,0);
	res_line_spek->SetLineColor(kRed);
	res_line_spek->Draw("same");

	TLegend* leg_res_spek= new TLegend(0.8,0.8,0.98,0.98);
	leg_res_spek->AddEntry("residuen_spek","Residuum+Fehler","EP");
	leg_res_spek->SetFillColor(0);
	leg_res_spek->Draw("same");

	bsp_fit_spek->SaveAs("../Protokoll/Protokoll_Data/bsp_fit_spek.pdf","pdf");


	

	double freq1= 2*sqrt(2*log(2))*spk_gaus1->GetParameter(2);
	double freq2= 2*sqrt(2*log(2))*spk_gaus2->GetParameter(2);
	double freq3= 2*sqrt(2*log(2))*spk_gaus3->GetParameter(2);
	double freq4= 2*sqrt(2*log(2))*spk_gaus4->GetParameter(2);
	double freq5= 2*sqrt(2*log(2))*spk_gaus5->GetParameter(2);

	double err_freq1=2*sqrt(2*log(2))*spk_gaus1->GetParError(2);
	double err_freq2=2*sqrt(2*log(2))*spk_gaus2->GetParError(2);
	double err_freq3=2*sqrt(2*log(2))*spk_gaus3->GetParError(2);
	double err_freq4=2*sqrt(2*log(2))*spk_gaus4->GetParError(2);
	double err_freq5=2*sqrt(2*log(2))*spk_gaus5->GetParError(2);

	double mw1=spk_gaus1->GetParameter(1);
	double mw2=spk_gaus2->GetParameter(1);
	double mw3=spk_gaus3->GetParameter(1);
	double mw4=spk_gaus4->GetParameter(1);
	double mw5=spk_gaus5->GetParameter(1);

	
	double err_mw1=spk_gaus1->GetParError(1);
	double err_mw2=spk_gaus2->GetParError(1);
	double err_mw3=spk_gaus3->GetParError(1);
	double err_mw4=spk_gaus4->GetParError(1);
	double err_mw5=spk_gaus5->GetParError(1);

	double freq[5]={freq5,freq4,freq3,freq2,freq1};	
	double err_freq[5]={err_freq5,err_freq4,err_freq3,err_freq2,err_freq1};	
	double mw[5]={mw5,mw4,mw3,mw2,mw1};
	double err_mw[5]={err_mw5,err_mw4,err_mw3,err_mw2,err_mw1};


	double pf_produkt[5];
	double err_pf_produkt[5];

	double c_0=299792458e-3;

	for (int i=0;i<5;i++){
		pf_produkt[i]=puls[i]*c_0*freq[i]/pow(mw[i],2);
		err_pf_produkt[i]=pf_produkt[i]*sqrt(pow(2*err_mw[i]/(mw[i]*mw[i]),2)+pow(err_puls[i]/puls[i],2)+pow(err_freq[i]/freq[i],2));
	

	}


	TGraphErrors* log_spek1= new TGraphErrors(k1/2,wl1,log_spt1,nullptr,err_log_spt1);
	log_spek1->SetTitle("Messung 5 logarithmiert");
	log_spek1->GetXaxis()->SetTitle("Wellenlaenge [nm]");
	log_spek1->GetYaxis()->SetTitle("log_{10}(I)");

	TCanvas* c_log_spek1= new TCanvas("c_log_spek1","c_log_spek1",900,600);
	log_spek1->Draw("APE");
	
	double log_level1=-2.02;
	double err_log_level1=-0.01;


	TLine* r_niveau1= new TLine(1055,log_level1,1077,log_level1);
	r_niveau1->SetLineColor(kRed);
	r_niveau1->Draw("same");
	
	TLegend* leg_log_spek1= new TLegend(0.1,0.7,0.3,0.9);
	leg_log_spek1->AddEntry("log_spek1","Daten","EPL");
	leg_log_spek1->AddEntry(r_niveau1,"Rauschniveau","L");
	leg_log_spek1->SetFillColor(0);
	leg_log_spek1->Draw("same");
	
	c_log_spek1->SaveAs("../Protokoll/Protokoll_Data/rausch1.pdf","pdf");

	TGraphErrors* log_spek2= new TGraphErrors(k2/2,wl2,log_spt2,nullptr,err_log_spt2);
	log_spek2->SetTitle("Messung 4 logarithmiert");
	log_spek2->GetXaxis()->SetTitle("Wellenlaenge [nm]");
	log_spek2->GetYaxis()->SetTitle("log_{10}(I)");

	TCanvas* c_log_spek2= new TCanvas("c_log_spek2","c_log_spek2",900,600);
	log_spek2->Draw("APE");
	
	double log_level2=-1.99;
	double err_log_level2=-0.01;

	TLine* r_niveau2= new TLine(1055,log_level2,1077,log_level2);
	r_niveau2->SetLineColor(kRed);
	r_niveau2->Draw("same");
	
	TLegend* leg_log_spek2= new TLegend(0.1,0.7,0.3,0.9);
	leg_log_spek2->AddEntry("log_spek2","Daten","EPL");
	leg_log_spek2->AddEntry(r_niveau2,"Rauschniveau","L");
	leg_log_spek2->SetFillColor(0);
	leg_log_spek2->Draw("same");
	c_log_spek2->SaveAs("../Protokoll/Protokoll_Data/rausch2.pdf","pdf");

	TGraphErrors* log_spek3= new TGraphErrors(k3/2,wl3,log_spt3,nullptr,err_log_spt3);
	log_spek3->SetTitle("Messung 3 logarithmiert");
	log_spek3->GetXaxis()->SetTitle("Wellenlaenge [nm]");
	log_spek3->GetYaxis()->SetTitle("log_{10}(I)");

	TCanvas* c_log_spek3= new TCanvas("c_log_spek3","c_log_spek3",900,600);
	log_spek3->Draw("APE");
	
	double log_level3=-2.1;
	double err_log_level3=0.01;

	TLine* r_niveau3= new TLine(1055,log_level3,1077,log_level3);
	r_niveau3->SetLineColor(kRed);
	r_niveau3->Draw("same");
	
	TLegend* leg_log_spek3= new TLegend(0.1,0.7,0.3,0.9);
	leg_log_spek3->AddEntry("log_spek3","Daten","EPL");
	leg_log_spek3->AddEntry(r_niveau3,"Rauschniveau","L");
	leg_log_spek3->SetFillColor(0);
	leg_log_spek3->Draw("same");
	c_log_spek3->SaveAs("../Protokoll/Protokoll_Data/rausch3.pdf","pdf");

	TGraphErrors* log_spek4= new TGraphErrors(k4/2,wl4,log_spt4,nullptr,err_log_spt4);
	log_spek4->SetTitle("Messung 2 logarithmiert");
	log_spek4->GetXaxis()->SetTitle("Wellenlaenge [nm]");
	log_spek4->GetYaxis()->SetTitle("log_{10}(I)");

	TCanvas* c_log_spek4= new TCanvas("c_log_spek4","c_log_spek4",900,600);
	log_spek4->Draw("APE");
	
	double log_level4=-2.1;
	double err_log_level4=0.01;

	TLine* r_niveau4= new TLine(1055,log_level4,1077,log_level4);
	r_niveau4->SetLineColor(kRed);
	r_niveau4->Draw("same");
	
	TLegend* leg_log_spek4= new TLegend(0.1,0.7,0.3,0.9);
	leg_log_spek4->AddEntry("log_spek4","Daten","EPL");
	leg_log_spek4->AddEntry(r_niveau4,"Rauschniveau","L");
	leg_log_spek4->SetFillColor(0);
	leg_log_spek4->Draw("same");

	c_log_spek4->SaveAs("../Protokoll/Protokoll_Data/rausch4.pdf","pdf");
	
	TGraphErrors* log_spek5= new TGraphErrors(k5/2,wl5,log_spt5,nullptr,err_log_spt5);
	log_spek5->SetTitle("Messung 1 logarithmiert");
	log_spek5->GetXaxis()->SetTitle("Wellenlaenge [nm]");
	log_spek5->GetYaxis()->SetTitle("log_{10}(I)");

	TCanvas* c_log_spek5= new TCanvas("c_log_spek5","c_log_spek5",900,600);
	log_spek5->Draw("APE");
	
	double log_level5=-2.06;
	double err_log_level5=0.01;


	TLine* r_niveau5= new TLine(1055,log_level5,1077,log_level5);
	r_niveau5->SetLineColor(kRed);
	r_niveau5->Draw("same");
	
	TLegend* leg_log_spek5= new TLegend(0.1,0.7,0.3,0.9);
	leg_log_spek5->AddEntry("log_spek5","Daten","EPL");
	leg_log_spek5->AddEntry(r_niveau5,"Rauschniveau","L");
	leg_log_spek5->SetFillColor(0);
	leg_log_spek5->Draw("same");

	c_log_spek5->SaveAs("../Protokoll/Protokoll_Data/rausch5.pdf","pdf");


	double level1=pow(10,log_level1);
	double err_level1=err_log_level1*pow(10,log_level1)*log(10);

	double level2=pow(10,log_level2);
	double err_level2=err_log_level2*pow(10,log_level2)*log(10);

	double level3=pow(10,log_level3);
	double err_level3=err_log_level3*pow(10,log_level3)*log(10);

	double level4=pow(10,log_level4);
	double err_level4=err_log_level4*pow(10,log_level4)*log(10);

	double level5=pow(10,log_level5);
	double err_level5=err_log_level5*pow(10,log_level5)*log(10);


	double x1_1=1064.25;
	double err_x1_1=0.005;	

	double x2_1=1065;
	double err_x2_1=0.005;
	
/*
	TF1* signal1= new TF1("signal1","[0]*TMath::Gaus(x,[1],[2])",x1_1-1,x2_1+1);
	signal1->SetParameter(0,spk_gaus1->GetParameter(0));
	signal1->SetParameter(1,spk_gaus1->GetParameter(1));
	signal1->SetParameter(2,spk_gaus1->GetParameter(2));
*/		
	double s1=spk_gaus1->Integral(x1_1,x2_1)-level1*(x2_1-x1_1);
	double err_s1=sqrt(pow(spk_gaus1->IntegralError(x1_1,x2_1,fitres_spek1->GetParams(),fitres_spek1->GetCovarianceMatrix().GetMatrixArray()),2)+pow(err_level1*(x2_1-x1_1),2)+pow(level1*err_x2_1,2)+pow(level1*err_x1_1,2));

	double s1_bar=s1/(x2_1-x1_1);
	double err_s1_bar=s1_bar*sqrt(pow(err_s1/s1,2)+pow(err_x1_1/(x2_1-x1_1),2)+pow(err_x2_1/(x2_1-x1_1),2));

	double snr1=s1_bar/err_spt_intent1[0];
	double err_snr1=err_s1_bar/err_spt_intent1[0];

	
	double x1_2=1064.22;
	double err_x1_2=0.005;	

	double x2_2=1064.92;
	double err_x2_2=0.005;
	
/*
	TF1* signal2= new TF1("signal2","[0]*TMath::Gaus(x,[1],[2])",x1_2-1,x2_2+1);
	signal2->SetParameter(0,spk_gaus2->GetParameter(0));
	signal2->SetParameter(1,spk_gaus2->GetParameter(1));
	signal2->SetParameter(2,spk_gaus2->GetParameter(2));
*/		
	double s2=spk_gaus2->Integral(x1_2,x2_2)-level2*(x2_2-x1_2);
	double err_s2=sqrt(pow(spk_gaus2->IntegralError(x1_2,x2_2,fitres_spek2->GetParams(),fitres_spek2->GetCovarianceMatrix().GetMatrixArray()),2)+pow(err_level2*(x2_2-x1_2),2)+pow(level2*err_x2_2,2)+pow(level2*err_x1_2,2));

	double s2_bar=s2/(x2_2-x1_2);
	double err_s2_bar=s2_bar*sqrt(pow(err_s2/s2,2)+pow(err_x1_2/(x2_2-x1_2),2)+pow(err_x2_2/(x2_2-x1_2),2));

	double snr2=s2_bar/err_spt_intent2[0];
	double err_snr2=err_s2_bar/err_spt_intent2[0];

	
	double x1_3=1064;
	double err_x1_3=0.005;	

	double x2_3=1064.96;
	double err_x2_3=0.005;
	
/*
	TF1* signal3= new TF1("signal3","[0]*TMath::Gaus(x,[1],[2])",x1_3-1,x2_3+1);
	signal3->SetParameter(0,spk_gaus3->GetParameter(0));
	signal3->SetParameter(1,spk_gaus3->GetParameter(1));
	signal3->SetParameter(2,spk_gaus3->GetParameter(2));
*/		
	double s3=spk_gaus3->Integral(x1_3,x2_3)-level3*(x2_3-x1_3);
	double err_s3=sqrt(pow(spk_gaus3->IntegralError(x1_3,x2_3,fitres_spek3->GetParams(),fitres_spek3->GetCovarianceMatrix().GetMatrixArray()),2)+pow(err_level3*(x2_3-x1_3),2)+pow(level3*err_x2_3,2)+pow(level3*err_x1_3,2));

	double s3_bar=s3/(x2_3-x1_3);
	double err_s3_bar=s3_bar*sqrt(pow(err_s3/s3,2)+pow(err_x1_3/(x2_3-x1_3),2)+pow(err_x2_3/(x2_3-x1_3),2));

	double snr3=s3_bar/err_spt_intent3[0];
	double err_snr3=err_s3_bar/err_spt_intent3[0];

	
	double x1_4=1064.14;
	double err_x1_4=0.005;	

	double x2_4=1064.91;
	double err_x2_4=0.005;
/*	

	TF1* signal4= new TF1("signal4","[0]*TMath::Gaus(x,[1],[2])",x1_4-1,x2_4+1);
	signal4->SetParameter(0,spk_gaus4->GetParameter(0));
	signal4->SetParameter(1,spk_gaus4->GetParameter(1));
	signal4->SetParameter(2,spk_gaus4->GetParameter(2));
*/		
	double s4=spk_gaus4->Integral(x1_4,x2_4)-level4*(x2_4-x1_4);
	double err_s4=sqrt(pow(spk_gaus4->IntegralError(x1_4,x2_4,fitres_spek4->GetParams(),fitres_spek4->GetCovarianceMatrix().GetMatrixArray()),2)+pow(err_level4*(x2_4-x1_4),2)+pow(level4*err_x2_4,2)+pow(level4*err_x1_4,2));

	double s4_bar=s4/(x2_4-x1_4);
	double err_s4_bar=s4_bar*sqrt(pow(err_s4/s4,2)+pow(err_x1_4/(x2_4-x1_4),2)+pow(err_x2_4/(x2_4-x1_4),2));

	double snr4=s4_bar/err_spt_intent4[0];
	double err_snr4=err_s4_bar/err_spt_intent4[0];

	
	double x1_5=1064.1;
	double err_x1_5=0.005;	

	double x2_5=1064.82;
	double err_x2_5=0.005;
	
/*
	TF1* signal5= new TF1("signal5","[0]*TMath::Gaus(x,[1],[2])",x1_5-1,x2_5+1);
	signal5->SetParameter(0,spk_gaus5->GetParameter(0));
	signal5->SetParameter(1,spk_gaus5->GetParameter(1));
	signal5->SetParameter(2,spk_gaus5->GetParameter(2));
*/		
	double s5=spk_gaus5->Integral(x1_5,x2_5) - level5*(x2_5-x1_5);
	double err_s5=sqrt(pow(spk_gaus5->IntegralError(x1_5,x2_5,fitres_spek5->GetParams(),fitres_spek5->GetCovarianceMatrix().GetMatrixArray()),2)+pow(err_level5*(x2_5-x1_5),2)+pow(level5*err_x2_5,2)+pow(level5*err_x1_5,2));

	double s5_bar=s5/(x2_5-x1_5);
	double err_s5_bar=s5_bar*sqrt(pow(err_s5/s5,2)+pow(err_x1_5/(x2_5-x1_5),2)+pow(err_x2_5/(x2_5-x1_5),2));

	double snr5=s5_bar/err_spt_intent5[0];
	double err_snr5=err_s5_bar/err_spt_intent5[0];

	double snr[5]={snr5,snr4,snr3,snr2,snr1};
	double err_snr[5]={err_snr5,err_snr4,err_snr3,err_snr2,err_snr1};

	double log_level[5]={log_level5,log_level4,log_level3,log_level2,log_level1};
	double err_log_level[5]={err_log_level5,err_log_level4,err_log_level3,err_log_level2,err_log_level1};


	for(int i=0;i<5;i++){
		cout<<"pulsbreite "<<i+1<<"in ps="<<puls[i]<<"+/-"<<err_puls[i]<<endl<<endl;
		cout<<"frequenzbreite "<<i+1<<"in ="<<c_0*freq[i]/pow(mw[i],2)<<"+/-"<<c_0*freq[i]/pow(mw[i],2)*sqrt(pow(2*err_mw[i]/(mw[i]*mw[i]),2)+pow(err_freq[i]/freq[i],2))<<endl<<endl;
		cout<<"snr "<<i+1<<"="<<snr[i]<<"+/-"<<err_snr[i]<<endl<<endl;
		cout<<"produnkt "<<i+1<<"="<<pf_produkt[i]<<"+/-"<<err_pf_produkt[i]<<endl<<endl;
		cout<<"rauschniveau "<<i+1<<"="<<log_level[i]<<"+/-"<<err_log_level[i]<<endl<<endl;
		

	}
	

}
