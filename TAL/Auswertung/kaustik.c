#include "TROOT.h"
#include "read_mca.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TLegend.h"
#include "TLine.h"
#include "TMath.h"

using namespace std;

double PI = 4*atan(1);

void kaustik_params(double A, double B, double C, double A_err, double B_err, double C_err){
    double z_0 = -B/(2*C);
    double z_0_err = z_0 * sqrt(pow(C_err/C,2)+pow(B_err/B,2));
    double M2 = PI/(4*1064*pow(10,-6))*sqrt(A*C-B*B/4);//1064 = lambda
    double M2_err = sqrt(0*pow(A*C_err,2)+pow(C*A_err,2)+pow(B*B_err/2,2))*PI/(8*1064*pow(10,-6))*1/sqrt(A*C-B*B/4);
    double w_0 = 0.5*sqrt(A-B*B/(4*C));
    double w_0_err = 0.5 * 1/(sqrt(A-B*B/(4*C)))
                *sqrt(pow(A_err,2)+pow(B/(2*C)*B_err,2)+pow(B*B/(4*C*C)*C_err,2));
    double z_r = 1/C*sqrt(A*C-B*B/4);
    double temp1 = pow((B*B/(2*C*C*C)-A/(2*C*C))*C_err,2);
    double z_r_err=1/(2*z_r)*sqrt(pow(1/C*A_err,2)+temp1+pow(B/(2*C*C)*B_err,2));
    double theta=sqrt(C)/2;
    double theta_err=theta*0.5*C_err/C;

    cout << "z_0= " << z_0 <<" +/- "<< z_0_err <<endl;
    cout << "M2= " << M2 << " +/- " << M2_err << endl;
    cout << "w_0= " << w_0 <<" +/- "<<w_0_err<< endl;
    cout << "z_r= " << z_r<<" +/- "<<z_r_err << endl;
    cout << "theta= " << theta <<" +/- "<<theta_err <<endl;
}


int kaustik(){
    gStyle->SetOptStat(0);
    gStyle->SetOptFit(1);

//	M2 daten einlesen
	
	int n_0=read_mca("../Data/2Tag/M2-Daten/ukp_profil1.rlg")[0];
	double* M_raw=&read_mca("../Data/2Tag/M2-Daten/ukp_profil1.rlg")[1];
	n_0=n_0-8;
	cout << n_0/8 << endl;	

	double* wx = new double[n_0/8];
	double* wy = new double[n_0/8];
	double* z = new double[n_0/8];

	for(int i=0;i<n_0;){
		wx[i/8]=pow(M_raw[i+5]*0.001,2);
		wy[i/8]=pow(M_raw[i+6]*0.001,2);
		z[i/8]=M_raw[i+7];
		i+=8;
	}
	int n = n_0/8;
	double* wx_err=new double[n];
	double* wy_err=new double[n];
	double* z_err=new double[n];

	for(int i=0;i<n;i++){
		wx_err[i]=2*sqrt(wx[i])*0.0001*1/sqrt(12);
		wy_err[i]=2*sqrt(wy[i])*0.0001*1/sqrt(12);
		z_err[i]=1/sqrt(12);
	}


	TGraphErrors* graph_x = new TGraphErrors(n,z,wx,z_err,wx_err);
    graph_x->SetTitle("Parabelfit zur Strahlqualitaetsmessung x-Richtung");

    TCanvas* plot2=new TCanvas("M2","Lin Fit",900,600);

    plot2->Divide(1,2,0,0);
    plot2->cd(1);
    plot2->GetPad(1)->SetPad(0,0.4,1.,1.);
    plot2->GetPad(1)->SetBottomMargin(0.1);
    plot2->GetPad(1)->SetTopMargin(0.07);
    plot2->GetPad(1)->SetRightMargin(0.01);
    plot2->GetPad(1)->SetLeftMargin(0.08);

    graph_x->GetYaxis()->SetLabelSize(0.052);
    graph_x->GetYaxis()->SetTitleSize(0.052);
    graph_x->GetYaxis()->SetTitleOffset(0.8);
    graph_x->GetYaxis()->SetTitle("Breite^2 [mm^2]");
    graph_x->GetXaxis()->SetTitle("Position [mm]");
    graph_x->GetYaxis()->SetLabelSize(0.05);
    graph_x->GetYaxis()->SetTitleSize(0.05);
//	graph_x->GetYaxis()->SetRangeUser(-50,600);
    graph_x->GetXaxis()->SetLabelSize(0.05);
    graph_x->GetXaxis()->SetTitleSize(0.05);
//	graph_x->GetXaxis()->SetLimits(0,1600);
    TF1* f=new TF1("Linear","[0]+x*[1]+x*x*[2]",z[0], z[n-1]);
    f->SetParameter(0,3);
    f->SetParameter(1,-0.01);
    f->SetParameter(2,0.00001);

   auto Fitpara=graph_x->Fit(f,"S","q");
    graph_x->Draw();
    f->Draw("Same");

    TLegend* leg1=new TLegend(0.1,0.7,0.3,0.9);
    leg1->AddEntry("graph","Daten","EP");
    leg1->AddEntry(f,"Fit","L");
    leg1->SetFillColor(0);
    leg1->Draw("same");
    double res[n];
    double res_err[n];

    for(int i=0;i<n;i++){
        res[i]=wx[i]-f->Eval(z[i]);
        res_err[i]=sqrt(pow(wx_err[i],2)+pow(f->GetParameter(1)*z_err[i]+2*f->GetParameter(2)*z[i]*z_err[i],2));
    }

    plot2->cd();
    plot2->cd(2);

    plot2->GetPad(2)->SetPad(0.,0.,1.,0.4);
    TGraphErrors residuen(n,z,res,nullptr,res_err);

    plot2->GetPad(2)->SetGridy();
    residuen.GetXaxis()->SetLabelSize(0.08);
    residuen.GetXaxis()->SetTitle("z [mm]");
    residuen.GetXaxis()->SetTitleSize(0.08);
    residuen.GetYaxis()->SetTitle("Residuum");
    residuen.GetYaxis()->SetLabelSize(0.08);
    residuen.GetYaxis()->SetTitleSize(0.08);
    residuen.GetYaxis()->SetTitleOffset(0.55);
//	residuen.GetXaxis()->SetLimits(0,1600);
    residuen.SetMarkerStyle(8);
    residuen.SetMarkerSize(0.4);

    plot2->GetPad(2)->SetFrameFillColor(0);
    plot2->GetPad(2)->SetFrameBorderMode(0);
    plot2->GetPad(2)->SetBottomMargin(0.17);
    plot2->GetPad(2)->SetTopMargin(0.04);
    plot2->GetPad(2)->SetRightMargin(0.01);
    plot2->GetPad(2)->SetLeftMargin(0.08);

    residuen.DrawClone("APE");
    TLine *line = new TLine(0,0,1600,0);
    line->SetLineColor(kRed);
    line->Draw("same");
    TLegend* leg2= new TLegend(0.4,0.2,0.6,0.4);
    leg2->AddEntry("residuen","Residuum+Fehler","EP");
    leg2->SetFillColor(0);
    leg2->Draw("same");

	double A = f->GetParameter(0);
    double B = f->GetParameter(1);
    double C = f->GetParameter(2);
    double A_err = Fitpara->GetErrors()[0];
    double B_err = Fitpara->GetErrors()[1];
    double C_err = Fitpara->GetErrors()[2];

//	cout << A_err << " " << B_err << " " << C_err << endl;
	cout << "x-params" << endl;
    kaustik_params(A,B,C,A_err,B_err,C_err);
	cout << endl;



	TGraphErrors* graph_y = new TGraphErrors(n,z,wy,z_err,wy_err);
    graph_y->SetTitle("Parabelfit zur Strahlqualitaetsmessung y-Richtung");

    TCanvas* ploty=new TCanvas("M2_y","Lin Fit",900,600);

    ploty->Divide(1,2,0,0);
    ploty->cd(1);
    ploty->GetPad(1)->SetPad(0,0.4,1.,1.);
    ploty->GetPad(1)->SetBottomMargin(0.1);
    ploty->GetPad(1)->SetTopMargin(0.07);
    ploty->GetPad(1)->SetRightMargin(0.01);
    ploty->GetPad(1)->SetLeftMargin(0.08);

    graph_y->GetYaxis()->SetLabelSize(0.052);
    graph_y->GetYaxis()->SetTitleSize(0.052);
    graph_y->GetYaxis()->SetTitleOffset(0.8);
    graph_y->GetYaxis()->SetTitle("Breite^2 [mm^2]");
    graph_y->GetXaxis()->SetTitle("Position [mm]");
    graph_y->GetYaxis()->SetLabelSize(0.05);
    graph_y->GetYaxis()->SetTitleSize(0.05);
//	graph_y->GetYaxis()->SetRangeUser(-50,600);
    graph_y->GetXaxis()->SetLabelSize(0.05);
    graph_y->GetXaxis()->SetTitleSize(0.05);
//	graph_y->GetXaxis()->SetLimits(0,1600);
    TF1* f_y=new TF1("Linear","[0]+x*[1]+x*x*[2]",z[0], z[n-1]);
    f_y->SetParameter(0,3);
    f_y->SetParameter(1,-0.01);
    f_y->SetParameter(2,0.00001);

   auto Fitpara_y=graph_y->Fit(f_y,"S","q");
    graph_y->Draw();
    f_y->Draw("Same");

    TLegend* leg3=new TLegend(0.1,0.7,0.3,0.9);
    leg3->AddEntry("graph","Daten","EP");
    leg3->AddEntry(f_y,"Fit","L");
    leg3->SetFillColor(0);
    leg3->Draw("same");
    double res_y[n];
    double res_y_err[n];

    for(int i=0;i<n;i++){
        res_y[i]=wy[i]-f_y->Eval(z[i]);
        res_y_err[i]=sqrt(pow(wy_err[i],2)+pow(f_y->GetParameter(1)*z_err[i]+2*f->GetParameter(2)*z[i]*z_err[i],2));
    }

    ploty->cd();
    ploty->cd(2);

    ploty->GetPad(2)->SetPad(0.,0.,1.,0.4);
    TGraphErrors res_yiduen_y(n,z,res_y,nullptr,res_y_err);

    ploty->GetPad(2)->SetGridy();
    res_yiduen_y.GetXaxis()->SetLabelSize(0.08);
    res_yiduen_y.GetXaxis()->SetTitle("z [mm]");
    res_yiduen_y.GetXaxis()->SetTitleSize(0.08);
    res_yiduen_y.GetYaxis()->SetTitle("Residuum");
    res_yiduen_y.GetYaxis()->SetLabelSize(0.08);
    res_yiduen_y.GetYaxis()->SetTitleSize(0.08);
    res_yiduen_y.GetYaxis()->SetTitleOffset(0.55);
//	res_yiduen_y.GetXaxis()->SetLimits(0,1600);
    res_yiduen_y.SetMarkerStyle(8);
    res_yiduen_y.SetMarkerSize(0.4);

    ploty->GetPad(2)->SetFrameFillColor(0);
    ploty->GetPad(2)->SetFrameBorderMode(0);
    ploty->GetPad(2)->SetBottomMargin(0.17);
    ploty->GetPad(2)->SetTopMargin(0.04);
    ploty->GetPad(2)->SetRightMargin(0.01);
    ploty->GetPad(2)->SetLeftMargin(0.08);

    res_yiduen_y.DrawClone("APE");
    TLine *line_y = new TLine(0,0,1600,0);
    line_y->SetLineColor(kRed);
    line_y->Draw("same");
    TLegend* leg4= new TLegend(0.4,0.2,0.6,0.4);
    leg4->AddEntry("res_yiduen_y","Residuum+Fehler","EP");
    leg4->SetFillColor(0);
    leg4->Draw("same");

	double A_y = f_y->GetParameter(0);
    double B_y = f_y->GetParameter(1);
    double C_y = f_y->GetParameter(2);
    double Ay_err = Fitpara_y->GetErrors()[0];
    double By_err = Fitpara_y->GetErrors()[1];
    double Cy_err = Fitpara_y->GetErrors()[2];

//	cout << A_err << " " << B_err << " " << C_err << endl;

	cout << "y-params" << endl;
    kaustik_params(A_y,B_y,C_y,Ay_err,By_err,Cy_err);

	return 0;

}

