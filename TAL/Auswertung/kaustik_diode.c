#include "TROOT.h"
#include "read_mca.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TLegend.h"
#include "TLine.h"
#include "TMath.h"

using namespace std;

double PI = 4*atan(1);

//	Funktion zur berechnung nerviger Strahlparameter
void kaustik_params(double A, double B, double C, double A_err, double B_err, double C_err){
	double z_0 = -B/(2*C);
	double z_0_err = z_0 * sqrt(pow(C_err/C,2)+pow(B_err/B,2));
	double M2 = PI/(4*808*pow(10,-6))*sqrt(A*C-B*B/4);//808 = lambda
	double M2_err = PI/(4*808*pow(10,-6))*1/(2*sqrt(A*C-B*B/4))*
				sqrt(pow(A*C_err,2)+pow(C*A_err,2)*+pow(B*B_err/2,2));
	double w_0 = 0.5*sqrt(A-B*B/(4*C));
	double w_0_err = 0.5 * 1/(sqrt(A-B*B/(4*C)))
				*sqrt(pow(A_err,2)+pow(B/(2*C)*B_err,2)+pow(B*B/(4*C*C)*C_err,2));
	double z_r = 1/C*sqrt(A*C-B*B/4);
	double temp1 = pow((B*B/(2*C*C*C)-A/(2*C*C))*C_err,2);
	double z_r_err=1/(2*z_r)*sqrt(pow(1/C*A_err,2)+temp1+pow(B/(2*C*C)*B_err,2));
	double theta=sqrt(C)/2;
	double theta_err=theta*0.5*C_err/C;
	
	cout << "z_0= " << z_0 <<" +/- "<< z_0_err <<endl;	
	cout << "M2= " << M2 << " +/- " << M2_err << endl;	
	cout << "w_0= " << w_0 <<" +/- "<<w_0_err<< endl;	
	cout << "z_r= " << z_r<<" +/- "<<z_r_err << endl;	
	cout << "theta= " << theta <<" +/- "<<theta_err <<endl;	
}




double* RMS(int n, double* vals){
    double mean=0;
    double var=0;
    for(int i=0;i<n;i++) mean+=vals[i];
    mean=mean/(n+0.0);
    for(int i=0;i<n;i++)var+=pow((vals[i]-mean),2);
    var=var/(n-1.0);
    double* toret = new double[2];
    toret[0]=mean;
    toret[1]=var;
    return toret;
}


int kaustik_diode(){
    gStyle->SetOptStat(0);
    gStyle->SetOptFit(1);

	int n1 = read_mca("../Data/strahlprofil_diode_129.txt")[0];

	double* d99 = &read_mca("../Data/strahlprofil_diode_99.txt")[1];
	double* d109 = &read_mca("../Data/strahlprofil_diode_109.txt")[1];
	double* d119 = &read_mca("../Data/strahlprofil_diode_119.txt")[1];
	double* d129 = &read_mca("../Data/strahlprofil_diode_129.txt")[1];
	double* d139 = &read_mca("../Data/139_neu_diode.txt")[1];
	double* d149 = &read_mca("../Data/strahlprofil_diode_149.txt")[1];
	double* d159 = &read_mca("../Data/strahlprofil_diode_159.txt")[1];

	const int n=7;
	double* ds[n] = {d99,d109,d119,d129,d139,d149,d159};
	

	double* width = new double[n];
	double* width_err = new double[n];
	double z[n]{99,109,119,129,139,149,159};
	double z_err[n];

	double temp[n1/2]; 

	for(int i=0;i<n;i++){
		for(int j=0;j<n1/2;j++){
			temp[j]=ds[i][2*j+1];//j+1 geht über zu y-daten
			cout << "temp= "<< temp[j]<<endl;
			}
		width[i]=RMS(n1/2,temp)[0]*0.001;			
		width_err[i]=sqrt(2*RMS(n1,temp)[1]/n1)*0.001;
		z[i]=129-z[i];
		z_err[i]=1/sqrt(12);
		cout<<endl;
	}
	z_err[3]=0;
	for(int i=0;i<n;i++){
		width_err[i]=2*width[i]*width_err[i];
		width[i]=width[i]*width[i];
	}

			
	TGraphErrors* graph_x = new TGraphErrors(n,z,width,z_err,width_err);
    graph_x->SetTitle("Parabelfit zur Strahlqualitaetsmessung y-Richtung");

    TCanvas* plot2=new TCanvas("M2","Lin Fit",900,600);

    plot2->Divide(1,2,0,0);
    plot2->cd(1);
    plot2->GetPad(1)->SetPad(0,0.4,1.,1.);
    plot2->GetPad(1)->SetBottomMargin(0.01);
    plot2->GetPad(1)->SetTopMargin(0.07);
    plot2->GetPad(1)->SetRightMargin(0.01);
    plot2->GetPad(1)->SetLeftMargin(0.08);

    graph_x->GetYaxis()->SetLabelSize(0.052);
    graph_x->GetYaxis()->SetTitleSize(0.052);
    graph_x->GetYaxis()->SetTitleOffset(0.8);
    graph_x->GetYaxis()->SetTitle("Breite^2 [mm^2]");
    graph_x->GetXaxis()->SetTitle("Position [mm]");
    graph_x->GetYaxis()->SetLabelSize(0.05);
    graph_x->GetYaxis()->SetTitleSize(0.05);
//  graph_x->GetYaxis()->SetRangeUser(-50,600);
    graph_x->GetXaxis()->SetLabelSize(0.05);
    graph_x->GetXaxis()->SetTitleSize(0.05);
//  graph_x->GetXaxis()->SetLimits(0,1600);
    TF1* f=new TF1("Linear","[0]+x*[1]+x*x*[2]",z[0], z[n-1]);
    f->SetParameter(0,21);
    f->SetParameter(1,-0.3);
    f->SetParameter(2,0.01);

	auto Fitpara=graph_x->Fit(f,"S","q");
    graph_x->Draw();
    f->Draw("Same");

    TLegend* leg1=new TLegend(0.1,0.7,0.3,0.9);
    leg1->AddEntry("graph","Daten","EP");
    leg1->AddEntry(f,"Fit","L");
    leg1->SetFillColor(0);
    leg1->Draw("same");
    double res[n];
    double res_err[n];

    for(int i=0;i<n;i++){
        res[i]=width[i]-f->Eval(z[i]);
        res_err[i]=sqrt(pow(width_err[i],2)+pow(f->GetParameter(1)*z_err[i]+2*f->GetParameter(2)*z[i]*z_err[i],2));
    }

    plot2->cd();
    plot2->cd(2);

    plot2->GetPad(2)->SetPad(0.,0.,1.,0.4);
    TGraphErrors residuen(n,z,res,nullptr,res_err);
	plot2->GetPad(2)->SetGridy();
    residuen.GetXaxis()->SetLabelSize(0.08);
    residuen.GetXaxis()->SetTitle("Position [mm]");
    residuen.GetXaxis()->SetTitleSize(0.08);
    residuen.GetYaxis()->SetTitle("Residuum");
    residuen.GetYaxis()->SetLabelSize(0.08);
    residuen.GetYaxis()->SetTitleSize(0.08);
    residuen.GetYaxis()->SetTitleOffset(0.55);
//  residuen.GetXaxis()->SetLimits(0,1600);
    residuen.SetMarkerStyle(8);
    residuen.SetMarkerSize(0.4);

    plot2->GetPad(2)->SetFrameFillColor(0);
    plot2->GetPad(2)->SetFrameBorderMode(0);
    plot2->GetPad(2)->SetBottomMargin(0.17);
    plot2->GetPad(2)->SetTopMargin(0.04);
    plot2->GetPad(2)->SetRightMargin(0.01);
    plot2->GetPad(2)->SetLeftMargin(0.08);

    residuen.DrawClone("APE");
    TLine *line = new TLine(-35,0,35,0);
    line->SetLineColor(kRed);
    line->Draw("same");
    TLegend* leg2= new TLegend(0.4,0.2,0.6,0.4);
    leg2->AddEntry("residuen","Residuum+Fehler","EP");
    leg2->SetFillColor(0);
    leg2->Draw("same");

	double A = f->GetParameter(0);
	double B = f->GetParameter(1);
	double C = f->GetParameter(2);
	double A_err = Fitpara->GetErrors()[0];
	double B_err = Fitpara->GetErrors()[1];
	double C_err = Fitpara->GetErrors()[2];


	cout << A << " " << B << " " << C << endl;
	cout << A_err << " " << B_err << " " << C_err << endl;
		
	kaustik_params(A,B,C,A_err,B_err,C_err);	

	return 0;
}


