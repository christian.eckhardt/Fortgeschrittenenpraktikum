#include "TROOT.h"
#include "read_mca.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TLegend.h"
#include "TLine.h"
#include "TMath.h"

using namespace std;

double PI = 4*atan(1);

int power(){
	gStyle->SetOptStat(0);
    gStyle->SetOptFit(1);

    int n= read_mca("../Data/Diode_I")[0];
    n=n;
    double* P = &read_mca("../Data/Diode_P")[1];
    double* I = &read_mca("../Data/Diode_I")[1];
    double* P_err = &read_mca("../Data/Diode_P_err")[1];
	double* I_err = new double[n];
	for(int i=0;i<n;i++){
		I_err[i] = 0;
	}

	TGraphErrors* graph = new TGraphErrors(n,I,P,I_err,P_err);
    graph->SetTitle("Bestimmung T1");

    TCanvas* plot=new TCanvas("T1","Bestimmung T1",900,600);
    plot->SetBottomMargin(0.1);
    plot->SetTopMargin(0.07);
    plot->SetRightMargin(0.01);
    plot->SetLeftMargin(0.1);

    graph->GetYaxis()->SetLabelSize(0.052);
    graph->GetYaxis()->SetTitleSize(0.052);
    graph->GetYaxis()->SetTitleOffset(0.8);
    graph->GetYaxis()->SetTitle("Spannung [V]");
    graph->GetXaxis()->SetTitle("Zeit [s]");
    graph->GetYaxis()->SetLabelSize(0.05);
    graph->GetYaxis()->SetTitleSize(0.05);
    graph->GetXaxis()->SetLabelSize(0.05);
    graph->GetXaxis()->SetTitleSize(0.05);
    graph->Draw();

/////////////////
//	gerade an linearen Teil fitten


/*
	int n_low = 5;
	int n_high = n;
    int n = n_high-n_low;
    double* P= new double[n];
    double* I= new double[n];
    double* P_err=new double[n];
    double* I_err=new double[n];
    for(int i=0; i<n;i++){
        if(i<n_high && i>=n_low){
            P[i-n_low]=P[i];
            I[i-n_low]=I[i];
            P_err[i-n_low]=P_err[i];
            I_err[i-n_low]=I_err[i];
        }
    }
*/
	
	int low = 20;

	TGraphErrors* graph_lin = new TGraphErrors(n,I,P,I_err,P_err);
    graph_lin->SetTitle("Bestimmung des Schwellstroms");

    TCanvas* plot2=new TCanvas("Zero-Cross","Lin Fit fuer Zero-crossing",900,600);

    plot2->Divide(1,2,0,0);
    plot2->cd(1);
    plot2->GetPad(1)->SetPad(0,0.4,1.,1.);
    plot2->GetPad(1)->SetBottomMargin(0.01);
    plot2->GetPad(1)->SetTopMargin(0.07);
    plot2->GetPad(1)->SetRightMargin(0.015);
    plot2->GetPad(1)->SetLeftMargin(0.08);

    graph_lin->GetYaxis()->SetLabelSize(0.052);
    graph_lin->GetYaxis()->SetTitleSize(0.052);
    graph_lin->GetYaxis()->SetTitleOffset(0.8);
    graph_lin->GetYaxis()->SetTitle("Leistung [mW]");
    graph_lin->GetXaxis()->SetTitle("Stromspannung [A]");
    graph_lin->GetYaxis()->SetLabelSize(0.05);
    graph_lin->GetYaxis()->SetTitleSize(0.05);
    graph_lin->GetXaxis()->SetLabelSize(0.05);
    graph_lin->GetXaxis()->SetTitleSize(0.05);
	graph_lin->GetXaxis()->SetLimits(-0.1,1.8);
	graph_lin->SetMarkerStyle(5);	

	TF1* f=new TF1("Linear","[0]*x+[1]",-100, I[n-1]);
    f->SetParameter(0,1000);
    f->SetParameter(1,-1000);

    auto Fitpara_lin=graph_lin->Fit(f,"S","q",I[low],I[n-1]);
    graph_lin->Draw();
    f->Draw("Same");

    TLegend* leg1=new TLegend(0.1,0.7,0.3,0.9);
    leg1->AddEntry("graph","Daten","EP");
    leg1->AddEntry(f,"Fit","L");
    leg1->SetFillColor(0);
    leg1->Draw("same");

    double res[n-low];
    double res_lin_err[n-low];
	double res_I[n-low];


    for (int i=low;i<n;i++){
		res[i-low]=P[i]-f->Eval(I[i]);
		res_lin_err[i-low]=sqrt(pow(P_err[i],2)+pow(f->GetParameter(0)*I_err[i],2));
    	res_I[i-low]=I[i];
	}

    plot2->cd();
    plot2->cd(2);

	plot2->GetPad(2)->SetPad(0.,0.,1.,0.4);
    TGraphErrors residuen(n-low,res_I,res,nullptr,res_lin_err);

    plot2->GetPad(2)->SetGridy();
    residuen.GetXaxis()->SetLabelSize(0.08);
    residuen.GetXaxis()->SetTitle("Strom [A]");
    residuen.GetXaxis()->SetTitleSize(0.08);
    residuen.GetYaxis()->SetTitle("Residuum");
    residuen.GetYaxis()->SetLabelSize(0.08);
    residuen.GetYaxis()->SetTitleSize(0.08);
    residuen.GetYaxis()->SetTitleOffset(0.55);
	residuen.GetXaxis()->SetLimits(-0.1,1.8);
    residuen.SetMarkerStyle(8);
    residuen.SetMarkerSize(0.4);

    plot2->GetPad(2)->SetFrameFillColor(0);
    plot2->GetPad(2)->SetFrameBorderMode(0);
    plot2->GetPad(2)->SetBottomMargin(0.17);
    plot2->GetPad(2)->SetTopMargin(0.04);
    plot2->GetPad(2)->SetRightMargin(0.015);
    plot2->GetPad(2)->SetLeftMargin(0.08);


    residuen.DrawClone("APE");
    TLine *line = new TLine(-0.1,0,1.8,0);
    line->SetLineColor(kRed);
    line->Draw("same");
    TLegend* leg2= new TLegend(0.4,0.2,0.6,0.4);
    leg2->AddEntry("residuen","Residuum+Fehler","EP");
    leg2->SetFillColor(0);
    leg2->Draw("same");


	
	return 0;
}

