#include "TROOT.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TGraphErrors.h"
#include "read_mca.h"
#include <fstream>
#include "TMath.h"

using namespace std;

void per(){
	
	gStyle->SetOptFit(1);

	const int n=read_mca("../Data/per.txt")[0];
	double* per=& read_mca("../Data/per.txt")[1];

	double winkel [n/3];
	double winkel_err [n/3];
	double p[n/3];
	double p_err[n/3];
	
	for (int i=0;i<n;i++){
		winkel[i/3]=per[i]*3.14159/180.;
		winkel_err[i/3]=1.0*3.14159/180./sqrt(12);
		p[i/3]=per[i+1];
		p_err[i/3]=per[i+2];
		i++;
		i++;
	}

	TGraphErrors* per_graph= new TGraphErrors(n/3,winkel,p,winkel_err,p_err);
	
	per_graph->GetXaxis()->SetLimits(-0.05,winkel[n/3-1]+0.05);
	per_graph->SetTitle("PER");
	per_graph->GetYaxis()->SetLabelSize(0.052);
	per_graph->GetYaxis()->SetTitleSize(0.052);
	per_graph->GetYaxis()->SetTitleOffset(0.8);
	per_graph->GetYaxis()->SetTitle("P_{Laser} [mW]");
	per_graph->GetXaxis()->SetTitle("Winkel [rad]");
	per_graph->GetYaxis()->SetLabelSize(0.05);
	per_graph->GetYaxis()->SetTitleSize(0.05);
	per_graph->GetXaxis()->SetLabelSize(0.05);
	per_graph->GetXaxis()->SetTitleSize(0.05);

	TF1* func= new TF1("func","[0]*pow(cos(2*x+[1]),2)+[2]",0,3.14519);
	func->SetParameter(2,0.2);
	func->SetParameter(1,0.2124);
	func->SetParameter(0,212.9);
	
	auto fitres=per_graph->Fit(func,"S","q");

	TCanvas* c_per= new TCanvas("c_per","c_per",900,600);

	c_per->cd();
	c_per->Divide(1,2,0,0);
	c_per->cd(1);
	c_per->GetPad(1)->SetPad(0,0.4,1.,1.);
	c_per->GetPad(1)->SetBottomMargin(0.01);
	c_per->GetPad(1)->SetTopMargin(0.07);
	c_per->GetPad(1)->SetRightMargin(0.01);
	c_per->GetPad(1)->SetLeftMargin(0.08);


	per_graph->Draw();


	TLegend* leg_per=new TLegend(0.1,0.7,0.3,0.9);
	leg_per->AddEntry("per_graph","Daten","EP");
	leg_per->AddEntry(func,"Fit:p0*cos(p1*#theta+p2)^2+p3","L");
	leg_per->SetFillColor(0);
	leg_per->Draw("same");

        double res[n/3];
        double res_err[n/3];

        double f_0=func->GetParameter(0);
	double err_f_0=func->GetParError(0);
        double f_1=func->GetParameter(1);
	double err_f_1=func->GetParError(1);
	double f_2=func->GetParameter(2);
	double err_f_2=func->GetParError(2);

        for (int i=0;i<n/3;i++){
	        res[i]=p[i]-func->Eval(winkel[i]);
	        res_err[i]=sqrt(pow(p_err[i],2)+pow(2*winkel_err[i]*f_0*2*cos(2*winkel[i]+f_1)*sin(2*winkel[i]+f_1),2));
	}

	c_per->cd();
        c_per->cd(2);

        c_per->GetPad(2)->SetPad(0.,0.,1.,0.4);
        TGraphErrors residuen(n/3,winkel,res,nullptr,res_err);

	residuen.GetXaxis()->SetLimits(-0.05,winkel[n/3-1]+0.05);

	residuen.SetTitle("");

	c_per->GetPad(2)->SetGridy();
	residuen.GetXaxis()->SetLabelSize(0.08);
	residuen.GetXaxis()->SetTitle("Winkel [rad]");
	residuen.GetXaxis()->SetTitleSize(0.08);
	residuen.GetYaxis()->SetTitle("Residuum");
	residuen.GetYaxis()->SetLabelSize(0.08);
	residuen.GetYaxis()->SetTitleSize(0.08);
	residuen.GetYaxis()->SetTitleOffset(0.55);
	residuen.SetMarkerStyle(8);
	residuen.SetMarkerSize(0.4);
//
	c_per->GetPad(2)->SetFrameFillColor(0);
	c_per->GetPad(2)->SetFrameBorderMode(0);
	c_per->GetPad(2)->SetBottomMargin(0.17);
	c_per->GetPad(2)->SetTopMargin(0.04);
	c_per->GetPad(2)->SetRightMargin(0.01);
	c_per->GetPad(2)->SetLeftMargin(0.08);

	residuen.DrawClone("APE");

	TLine *line = new TLine(-0.05,0,winkel[n/3-1]+0.05,0);
	line->SetLineColor(kRed);
	line->Draw("same");
	TLegend* leg_res= new TLegend(0.4,0.2,0.6,0.4);
	leg_res->AddEntry("Residuen","Residuum+Fehler","EP");
	leg_res->SetFillColor(0);
	leg_res->Draw("same");

	
	cout<<"P_max="<<f_0+f_2<<"+/-"<<sqrt(pow(err_f_0,2)+pow(err_f_1,2))<<endl;
	cout<<"P_min="<<f_2<<"+/-"<<err_f_2<<endl;
	cout<<"PER="<<1+f_0/f_2<<"+/-"<<f_0/f_2*sqrt(pow(err_f_0/f_0,2)+pow(err_f_2/f_2,2)) <<endl;

}
