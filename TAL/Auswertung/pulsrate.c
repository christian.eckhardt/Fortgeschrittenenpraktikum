#include "TROOT.h"
#include "read_mca.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TLegend.h"
#include "TLine.h"
#include "TMath.h"

using namespace std;

double PI = 4*atan(1);

int pulsrate(){

    gStyle->SetOptStat(0);
    gStyle->SetOptFit(1);

    int n= read_mca("../Data/Oszi.CSV")[0];
    cout<<n<<endl;
    double* raw = &read_mca("../Data/Oszi.CSV")[1];

    double* U=new double[n/2];
    double* t=new double[n/2];
    double* Err=new double[n/2];

    for(int i=0;i<n;i++){
        U[i/2]=raw[i+1];
        t[i/2]=raw[i];
//		Err[i/2]=0.00816497;
        i++;
	}
	n=n/2;
	
	TGraphErrors* graph = new TGraphErrors(n,t,U,nullptr,nullptr);


	graph->SetTitle("Repititionsmessung mit dem Oszilloskop");

    TCanvas* plot1=new TCanvas("oszi","Repititionsmessung mit dem Oszilloskop",900,600);
    plot1->SetBottomMargin(0.1);
    plot1->SetTopMargin(0.07);
    plot1->SetRightMargin(0.01);
    plot1->SetLeftMargin(0.1);

    graph->GetYaxis()->SetLabelSize(0.052);
    graph->GetYaxis()->SetTitleSize(0.052);
    graph->GetYaxis()->SetTitleOffset(0.8);
    graph->GetYaxis()->SetTitle("Spannung [V]");
    graph->GetXaxis()->SetTitle("Zeit [ns]");
    graph->GetYaxis()->SetLabelSize(0.05);
    graph->GetYaxis()->SetTitleSize(0.05);
    graph->GetXaxis()->SetLabelSize(0.05);
    graph->GetXaxis()->SetTitleSize(0.05);
	
	graph->SetMarkerStyle(6);
	plot1->cd();
	graph->Draw("APE");

////////////////////////////////
//	veranschaulichen wie peaks bestimmt wurden

	TGraphErrors* graph1 = new TGraphErrors(n,t,U,nullptr,nullptr);
    
    graph1->GetYaxis()->SetLabelSize(0.052);
    graph1->GetYaxis()->SetTitleSize(0.052);
    graph1->GetYaxis()->SetTitleOffset(0.8);
    graph1->GetYaxis()->SetTitle("Spannung [V]");
    graph1->GetXaxis()->SetTitle("Zeit [s]");
    graph1->GetYaxis()->SetLabelSize(0.05);
    graph1->GetYaxis()->SetTitleSize(0.05);
    graph1->GetXaxis()->SetLabelSize(0.05);
    graph1->GetXaxis()->SetTitleSize(0.05);
	
	graph1->SetMarkerStyle(6);

	TCanvas* peakfind =new TCanvas("peakfind","peakfind",900,600);
    graph1->GetXaxis()->SetLimits(10.*pow(10,-9),12.4*pow(10,-9));
    graph1->SetTitle("Beispiel zur Peakbestimmung");


    double peak_value=11.24*pow(10,-9);
	double dt=(t[10]-t[9]);
	cout << "dt= " << dt << endl; 
    double left_value=peak_value-dt;
    double right_value=peak_value+dt;
    TLine* left= new TLine(left_value,-0.2,left_value,1.6);
    TLine* right= new TLine(right_value,-0.2,right_value,1.6);
    TLine* peak= new TLine(peak_value,-0.2,peak_value,1.6);
    peak->SetLineColor(2);
    TBox* area= new TBox(left_value,-0.2,right_value,1.6);
    area->SetFillColor(4);
    area->SetFillStyle(3004);
	  
	
	peakfind->cd();
    graph1->Draw("APE");
	area->Draw("same");
    left->Draw("same");
    peak->Draw("same");
    right->Draw("same");

    TLegend* leg_peak= new TLegend(0.7,0.7,0.9,0.9);
    leg_peak->AddEntry(area,"Fehlerabschätzung","F");
    leg_peak->AddEntry(graph1,"Datenpunkte","EL");
    leg_peak->AddEntry(left,"linke und rechte Grenze","L");
    leg_peak->AddEntry(peak,"Peakposition","L");
    leg_peak->Draw("same");
//	peakfind->SaveAs("../Protokoll/Protokoll_Data/peakfind.pdf","pdf");

//////////////////////
//	linearer fit

	double peaks_t[10] = {-48.76,-38.76,-28.82,-18.8,-8.76,1.2,11.24,21.16,31.18,41.16};
	double nums[10];
	double peak_err[10];
	for(int i=0;i<10;i++){
		peaks_t[i]=peaks_t[i]*pow(10,-9);
		nums[i]=i+1;
		peak_err[i]=dt;		
	}

    TGraphErrors* graph_lin = new TGraphErrors(10,nums,peaks_t,nullptr,peak_err);
    graph_lin->SetTitle("Lineare Anpassung zur Bestimmung der Repititionsrate");

    TCanvas* plot2=new TCanvas("Zero-Cross","Lin Fit",900,600);

    plot2->Divide(1,2,0,0);
    plot2->cd(1);
    plot2->GetPad(1)->SetPad(0,0.4,1.,1.);
    plot2->GetPad(1)->SetBottomMargin(0.01);
    plot2->GetPad(1)->SetTopMargin(0.07);
    plot2->GetPad(1)->SetRightMargin(0.01);
    plot2->GetPad(1)->SetLeftMargin(0.08);

    graph_lin->GetYaxis()->SetLabelSize(0.052);
    graph_lin->GetYaxis()->SetTitleSize(0.052);
    graph_lin->GetYaxis()->SetTitleOffset(0.8);
    graph_lin->GetYaxis()->SetTitle("Zeit [s]");
    graph_lin->GetXaxis()->SetTitle("Pulsnummer");
    graph_lin->GetYaxis()->SetLabelSize(0.05);
    graph_lin->GetYaxis()->SetTitleSize(0.05);
    graph_lin->GetXaxis()->SetLabelSize(0.05);
    graph_lin->GetXaxis()->SetTitleSize(0.05);
  TF1* f=new TF1("Linear","[0]*x+[1]",nums[0], nums[10-1]);
    f->SetParameter(0,1);
    f->SetParameter(1,1);

    auto Fitpara_lin=graph_lin->Fit(f,"S","q");
    graph_lin->Draw();
    f->Draw("Same");

    TLegend* leg1=new TLegend(0.1,0.7,0.3,0.9);
    leg1->AddEntry("graph","Daten","EP");
    leg1->AddEntry(f,"Fit","L");
    leg1->SetFillColor(0);
    leg1->Draw("same");

    double res[n];
//	double res_lin_err[n];

    for(int i=0;i<10;i++){
        res[i]=peaks_t[i]-f->Eval(nums[i]);
    }

    plot2->cd();
    plot2->cd(2);

    plot2->GetPad(2)->SetPad(0.,0.,1.,0.4);
    TGraphErrors residuen(10,nums,res,nullptr,peak_err);

    plot2->GetPad(2)->SetGridy();
    residuen.GetXaxis()->SetLabelSize(0.08);
    residuen.GetXaxis()->SetTitle("Pulsnummer");
    residuen.GetXaxis()->SetTitleSize(0.08);
    residuen.GetYaxis()->SetTitle("Residuum");
    residuen.GetYaxis()->SetLabelSize(0.08);
    residuen.GetYaxis()->SetTitleSize(0.08);
    residuen.GetYaxis()->SetTitleOffset(0.55);
    residuen.SetMarkerStyle(8);
    residuen.SetMarkerSize(0.4);

    plot2->GetPad(2)->SetFrameFillColor(0);
    plot2->GetPad(2)->SetFrameBorderMode(0);
    plot2->GetPad(2)->SetBottomMargin(0.17);
    plot2->GetPad(2)->SetTopMargin(0.04);
    plot2->GetPad(2)->SetRightMargin(0.01);
    plot2->GetPad(2)->SetLeftMargin(0.08);


    residuen.DrawClone("APE");
    TLine *line = new TLine(nums[0],0,nums[10-1]+1,0);
    line->SetLineColor(kRed);
    line->Draw("same");
    TLegend* leg2= new TLegend(0.4,0.2,0.6,0.4);
    leg2->AddEntry("residuen","Residuum+Fehler","EP");
    leg2->SetFillColor(0);
    leg2->Draw("same");

	return 0;
}
