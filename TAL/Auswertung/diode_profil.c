#include "TCanvas.h"
#include "TF1.h"
#include "TGraphErrors.h"
#include "TROOT.h"
#include "read_mca.h"
#include "TLine.h"
#include "TLegend.h"
#include "TMath.h"
#include <fstream>
using namespace std;


void diode_profil(){

	gStyle->SetOptStat(0);
	gStyle->SetOptFit(1);

	int n= read_mca("../Data/strahlprofil_diode_99.txt")[0];
	const int m=7;
	double z[m]={9.9,10.9,11.9,12.9,13.9,14.9,15.9};
	double z_err[m]={0.1,0.1,0.1,0.1,0.1,0.1,0.1};


	double* profil1= &read_mca("../Data/strahlprofil_diode_99.txt")[1];
	double* profil2= &read_mca("../Data/strahlprofil_diode_109.txt")[1];
	double* profil3= &read_mca("../Data/strahlprofil_diode_119.txt")[1];
	double* profil4= &read_mca("../Data/strahlprofil_diode_129.txt")[1];
	double* profil5= &read_mca("../Data/strahlprofil_diode_139.txt")[1];
	double* profil6= &read_mca("../Data/strahlprofil_diode_149.txt")[1];
	double* profil7= &read_mca("../Data/strahlprofil_diode_159.txt")[1];

	double dx[m]={0,0,0,0,0,0,0};
	double dy[m]={0,0,0,0,0,0,0};

	double tempdx[m]={0,0,0,0,0,0,0};
	double tempdy[m]={0,0,0,0,0,0,0};

	double dx_err[m];
	double dy_err[m];
	for(int i=0;i<n;i++){

		dx[0]+=profil1[i]/5.;
		dy[0]+=profil1[i+1]/5.;

		dx[1]+=profil2[i]/5.;
		dy[1]+=profil2[i+1]/5.;

		dx[2]+=profil3[i]/5.;
		dy[2]+=profil3[i+1]/5.;


		dx[3]+=profil4[i]/5.;
		dy[3]+=profil4[i+1]/5.;

		dx[4]+=profil5[i]/5.;
		dy[4]+=profil5[i+1]/5.;

		dx[5]+=profil6[i]/5.;
		dy[5]+=profil6[i+1]/5.;

		dx[6]+=profil7[i]/5.;
		dy[6]+=profil7[i+1]/5.;
		i++;
	}

	for(int i=0;i<n;i++){
		
		tempdx[0]+=pow(dx[0]-profil1[i],2)/(4.);
		tempdy[0]+=pow(dy[0]-profil1[i+1],2)/(4.);

		tempdx[1]+=pow(dx[1]-profil2[i],2)/(4.);
		tempdy[1]+=pow(dy[1]-profil2[i+1],2)/(4.);

		tempdx[2]+=pow(dx[2]-profil3[i],2)/(4.);
		tempdy[2]+=pow(dy[2]-profil3[i+1],2)/(4.);

		tempdx[3]+=pow(dx[3]-profil4[i],2)/(4.);
		tempdy[3]+=pow(dy[3]-profil4[i+1],2)/(4.);

		tempdx[4]+=pow(dx[4]-profil5[i],2)/(4.);
		tempdy[4]+=pow(dy[4]-profil5[i+1],2)/(4.);

		tempdx[5]+=pow(dx[5]-profil6[i],2)/(4.);
		tempdy[5]+=pow(dy[5]-profil6[i+1],2)/(4.);

		tempdx[6]+=pow(dx[6]-profil7[i],2)/(4.);
		tempdy[6]+=pow(dy[6]-profil7[i+1],2)/(4.);


		i++;
	}

	for(int i=0;i<n;i++){
		dx_err[i]=sqrt(tempdx[i]/5.);
		cout<<dx[i]<<" "<<dx_err[i]<<endl;
		dy_err[i]=sqrt(tempdy[i]/5.);
		cout<<dy[i]<<" "<<dy_err[i]<<endl;
	}

}
