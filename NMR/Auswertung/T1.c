#include "TROOT.h"
#include "read_mca.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TLegend.h"
#include "TLine.h"
#include "TMath.h"

using namespace std;

double PI = 4*atan(1);

int T1(){
	
///////////////////////////////
//sysfehler rechnung
//relative fehler
	double t_pi = 0.04/6.32*PI;
	double t_pi2= 0.04/3.1*PI/2;

//fehler aus falscher freuquenz
	double U0 = 22.1;
	double A = 12*pow(10,-6);
	double wp = 21428310;
	double gam = 2.675*pow(10,8);
	double B1 = U0/(4*wp*A);
	double dw=1000;
	double d_pi = 2*dw/(gam*B1);	
	cout << "B1= " << B1 << endl;
	cout << "d_pi= " << d_pi << endl; 
	
	double pi_err=t_pi+d_pi;
	double pi2_err= sqrt(pow(t_pi2,2)+pow(d_pi,2));
	double correction = cos(pi_err)*cos(pi2_err);
	cout << "corrections" << correction << endl;

//////////////////////////////

    gStyle->SetOptStat(0);
    gStyle->SetOptFit(1);

	int n= read_mca("../Data/T1_U")[0];
	n=n-1;
	double* U = &read_mca("../Data/T1_U")[2];
	double* t = &read_mca("../Data/T1_t")[2];

//	make error array with estimates during experiment
	double* U_err = new double[n];
	for(int i=0; i<n;i++){
		if(i<4){ 
			U_err[i]=0.01;
			continue;
		}
		if(i<7){ 
			U_err[i]=0.008;
			continue;
		}
		if(i<11){ 
			U_err[i]=0.004;
			continue;
		}
		if(i<22){ 
			U_err[i]=0.002;
			continue;
		}
		if(i<27){ 
			U_err[i]=0.004;
			continue;
		}
		if(i<29){ 
			U_err[i]=0.008;
			continue;
		}
		else{U_err[i]=0.01;}
	}
	double stat_err=0.00816497;
	for(int i=0; i<n; i++){
		U_err[i]=sqrt(pow(stat_err,2)+U_err[i]*U_err[i]/12);
	}

//	for(int i=0;i<n;i++){
//		cout<<"U= " << U[i] << " +/- " << U_err[i] << " t= " << t[i]<<endl;
//	}
	TGraphErrors* graph = new TGraphErrors(n,t,U,nullptr,U_err);
    graph->SetTitle("Bestimmung T1");

    TCanvas* plot=new TCanvas("T1","Bestimmung T1",900,600);
    plot->SetBottomMargin(0.1);
    plot->SetTopMargin(0.07);
    plot->SetRightMargin(0.01);
    plot->SetLeftMargin(0.1);

    graph->GetYaxis()->SetLabelSize(0.052);
    graph->GetYaxis()->SetTitleSize(0.052);
    graph->GetYaxis()->SetTitleOffset(0.8);
    graph->GetYaxis()->SetTitle("Spannung [V]");
    graph->GetXaxis()->SetTitle("Zeit [s]");
	graph->GetYaxis()->SetLabelSize(0.05);
    graph->GetYaxis()->SetTitleSize(0.05);
	graph->GetXaxis()->SetLabelSize(0.05);
    graph->GetXaxis()->SetTitleSize(0.05);
	graph->Draw();

	int zero_cross = 14;
	for(int i=0;i<n;i++){ 
		U[i]=U[i];//correction
		if(i>zero_cross) break;	
		U[i]=-U[i];
	}
	TGraphErrors* graph2 = new TGraphErrors(n,t,U,nullptr,U_err);
    graph2->SetTitle("Bestimmung T1 invertiert");

    TCanvas* plot1=new TCanvas("T1_inv","Bestimmung T1 invertiert",900,600);
    plot1->SetBottomMargin(0.1);
    plot1->SetTopMargin(0.07);
    plot1->SetRightMargin(0.01);
    plot1->SetLeftMargin(0.1);

    graph2->GetYaxis()->SetLabelSize(0.052);
    graph2->GetYaxis()->SetTitleSize(0.052);
    graph2->GetYaxis()->SetTitleOffset(0.8);
    graph2->GetYaxis()->SetTitle("Spannung [V]");
    graph2->GetXaxis()->SetTitle("Zeit [s]");
	graph2->GetYaxis()->SetLabelSize(0.05);
    graph2->GetYaxis()->SetTitleSize(0.05);
	graph2->GetXaxis()->SetLabelSize(0.05);
    graph2->GetXaxis()->SetTitleSize(0.05);
	
	graph2->Draw();

//	paar punkte fuer lineare fit auswaehlen
	int n_low = 10;
	int n_high = 19;
	int n_new = n_high-n_low;
	double* lin_U= new double[n_new];
	double* lin_t= new double[n_new];
	double* lin_U_err=new double[n_new];
	for(int i=0; i<n;i++){
		if(i<n_high && i>=n_low){
			lin_U[i-n_low]=U[i];
			lin_t[i-n_low]=t[i];
			lin_U_err[i-n_low]=U_err[i];
		}
	}
		
	TGraphErrors* graph_lin = new TGraphErrors(n_new,lin_t,lin_U,nullptr,lin_U_err);
    graph_lin->SetTitle("Lin-Fit fuer Zero-crossing");

    TCanvas* plot2=new TCanvas("Zero-Cross","Lin Fit fuer Zero-crossing",900,600);

	plot2->Divide(1,2,0,0);
	plot2->cd(1);
    plot2->GetPad(1)->SetPad(0,0.4,1.,1.);
    plot2->GetPad(1)->SetBottomMargin(0.1);
    plot2->GetPad(1)->SetTopMargin(0.07);
    plot2->GetPad(1)->SetRightMargin(0.01);
    plot2->GetPad(1)->SetLeftMargin(0.08);

    graph_lin->GetYaxis()->SetLabelSize(0.052);
    graph_lin->GetYaxis()->SetTitleSize(0.052);
    graph_lin->GetYaxis()->SetTitleOffset(0.8);
    graph_lin->GetYaxis()->SetTitle("Spannung [V]");
    graph_lin->GetXaxis()->SetTitle("Zeit [s]");
	graph_lin->GetYaxis()->SetLabelSize(0.05);
    graph_lin->GetYaxis()->SetTitleSize(0.05);
	graph_lin->GetXaxis()->SetLabelSize(0.05);
    graph_lin->GetXaxis()->SetTitleSize(0.05);

	TF1* f=new TF1("Linear","[0]*x+[1]",lin_t[0], lin_t[n_new-1]);
	f->SetParameter(0,1);
	f->SetParameter(1,1);

	auto Fitpara_lin=graph_lin->Fit(f,"S","q");
    graph_lin->Draw();
    f->Draw("Same");

    TLegend* leg1=new TLegend(0.1,0.7,0.3,0.9);
    leg1->AddEntry("graph","Daten","EP");
    leg1->AddEntry(f,"Fit","L");
    leg1->SetFillColor(0);
    leg1->Draw("same");

	double res[n];
//    double res_lin_err[n];

    for (int i=0;i<n_new;i++){
        res[i]=lin_U[i]-f->Eval(lin_t[i]);
    }

    plot2->cd();
    plot2->cd(2);

    plot2->GetPad(2)->SetPad(0.,0.,1.,0.4);
    TGraphErrors residuen(n_new,lin_t,res,nullptr,lin_U_err);

    plot2->GetPad(2)->SetGridy();
    residuen.GetXaxis()->SetLabelSize(0.08);
    residuen.GetXaxis()->SetTitle("Zeit [s]");
    residuen.GetXaxis()->SetTitleSize(0.08);
    residuen.GetYaxis()->SetTitle("Residuum");
    residuen.GetYaxis()->SetLabelSize(0.08);
    residuen.GetYaxis()->SetTitleSize(0.08);
    residuen.GetYaxis()->SetTitleOffset(0.55);
    residuen.SetMarkerStyle(8);
    residuen.SetMarkerSize(0.4);

    plot2->GetPad(2)->SetFrameFillColor(0);
    plot2->GetPad(2)->SetFrameBorderMode(0);
    plot2->GetPad(2)->SetBottomMargin(0.17);
    plot2->GetPad(2)->SetTopMargin(0.04);
    plot2->GetPad(2)->SetRightMargin(0.01);
    plot2->GetPad(2)->SetLeftMargin(0.08);
	

	residuen.DrawClone("APE");
    TLine *line = new TLine(lin_t[0],0,lin_t[n_new-1]+1,0);
    line->SetLineColor(kRed);
    line->Draw("same");
    TLegend* leg2= new TLegend(0.4,0.2,0.6,0.4);
    leg2->AddEntry("residuen","Residuum+Fehler","EP");
    leg2->SetFillColor(0);
    leg2->Draw("same");



///////////////////////Exp fit

	TGraphErrors* graph_exp = new TGraphErrors(n,t,U,nullptr,U_err);
    graph_exp->SetTitle("Exp-Fit");

    TCanvas* plot3=new TCanvas("Exp_Fit","Lin Fit fuer Zero-crossing",900,600);
	plot3->cd();
	plot3->Divide(1,2,0,0);
	plot3->cd(1);
    plot3->GetPad(1)->SetPad(0,0.4,1.,1.);
    plot3->GetPad(1)->SetBottomMargin(0.1);
    plot3->GetPad(1)->SetTopMargin(0.07);
    plot3->GetPad(1)->SetRightMargin(0.01);
    plot3->GetPad(1)->SetLeftMargin(0.08);

    graph_exp->GetYaxis()->SetLabelSize(0.052);
    graph_exp->GetYaxis()->SetTitleSize(0.052);
    graph_exp->GetYaxis()->SetTitleOffset(0.8);
    graph_exp->GetYaxis()->SetTitle("Spannung [V]");
    graph_exp->GetXaxis()->SetTitle("Zeit [s]");
	graph_exp->GetYaxis()->SetLabelSize(0.05);
    graph_exp->GetYaxis()->SetTitleSize(0.05);
	graph_exp->GetXaxis()->SetLabelSize(0.05);
    graph_exp->GetXaxis()->SetTitleSize(0.05);

	TF1* f_exp=new TF1("Exponential","[0]*(1-2*exp(-x*[1]))",t[0],t[n-1]);
	f_exp->SetParameter(0,1.5);
	f_exp->SetParameter(1,35);

	auto Fitpara_exp=graph_exp->Fit(f_exp,"S","q");
    graph_exp->Draw();
    f_exp->Draw("Same");

    TLegend* leg3=new TLegend(0.1,0.7,0.3,0.9);
    leg3->AddEntry("graph","Daten","EP");
    leg3->AddEntry(f_exp,"Fit","L");
    leg3->SetFillColor(0);
    leg3->Draw("same");

	double res_exp[n];

    for (int i=0;i<n;i++){
        res_exp[i]=U[i]-f_exp->Eval(t[i]);
    }

    plot3->cd();
    plot3->cd(2);

    plot3->GetPad(2)->SetPad(0.,0.,1.,0.4);
    TGraphErrors residuen_exp(n,t,res_exp,nullptr,U_err);

    plot3->GetPad(2)->SetGridy();
    residuen_exp.GetXaxis()->SetLabelSize(0.08);
    residuen_exp.GetXaxis()->SetTitle("Zeit [s]");
    residuen_exp.GetXaxis()->SetTitleSize(0.08);
    residuen_exp.GetYaxis()->SetTitle("Residuum");
    residuen_exp.GetYaxis()->SetLabelSize(0.08);
    residuen_exp.GetYaxis()->SetTitleSize(0.08);
    residuen_exp.GetYaxis()->SetTitleOffset(0.55);
    residuen_exp.SetMarkerStyle(8);
    residuen_exp.SetMarkerSize(0.4);

    plot3->GetPad(2)->SetFrameFillColor(0);
    plot3->GetPad(2)->SetFrameBorderMode(0);
    plot3->GetPad(2)->SetBottomMargin(0.17);
    plot3->GetPad(2)->SetTopMargin(0.04);
    plot3->GetPad(2)->SetRightMargin(0.01);
    plot3->GetPad(2)->SetLeftMargin(0.08);

	residuen_exp.DrawClone("APE");

    TLine *line2 = new TLine(t[0],0,t[n-1]+1,0);
    line2->SetLineColor(kRed);
    line2->Draw("same");
    TLegend* leg4= new TLegend(0.4,0.2,0.6,0.4);
    leg4->AddEntry("residuen","Residuum+Fehler","EP");
    leg4->SetFillColor(0);
    leg4->Draw("same");


////////////////Linearisierung
////////////////////////////
//irgendwie M0 bestimmen
	double M0 = U[n-1]+0.01;//(abs(U[0])+U[n-1])/2;
	double* M_lin = new double[n];
	double* M_lin_err = new double[n];
	for(int i=0;i<n;i++){
		M_lin[i]=log((M0-U[i])/(2*M0));
		M_lin_err[i]=abs(2*M0/(M0-U[i]))*U_err[i];
	}
	


		
	TGraphErrors* graph_log = new TGraphErrors(n,t,M_lin,nullptr,M_lin_err);
    graph_log->SetTitle("Linearisierter Fit");

    TCanvas* plot4=new TCanvas("Lin_Log","Linearisierte Daten",900,600);

	plot4->Divide(1,2,0,0);
	plot4->cd(1);
    plot4->GetPad(1)->SetPad(0,0.4,1.,1.);
    plot4->GetPad(1)->SetBottomMargin(0.1);
    plot4->GetPad(1)->SetTopMargin(0.07);
    plot4->GetPad(1)->SetRightMargin(0.01);
    plot4->GetPad(1)->SetLeftMargin(0.08);

    graph_log->GetYaxis()->SetLabelSize(0.052);
    graph_log->GetYaxis()->SetTitleSize(0.052);
    graph_log->GetYaxis()->SetTitleOffset(0.8);
    graph_log->GetYaxis()->SetTitle("Spannung [V]");
    graph_log->GetXaxis()->SetTitle("Zeit [s]");
	graph_log->GetYaxis()->SetLabelSize(0.05);
    graph_log->GetYaxis()->SetTitleSize(0.05);
	graph_log->GetXaxis()->SetLabelSize(0.05);
    graph_log->GetXaxis()->SetTitleSize(0.05);

	TF1* f_log=new TF1("Lin_Log","[0]*x+[1]",t[0], t[n-1]);
	f_log->SetParameter(0,-1);
	f_log->SetParameter(1,1);

	auto Fitpara_log=graph_log->Fit(f_log,"S","q");
    graph_log->Draw();
    f_log->Draw("Same");

    TLegend* leg5=new TLegend(0.1,0.7,0.3,0.9);
    leg5->AddEntry("graph","Daten","EP");
    leg5->AddEntry(f_log,"Fit","L");
    leg5->SetFillColor(0);
    leg5->Draw("same");

	double res3[n];
//    double res_lin_err[n];

    for(int i=0;i<n;i++){
        res3[i]=M_lin[i]-f_log->Eval(t[i]);
    }

    plot4->cd();
    plot4->cd(2);

    plot4->GetPad(2)->SetPad(0.,0.,1.,0.4);
    TGraphErrors residuen_log(n,t,res3,nullptr,M_lin_err);

    plot4->GetPad(2)->SetGridy();
    residuen_log.GetXaxis()->SetLabelSize(0.08);
    residuen_log.GetXaxis()->SetTitle("Zeit [s]");
    residuen_log.GetXaxis()->SetTitleSize(0.08);
    residuen_log.GetYaxis()->SetTitle("Residuum");
    residuen_log.GetYaxis()->SetLabelSize(0.08);
    residuen_log.GetYaxis()->SetTitleSize(0.08);
    residuen_log.GetYaxis()->SetTitleOffset(0.55);
    residuen_log.SetMarkerStyle(8);
    residuen_log.SetMarkerSize(0.4);

    plot4->GetPad(2)->SetFrameFillColor(0);
    plot4->GetPad(2)->SetFrameBorderMode(0);
    plot4->GetPad(2)->SetBottomMargin(0.17);
    plot4->GetPad(2)->SetTopMargin(0.04);
    plot4->GetPad(2)->SetRightMargin(0.01);
    plot4->GetPad(2)->SetLeftMargin(0.08);
	

	residuen_log.DrawClone("APE");
    TLine *line3 = new TLine(t[0],0,t[n-1]+1,0);
    line3->SetLineColor(kRed);
    line3->Draw("same");
    TLegend* leg6= new TLegend(0.4,0.2,0.6,0.4);
    leg6->AddEntry("residuen","Residuum+Fehler","EP");
    leg6->SetFillColor(0);
    leg6->Draw("same");

//////////////////////
//	Ausgabe der Ergebnisse
	double p1 = f->GetParameter(0);
	double p2 = f->GetParameter(1);
	double t0 = -p2/p1;
	const double* t0_e=Fitpara_lin->GetErrors();
	double t0_err = t0*sqrt(pow((t0_e[0])/(p1),2)+pow((t0_e[1])/(p2),2));
	cout << "T1_zero= " << t0/log(2) << " +/- " << t0_err/log(2) << endl;	
	double p3 = f_exp->GetParameter(0);
	double p4 = f_exp->GetParameter(1);
	double T1_exp = -1/p4;
	const double* t1_e=Fitpara_exp->GetErrors();
	double t1_err = pow(T1_exp,2)*t1_e[1];
	cout << "T1_exp= " << T1_exp << " +/- " << t1_err  << endl;
	
	double p5 = f_log->GetParameter(0);
	double p6 = f_log->GetParameter(1);
	double T1_log = -1/p5;
	const double* t2_e=Fitpara_log->GetErrors();
	double t2_err = pow(T1_log,2)*t2_e[0];
	cout << "T1_log= " << T1_log << " +/- " << t2_err << endl;	

	return 0;
}





