#include <iostream>
#include "read_mca.h"
#include <cmath>

using namespace std;

//n = Anzahl Werte
double* RMS(int n, double* vals){
	double mean=0;
	double var=0;
	for(int i=0;i<n;i++) mean+=vals[i];
	mean=mean/(n+0.0);
	for(int i=0;i<n;i++)var+=pow((vals[i]-mean),2);
	var=var/(n-1.0);
	double* toret = new double[2];
	toret[0]=mean;
	toret[1]=sqrt(var);
	return toret;
}

double RMS(int n, double* vals, double mean){
	double var=0;
	double sig=0;
	for(int i=0;i<n;i++) var+=pow((vals[i]-mean),2);
	var=var/(n+0.0);
	sig=pow(var,0.5);
	return sig;
}

//Funktion berechnet aus ergebnissen des T9 programms den sysfehler auf sigma
double sig_sys(int n,double* ns_o, double* ns_M, double n_o){
	double L=134.4;//Luminositaet bei 91,33 GeV
	double mean=1/L*n_o/ns_M[0]*ns_o[0];//Mittelwert - originalwerte stehen im 0. Element
	cout << "sig mean = " << mean << endl;
	double* sigs=new double[n-1];
	for(int i=1; i<n; i++){
		sigs[i-1]=1/L*n_o/ns_M[i]*ns_o[i];//sigmas ausrechnen und dann mit RMS sigma bestimmen
		cout << sigs[i] << endl;
	}
	double sys=0;
	sys=RMS(n-1,sigs,mean);
	cout<<"sys= "<<sys<<endl;
	return sys;
}

int main(){
	

	double Us[10]={1.12,1.1,1.11,1.11,1.11,1.1,1.12,1.11,1.12,1.1};
	double std=RMS(10,Us)[1];

	cout << "messung std dev: "  <<std <<endl;

	return 0;
}

