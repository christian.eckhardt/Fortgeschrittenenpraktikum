#include "TROOT.h"
#include "read_mca.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TLegend.h"
#include "TLine.h"
#include "TMath.h"
#include "TBox.h"
using namespace std;

int echo(){


    gStyle->SetOptStat(0);
    gStyle->SetOptFit(1);

    int n= read_mca("../Data/Carr_Dat")[0];
	cout<<n<<endl;
    double* Carr_raw = &read_mca("../Data/Carr_Dat")[1];
    double* Gill_raw = &read_mca("../Data/Gill_Dat")[1];

	double* Carr_U=new double[n/2];
	double* Carr_t=new double[n/2];
	double* Gill_U=new double[n/2];
	double* Gill_t=new double[n/2];
	double* Err=new double[n/2];

	for(int i=0;i<n;i++){
		Carr_t[i/2]=Carr_raw[i];
		Carr_U[i/2]=Carr_raw[i+1];
		Gill_t[i/2]=Gill_raw[i];
		Gill_U[i/2]=Gill_raw[i+1];
		Err[i/2]=0.00816497;
		i++;
	}
	n=n/2;
//	for(int i=0; i<n;i++){
//		cout << Gill_U[i]<< " " << Gill_t[i] <<endl;
//	}
    TMultiGraph *multi= new TMultiGraph();	
    TGraphErrors* gill = new TGraphErrors(n,Gill_t,Gill_U,nullptr,Err);
    gill->SetTitle("Meiboom-Gill Sequenz vs. Car-Purcell Sequenz");

    TGraphErrors* carr = new TGraphErrors(n,Carr_t,Carr_U,nullptr,Err);

    TCanvas* vgl=new TCanvas("vgl","Meiboom-Gill Sequenz vs. Carr-Purcell Sequenz",900,600);
    vgl->SetBottomMargin(0.1);
    vgl->SetTopMargin(0.07);
    vgl->SetRightMargin(0.01);
    vgl->SetLeftMargin(0.1);

    gill->GetYaxis()->SetLabelSize(0.052);
    gill->GetYaxis()->SetTitleSize(0.052);
    gill->GetYaxis()->SetTitleOffset(0.8);
    gill->GetYaxis()->SetTitle("Spannung [V]");
    gill->GetXaxis()->SetTitle("Zeit [s]");
    gill->GetYaxis()->SetLabelSize(0.05);
    gill->GetYaxis()->SetTitleSize(0.05);
    gill->GetXaxis()->SetLabelSize(0.05);
    gill->GetXaxis()->SetTitleSize(0.05);
    gill->SetLineColor(2);
    gill->SetFillColor(0);
    gill->Draw();
    carr->SetLineColor(4);
    carr->SetFillColor(0);
    carr->Draw("same");
  
    multi->Add(gill);
    multi->Add(carr);
  //  multi->Draw("APE");
    TLegend* leg_vgl= new TLegend(0.84,0.78,0.99,0.93);
    leg_vgl->AddEntry(gill,"MG");
    leg_vgl->AddEntry(carr,"CP");
    leg_vgl->Draw("same");
    vgl->SaveAs("../Protokoll/Protokoll_Data/vgl.pdf","pdf");
  
	TCanvas* c_gill = new TCanvas("c_gill","Meiboom-Gill Sequenz",900,600);	
	gill->SetTitle("Meiboom-Gill Sequenz");
	gill->SetLineColor(1);
	gill->Draw();
	c_gill->SaveAs("../Protokoll/Protokoll_Data/mg_sequenz.pdf","pdf");

/*
	TCanvas* peakfind =new TCanvas("peakfind","peakfind",900,600);
	gill->GetXaxis()->SetLimits(0.014,0.018);
	gill->SetTitle("Beispiel zur Peakbestimmung");

	double left_value=0.01595;
	double right_value=0.01605;
	double peak_value=0.016;
	TLine* left= new TLine(left_value,0,left_value,1.71);
	TLine* right= new TLine(right_value,0,right_value,1.71);
	TLine* peak= new TLine(peak_value,0,peak_value,1.71);
	peak->SetLineColor(2);
	TBox* area= new TBox(left_value,0,right_value,1.71);
	area->SetFillColor(4);
	area->SetFillStyle(3004);
	
	gill->Draw();
	area->Draw("same");
	left->Draw("same");
	peak->Draw("same");
	right->Draw("same");

	TLegend* leg_peak= new TLegend(0.7,0.7,0.9,0.9);
	leg_peak->AddEntry(area,"Fehlerabschätzung","F");
	leg_peak->AddEntry(gill,"Datenpunkte","EL");
	leg_peak->AddEntry(left,"linke und rechte Grenze","L");
	leg_peak->AddEntry(peak,"Peakposition","L");
	leg_peak->Draw("same");
	peakfind->SaveAs("../Protokoll/Protokoll_Data/peakfind.pdf","pdf");
*/	return 0;

  
}
