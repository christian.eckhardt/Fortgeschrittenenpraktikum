#include "TROOT.h"
#include "read_mca.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TLegend.h"
#include "TLine.h"
#include "TMath.h"

using namespace std;

int Meiboom(){


    gStyle->SetOptStat(0);
    gStyle->SetOptFit(1);

    int n= read_mca("../Data/peaks")[0];
    double* t = &read_mca("../Data/peaks")[1];
    double* t_err = &read_mca("../Data/peak_sigs")[1];
	int n_long=read_mca("../Data/Gill_Dat")[0];
	double* Gill_raw = &read_mca("../Data/Gill_Dat")[1];
	double* U_raw = new double[n_long/2];
	double* t_raw = new double[n_long/2];
	double* U = new double[n];
	double* U_err = new double[n];

	for(int i=0;i<n_long;i++){
        t_raw[i/2]=Gill_raw[i];
		U_raw[i/2]=Gill_raw[i+1];
		i++;
	}

	for(int j=0;j<n;j++){
		for(int i=0;i<n_long/2;i++){
			if(abs(t_raw[i]-t[j])<0.000001) {
				U[j]=U_raw[i]-U_raw[n_long/2-1 ];
				cout << U[j] << " " << U_raw[i] << " "<<t_raw[i] <<endl;
			}
		}
//	cout << U[j] << endl;
	U_err[j]=sqrt(pow(0.00816497,2)+pow(0.008,2)/12);
	}

	TGraphErrors* graph_exp = new TGraphErrors(n,t,U,t_err,U_err);
    graph_exp->SetTitle("Exp-Fit");

    TCanvas* plot3=new TCanvas("Exp_Fit","Hahn Methode",900,600);
	plot3->cd();
	plot3->Divide(1,2,0,0);
	plot3->cd(1);
    plot3->GetPad(1)->SetPad(0,0.4,1.,1.);
    plot3->GetPad(1)->SetBottomMargin(0.1);
    plot3->GetPad(1)->SetTopMargin(0.07);
    plot3->GetPad(1)->SetRightMargin(0.01);
    plot3->GetPad(1)->SetLeftMargin(0.08);

    graph_exp->GetYaxis()->SetLabelSize(0.052);
    graph_exp->GetYaxis()->SetTitleSize(0.052);
    graph_exp->GetYaxis()->SetTitleOffset(0.8);
    graph_exp->GetYaxis()->SetTitle("Spannung [V]");
    graph_exp->GetXaxis()->SetTitle("Zeit [s]");
	graph_exp->GetYaxis()->SetLabelSize(0.05);
    graph_exp->GetYaxis()->SetTitleSize(0.05);
	graph_exp->GetXaxis()->SetLabelSize(0.05);
    graph_exp->GetXaxis()->SetTitleSize(0.05);

	TF1* f_exp=new TF1("Exponential","[0]*exp(-x*[1])",t[0],t[n-1]);
	f_exp->SetParameter(0,1.6);
	f_exp->SetParameter(1,88);

	auto Fitpara_exp=graph_exp->Fit(f_exp,"S","q");
    graph_exp->Draw();
    f_exp->Draw("Same");

    TLegend* leg3=new TLegend(0.1,0.7,0.3,0.9);
    leg3->AddEntry("graph","Daten","EP");
    leg3->AddEntry(f_exp,"Fit","L");
    leg3->SetFillColor(0);
    leg3->Draw("same");

	double res_exp[n];
	double res_err[n];	
	
	double f_0=f_exp->GetParameter(0);
	double f_1=f_exp->GetParameter(1);
    
	for (int i=0;i<n;i++){
        res_exp[i]=U[i]-f_exp->Eval(t[i]);
		res_err[i]=sqrt(pow(U_err[i],2)+pow(t_err[i]*f_0*f_1*exp(-f_1*t[i]),2));
    }

    plot3->cd();
    plot3->cd(2);

    plot3->GetPad(2)->SetPad(0.,0.,1.,0.4);
    TGraphErrors residuen_exp(n,t,res_exp,nullptr,res_err);

    plot3->GetPad(2)->SetGridy();
    residuen_exp.GetXaxis()->SetLabelSize(0.08);
    residuen_exp.GetXaxis()->SetTitle("Zeit [s]");
    residuen_exp.GetXaxis()->SetTitleSize(0.08);
    residuen_exp.GetYaxis()->SetTitle("Residuum");
    residuen_exp.GetYaxis()->SetLabelSize(0.08);
    residuen_exp.GetYaxis()->SetTitleSize(0.08);
    residuen_exp.GetYaxis()->SetTitleOffset(0.55);
    residuen_exp.SetMarkerStyle(8);
    residuen_exp.SetMarkerSize(0.4);

    plot3->GetPad(2)->SetFrameFillColor(0);
    plot3->GetPad(2)->SetFrameBorderMode(0);
    plot3->GetPad(2)->SetBottomMargin(0.17);
    plot3->GetPad(2)->SetTopMargin(0.04);
    plot3->GetPad(2)->SetRightMargin(0.01);
    plot3->GetPad(2)->SetLeftMargin(0.08);

	residuen_exp.DrawClone("APE");

    TLine *line2 = new TLine(t[0],0,t[n-1]+1,0);
    line2->SetLineColor(kRed);
    line2->Draw("same");
    TLegend* leg4= new TLegend(0.4,0.2,0.6,0.4);
    leg4->AddEntry("residuen","Residuum+Fehler","EP");
    leg4->SetFillColor(0);
    leg4->Draw("same");

//////////////////////////
//	log fit
//zunaechst logdaten 

	double* U_lin = new double[n];
	double* U_lin_err = new double[n];
		
	for(int i=0;i<n;i++){
		U_lin[i] = log(U[i]);
		U_lin_err[i] = U_err[i]/U[i];
	}


		
	TGraphErrors* graph_log = new TGraphErrors(n,t,U_lin,t_err,U_lin_err);
    graph_log->SetTitle("Linearisierter Fit");

    TCanvas* plot4=new TCanvas("Lin_Log","Linearisierte Daten",900,600);

	plot4->Divide(1,2,0,0);
	plot4->cd(1);
    plot4->GetPad(1)->SetPad(0,0.4,1.,1.);
    plot4->GetPad(1)->SetBottomMargin(0.1);
    plot4->GetPad(1)->SetTopMargin(0.07);
    plot4->GetPad(1)->SetRightMargin(0.01);
    plot4->GetPad(1)->SetLeftMargin(0.08);

    graph_log->GetYaxis()->SetLabelSize(0.052);
    graph_log->GetYaxis()->SetTitleSize(0.052);
    graph_log->GetYaxis()->SetTitleOffset(0.8);
    graph_log->GetYaxis()->SetTitle("Spannung [V]");
    graph_log->GetXaxis()->SetTitle("Zeit [s]");
	graph_log->GetYaxis()->SetLabelSize(0.05);
    graph_log->GetYaxis()->SetTitleSize(0.05);
	graph_log->GetXaxis()->SetLabelSize(0.05);
    graph_log->GetXaxis()->SetTitleSize(0.05);

	TF1* f_log=new TF1("Lin_Log","[0]*x+[1]",t[0], t[n-1]);
	f_log->SetParameter(0,-1);
	f_log->SetParameter(1,1);

	auto Fitpara_log=graph_log->Fit(f_log,"S","q");
    graph_log->Draw();
    f_log->Draw("Same");

    TLegend* leg5=new TLegend(0.1,0.7,0.3,0.9);
    leg5->AddEntry("graph","Daten","EP");
    leg5->AddEntry(f_log,"Fit","L");
    leg5->SetFillColor(0);
    leg5->Draw("same");

	double res3[n];
    double res_lin_err[n];
	double f_l=f_log->GetParameter(1);

    for(int i=0;i<n;i++){
        res3[i]=U_lin[i]-f_log->Eval(t[i]);
		res_lin_err[i] = sqrt(pow(U_lin_err[i],2)+pow(f_l*t_err[i],2));
    }

    plot4->cd();
    plot4->cd(2);


    plot4->GetPad(2)->SetPad(0.,0.,1.,0.4);
    TGraphErrors residuen_log(n,t,res3,nullptr,U_lin_err);

    plot4->GetPad(2)->SetGridy();
    residuen_log.GetXaxis()->SetLabelSize(0.08);
    residuen_log.GetXaxis()->SetTitle("Zeit [s]");
    residuen_log.GetXaxis()->SetTitleSize(0.08);
    residuen_log.GetYaxis()->SetTitle("Residuum");
    residuen_log.GetYaxis()->SetLabelSize(0.08);
    residuen_log.GetYaxis()->SetTitleSize(0.08);
    residuen_log.GetYaxis()->SetTitleOffset(0.55);
    residuen_log.SetMarkerStyle(8);
    residuen_log.SetMarkerSize(0.4);

    plot4->GetPad(2)->SetFrameFillColor(0);
    plot4->GetPad(2)->SetFrameBorderMode(0);
    plot4->GetPad(2)->SetBottomMargin(0.17);
    plot4->GetPad(2)->SetTopMargin(0.04);
    plot4->GetPad(2)->SetRightMargin(0.01);
    plot4->GetPad(2)->SetLeftMargin(0.08);
	

	residuen_log.DrawClone("APE");
    TLine *line3 = new TLine(t[0],0,t[n-1]+1,0);
    line3->SetLineColor(kRed);
    line3->Draw("same");
    TLegend* leg6= new TLegend(0.4,0.2,0.6,0.4);
    leg6->AddEntry("residuen","Residuum+Fehler","EP");
    leg6->SetFillColor(0);
    leg6->Draw("same");


	return 0;
}
