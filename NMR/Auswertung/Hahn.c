#include "TROOT.h"
#include "read_mca.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TLegend.h"
#include "TLine.h"
#include "TMath.h"

using namespace std;

int Hahn(){


    gStyle->SetOptStat(0);
    gStyle->SetOptFit(1);

    int n= read_mca("../Data/Hahn")[0];
	n=n-2;
    double* U = &read_mca("../Data/Hahn")[3];
    double* t = &read_mca("../Data/Hahn_t")[3];
	double* U_err = new double[n];
	for(int i=0;i<n;i++){ 
		U_err[i]=0.01;
		t[i]=2*t[i]; //relaxation während der zeit 2tau
		U[i]=U[i];
	}


	TGraphErrors* graph_exp = new TGraphErrors(n,t,U,nullptr,U_err);
    graph_exp->SetTitle("Exp-Fit");

    TCanvas* plot3=new TCanvas("Exp_Fit","Hahn Methode",900,600);
	plot3->cd();
	plot3->Divide(1,2,0,0);
	plot3->cd(1);
    plot3->GetPad(1)->SetPad(0,0.4,1.,1.);
    plot3->GetPad(1)->SetBottomMargin(0.1);
    plot3->GetPad(1)->SetTopMargin(0.07);
    plot3->GetPad(1)->SetRightMargin(0.01);
    plot3->GetPad(1)->SetLeftMargin(0.08);

    graph_exp->GetYaxis()->SetLabelSize(0.052);
    graph_exp->GetYaxis()->SetTitleSize(0.052);
    graph_exp->GetYaxis()->SetTitleOffset(0.8);
    graph_exp->GetYaxis()->SetTitle("Spannung [V]");
    graph_exp->GetXaxis()->SetTitle("Zeit [s]");
	graph_exp->GetYaxis()->SetLabelSize(0.05);
    graph_exp->GetYaxis()->SetTitleSize(0.05);
	graph_exp->GetXaxis()->SetLabelSize(0.05);
    graph_exp->GetXaxis()->SetTitleSize(0.05);

	TF1* f_exp=new TF1("Exponential","[0]*exp(-x*[1])",t[0],t[n-1]);
	f_exp->SetParameter(0,1.6);
	f_exp->SetParameter(1,88);

	auto Fitpara_exp=graph_exp->Fit(f_exp,"S","q");
    graph_exp->Draw();
    f_exp->Draw("Same");

    TLegend* leg3=new TLegend(0.1,0.7,0.3,0.9);
    leg3->AddEntry("graph","Daten","EP");
    leg3->AddEntry(f_exp,"Fit","L");
    leg3->SetFillColor(0);
    leg3->Draw("same");

	double res_exp[n];

    for (int i=0;i<n;i++){
        res_exp[i]=U[i]-f_exp->Eval(t[i]);
    }

    plot3->cd();
    plot3->cd(2);

    plot3->GetPad(2)->SetPad(0.,0.,1.,0.4);
    TGraphErrors residuen_exp(n,t,res_exp,nullptr,U_err);

    plot3->GetPad(2)->SetGridy();
    residuen_exp.GetXaxis()->SetLabelSize(0.08);
    residuen_exp.GetXaxis()->SetTitle("Zeit [s]");
    residuen_exp.GetXaxis()->SetTitleSize(0.08);
    residuen_exp.GetYaxis()->SetTitle("Residuum");
    residuen_exp.GetYaxis()->SetLabelSize(0.08);
    residuen_exp.GetYaxis()->SetTitleSize(0.08);
    residuen_exp.GetYaxis()->SetTitleOffset(0.55);
    residuen_exp.SetMarkerStyle(8);
    residuen_exp.SetMarkerSize(0.4);

    plot3->GetPad(2)->SetFrameFillColor(0);
    plot3->GetPad(2)->SetFrameBorderMode(0);
    plot3->GetPad(2)->SetBottomMargin(0.17);
    plot3->GetPad(2)->SetTopMargin(0.04);
    plot3->GetPad(2)->SetRightMargin(0.01);
    plot3->GetPad(2)->SetLeftMargin(0.08);

	residuen_exp.DrawClone("APE");

    TLine *line2 = new TLine(t[0],0,t[n-1]+1,0);
    line2->SetLineColor(kRed);
    line2->Draw("same");
    TLegend* leg4= new TLegend(0.4,0.2,0.6,0.4);
    leg4->AddEntry("residuen","Residuum+Fehler","EP");
    leg4->SetFillColor(0);
    leg4->Draw("same");

	return 0;
}
