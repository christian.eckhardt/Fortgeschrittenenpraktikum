#ifndef _TOHIST_H_INCLUDED
#define _TOHIST_H_INCLUDED

#include <vector>
using namespace std;

double* tohist(double* data){

	const int size = data[0];
	vector<double> hist;
	double num=0;
	for(int i=1;i<size+1;i++){
		num=data[i];
		for(int j=0;j<num;j++){
			hist.push_back(i);
		}
	}
	hist.insert(hist.begin(),hist.size());
	double* toreturn=new double[hist.size()];
	copy(hist.begin(), hist.end(), toreturn);
	return toreturn;
}

#endif


