#include "TROOT.h"
#include "read_mca.h"
#include "tohist.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TLegend.h"
#include "TLine.h"
#include "TMath.h"

using namespace std;

int ion(){
	

	gStyle->SetOptStat(0);
	gStyle->SetOptFit(1);
	int n = read_mca("../ion")[0];
	double* I = &read_mca("../ion")[1];
	double* dist = &read_mca("../dist")[1];
//	wurzel n fehler
	double* I_err = &read_mca("../ion_err")[1];

//	calculate real distances
//	see scatch in block to find out how
	for(int i=0;i<n;i++){
	dist[i]=9.74-dist[i];
	}
//	Abstandskorrektur

//	for(int i=0;i<n;i++){
//	I[i]=I[i]*dist[i];
//	}


	
	TGraphErrors* graph = new TGraphErrors(n,dist,I,nullptr,I_err);
	graph->SetTitle("Ionisation Chamber;Distance [cm]; Current[0.1 nA]");

    TCanvas* plot=new TCanvas("Ion_Chamber","Ionisation Chamber",900,600);
    plot->SetBottomMargin(0.1);
    plot->SetTopMargin(0.07);
    plot->SetRightMargin(0.01);
    plot->SetLeftMargin(0.1);
	
    graph->GetYaxis()->SetLabelSize(0.052);
    graph->GetYaxis()->SetTitleSize(0.052);
    graph->GetYaxis()->SetTitleOffset(0.8);
	graph->GetXaxis()->SetRangeUser(0.0,6.0);	

    graph->DrawClone();

/*
    TLine *line = new TLine(belag[0]-1,0,belag[n-1]+1,0);
    line->SetLineColor(kRed);
    line->Draw("same");
    TLegend* leg2= new TLegend(0.4,0.2,0.6,0.4);
    leg2->AddEntry("residuen","Residuum+Fehler","EP");
    leg2->SetFillColor(0);
    leg2->Draw("same");
*/

return 0;
}
