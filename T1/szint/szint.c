#include "TROOT.h"
#include "read_mca.h"
#include "tohist.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TLegend.h"
#include "TLine.h"
#include "TMath.h"

using namespace std;


int szint(){

	gStyle->SetOptStat(0);
	gStyle->SetOptFit(1);


	int n_pars = 3;
	int n=read_mca("Szintillator_organisch.TKA")[0];
	cout << n << endl;
	double* org=&read_mca("Szintillator_organisch.TKA")[1];
	double* NaI=&read_mca("Szintillator_anorganisch.TKA")[1];

	TH1D* hist_o = new TH1D("spec_o", "spectrum organic szintillator;Channel;Counts",n,1,n);
	TH1D* hist_a = new TH1D("spec_a", "spectrum anorganic szintillator;Channel;Counts",n,1,n);
	TCanvas* plot_o=new TCanvas("speco","Spectrum anorganic",900,600);

	for(int i=1;i<n+1;i++){
		for(int j=0; j<org[i-1];j++) hist_o->Fill(i);
		for(int j=0; j<NaI[i-1];j++) hist_a->Fill(i);
	}

///////////integrate over whole histogram to get detection probability

	Int_o=hist_o->Integral();
	Int_a=hist_a->Integral();

	cout << "Int_o= " << Int_o << endl;
	cout << "Int_a= " << Int_a << endl;

	Int_o_p=hist_o->Integral(2000,3500);
	Int_a_p=hist_a->Integral(2000,3500);

	cout << "Int_o_peak= " << Int_o_p << endl;
	cout << "Int_a_peak= " << Int_a_p << endl;
/////////////



	plot_o->SetBottomMargin(0.12);
    plot_o->SetTopMargin(0.07);
    plot_o->SetRightMargin(0.02);
    plot_o->SetLeftMargin(0.1);

    hist_o->GetXaxis()->SetLabelSize(0.052);
    hist_o->GetXaxis()->SetTitleSize(0.052);
    hist_o->GetYaxis()->SetLabelSize(0.052);
    hist_o->GetYaxis()->SetTitleSize(0.052);
    hist_o->GetYaxis()->SetTitleOffset(1);

	plot_o->cd();
	hist_o->Draw("E");

	TLine *line1 = new TLine(2000,0,2000,758);
	line1->SetLineColor(kRed);
	line1->Draw("same");

	TLine *line2 = new TLine(3500,0,3500,758);
	line2->SetLineColor(kRed);
	line2->Draw("same");

	TCanvas* plot_a=new TCanvas("speca","Spectrum anorganic",900,600);



	plot_a->SetBottomMargin(0.12);
    plot_a->SetTopMargin(0.07);
    plot_a->SetRightMargin(0.02);
    plot_a->SetLeftMargin(0.1);

    hist_a->GetXaxis()->SetLabelSize(0.052);
    hist_a->GetXaxis()->SetTitleSize(0.052);
    hist_a->GetYaxis()->SetLabelSize(0.052);
    hist_a->GetYaxis()->SetTitleSize(0.052);
    hist_a->GetYaxis()->SetTitleOffset(1);

	plot_a->cd();
    hist_a->Draw("E");

	TLine *line3 = new TLine(2000,0,2000,1755);
	line3->SetLineColor(kRed);
	line3->Draw("same");

	TLine *line4 = new TLine(3500,0,3500,1755);
	line4->SetLineColor(kRed);
	line4->Draw("same");

	return 0;


}

