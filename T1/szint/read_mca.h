#ifndef _READ_ASCII_H_INCLUDED
#define _READ_ASCII_H_INCLUDED
#include <stdexcept>
#include <fstream>
#include <string>
#include <vector>
using namespace std;


double* read_mca(char const* dest){

	ifstream infile;
	infile.open(dest);
	if(!infile) cerr<<"File not open"<<endl;
	string temp_s;
	double temp_d;
	vector<double> array;
	string begin="<<DATA>>";
	string end="<<END>>";
	bool dat=false;	
	while(infile >> temp_s){
		if (temp_s.compare(begin)==0){dat=true; continue;}
		if (temp_s.compare(end)==0){dat=false; break;}
		if(dat){
			try{
				temp_d = stod(temp_s);
			}
			catch (const invalid_argument& ia){
				cerr << "Inhalt ist keine Zahl" << endl;
			}
			
			array.push_back(temp_d);
		}
	}
	array.insert(array.begin(),array.size());
	cout << "Found " << array.size() << " Values" << endl;
	
	double* toreturn=new double[array.size()];
	copy(array.begin(), array.end(), toreturn);
	return toreturn;
}

#endif

