#include "read_mca.h"
#include "TH1D.h"
#include "tohist.h"
#include "TGraphErrors.h"
#include "TROOT.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TLine.h"
#include "TLegend.h"
#include "TLatex.h"
#include "TPDF.h"
#include <string>
#include <vector>
#include "to_csv.h"

double PI = 4*atan(1);


double fkt1(double* vars, double* params){
	return params[0]*exp(vars[0]*params[1]);
}

double d_fkt1(double* vars, double* params){
	return params[0]*params[1]*exp(vars[0]*params[1]);
}

void fit(const char* dat_name, double* params, double low, double high,const char* canvas_name,int dat_line){

	gStyle->SetOptStat(0);
	gStyle->SetOptFit(1);

	int n_p=high-low;
	int n_par = 2;
	int n=read_mca(dat_name)[0];//-2 weil auch zeiten im file stehen
	double* data=&read_mca(dat_name)[1];

//	normalize data to dead time
	TH1D* hist=new TH1D(canvas_name, "Fit;Channel;Counts",n_p,low,high);
	double real_bins [11] = {0,2,4,6,8,10,12,14,16,18,20};	
	hist->SetBins(10,real_bins);

	for(int i=0;i<n;i++){
		for(int j=0;j<data[i];j++){
			hist->Fill(2*i);	
		}
	}
	//stumpfes korrigieren des hinzufuegens zu kleiner bins
	hist->SetBinContent(0,hist->GetBinContent(1));	

//////////hier spaeter rumfitten
	TCanvas* plot=new TCanvas(canvas_name,dat_name,900,600);
	plot->Divide(1,2,0,0);

	plot->cd(1);
	plot->GetPad(1)->SetPad(0.,0.4,1.,1.);

	plot->GetPad(1)->SetBottomMargin(0.012);
	plot->GetPad(1)->SetTopMargin(0.07);
	plot->GetPad(1)->SetRightMargin(0.02);
	plot->GetPad(1)->SetLeftMargin(0.08);

	hist->GetXaxis()->SetLabelSize(0.052);
	hist->GetXaxis()->SetTitleSize(0.052);
	hist->GetYaxis()->SetLabelSize(0.052);
	hist->GetYaxis()->SetTitleSize(0.052);
	hist->GetYaxis()->SetTitleOffset(0.8);


	hist->Draw("E0");
	
/////////
	//Funktion fuer fit initialisieren

	TF1* fit_fkt=new TF1("fkt",fkt1,low,high,n_par);
	TF1* fit_fkt_d=new TF1("fkt_d",d_fkt1,low,high,n_par);
	fit_fkt->SetLineColor(kRed);
	for(int i=0; i<n_par; i++) fit_fkt->SetParameter(i,params[i]);
//	fit_fkt->FixParameter(2,300);
	auto fit_res=hist->Fit(fit_fkt,"S","q",low,high);
	for(int i=0;i<n_par;i++) fit_fkt_d->SetParameter(i,fit_fkt->GetParameter(i));

	TLegend* leg1= new TLegend(0.4,0.75,0.6,0.935);
	leg1->AddEntry(hist,"Histogram+Fehler","EL");
	leg1->AddEntry(fit_fkt,"FIT","l");
	leg1->SetFillColor(0);
	leg1->Draw("same");


///////
//	jetzt residuen an den start bringen
	double res[n_p];
	double res_err[n_p];
	double bins[n_p];
	for(int i=0;i<n_p;i++){
		res[i]=hist->GetBinContent(i+1)-fit_fkt->Eval(hist->GetBinCenter(i+1));
		bins[i]=hist->GetBinCenter(i+1);
		cout << hist->GetBinCenter(i+1);
		res_err[i]=sqrt(hist->GetBinContent(i+1));
	}
	
	TGraphErrors* residuen = new TGraphErrors(n_p,bins,res,nullptr,res_err);
	
	plot->cd();
	plot->cd(2);
	plot->GetPad(2)->SetPad(0.,0.,1.,0.4);	


	residuen->GetXaxis()->SetLabelSize(0.08);
	residuen->GetXaxis()->SetTitle("Thickness-1 [mm]");
	residuen->GetXaxis()->SetTitleSize(0.08);
	residuen->GetYaxis()->SetTitle("Residuum");
	residuen->GetYaxis()->SetLabelSize(0.08);
	residuen->GetYaxis()->SetTitleSize(0.08);
	residuen->GetXaxis()->SetLimits(low,high);
	residuen->GetYaxis()->SetTitleOffset(0.6);
	residuen->SetMarkerStyle(8);
	residuen->SetMarkerSize(0.4);

	plot->GetPad(2)->SetFrameFillColor(0);
	plot->GetPad(2)->SetFrameBorderMode(0);

	plot->GetPad(2)->SetBottomMargin(0.17);
	plot->GetPad(2)->SetTopMargin(0.04);
	plot->GetPad(2)->SetRightMargin(0.02);
	plot->GetPad(2)->SetLeftMargin(0.08);

	residuen->Draw("APE");
	
	TLine *line = new TLine(low,0,high,0);
	line->SetLineColor(kRed);
	line->Draw("same");
	TLegend* leg2= new TLegend(0.8,0.8,0.98,0.92);
	leg2->AddEntry("residuen","Residuum+Fehler","EP");
	leg2->SetFillColor(0);
	leg2->Draw("same");

}

int exp_fit(){

	double params0[2]={1e+06,-0.005};
	fit("Pb_exp", params0, 0, 20, "c0",0);
return 0;
}


