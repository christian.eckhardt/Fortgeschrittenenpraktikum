#include "read_mca.h"
#include "TH1D.h"
#include "tohist.h"
#include "TGraphErrors.h"
#include "TROOT.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TLine.h"
#include "TLegend.h"
#include "TLatex.h"
#include "TPDF.h"
#include <string>
#include <vector>
#include "to_csv.h"

double PI = 4*atan(1);


double fkt1(double* vars, double* params){
	return params[0]*exp(vars[0]*params[1])
	+params[2]*TMath::Gaus(vars[0],params[3],params[4]);
}

double d_fkt1(double* vars, double* params){
	return params[0]*params[1]*exp(vars[0]*params[1])+
	params[2]*(vars[0]-params[3])/pow(params[4],2)
	*TMath::Gaus(vars[0],params[3],params[4]);
}

void fit(const char* dat_name, double* params, double low, double high,const char* canvas_name,int dat_line){

	gStyle->SetOptStat(0);
	gStyle->SetOptFit(1);

	int n_p=high-low;
	int n_par = 5;
	int n=read_mca(dat_name)[0]-2;//-2 weil auch zeiten im file stehen
	double t_real = read_mca(dat_name)[2];
	double t_dead = read_mca(dat_name)[1];
	double* data=&read_mca(dat_name)[3];

//	normalize data to dead time
	for(int i=0;i<n;i++) data[i] = data[i]*t_dead/t_real;

	TH1D* hist=new TH1D(canvas_name, "Fit;Channel;Counts",n_p,low,high);
	for(int i=0;i<n;i++){
		for(int j=0;j<data[i];j++){
			hist->Fill(i);	
		}
	}
	//stumpfes korrigieren des hinzufuegens zu kleiner bins
	hist->SetBinContent(0,hist->GetBinContent(1));	

//////////hier spaeter rumfitten
	TCanvas* plot=new TCanvas(canvas_name,dat_name,900,600);
	plot->Divide(1,2,0,0);

	plot->cd(1);
	plot->GetPad(1)->SetPad(0.,0.4,1.,1.);

	plot->GetPad(1)->SetBottomMargin(0.012);
	plot->GetPad(1)->SetTopMargin(0.07);
	plot->GetPad(1)->SetRightMargin(0.02);
	plot->GetPad(1)->SetLeftMargin(0.08);

	hist->GetXaxis()->SetLabelSize(0.052);
	hist->GetXaxis()->SetTitleSize(0.052);
	hist->GetYaxis()->SetLabelSize(0.052);
	hist->GetYaxis()->SetTitleSize(0.052);
	hist->GetYaxis()->SetTitleOffset(0.8);


	hist->Draw("E");
	
/////////
	//Funktion fuer fit initialisieren
	TF1* fit_fkt=new TF1("fkt",fkt1,low,high,n_par);
	TF1* fit_fkt_d=new TF1("fkt_d",d_fkt1,low,high,n_par);
	fit_fkt->SetLineColor(kRed);
	for(int i=0; i<n_par; i++) fit_fkt->SetParameter(i,params[i]);
//	fit_fkt->FixParameter(2,300);
	auto fit_res=hist->Fit(fit_fkt,"S","q",low,high);
	for(int i=0;i<n_par;i++) fit_fkt_d->SetParameter(i,fit_fkt->GetParameter(i));

	TLegend* leg1= new TLegend(0.4,0.75,0.6,0.935);
	leg1->AddEntry(hist,"Histogram+Fehler","EL");
	leg1->AddEntry(fit_fkt,"FIT","l");
	leg1->SetFillColor(0);
	leg1->Draw("same");


///////
//	jetzt residuen an den start bringen
	double res[n_p];
	double res_err[n_p];
	double bins[n_p];
	for(int i=0;i<n_p;i++){
		res[i]=hist->GetBinContent(i+1)-fit_fkt->Eval(hist->GetBinCenter(i+1));
		bins[i]=hist->GetBinCenter(i+1);
		res_err[i]=sqrt(hist->GetBinContent(i+int(low)+1)+
			pow(fit_fkt_d->Eval(hist->GetBinCenter(i+int(low)+1))*1/sqrt(12),2));
	}
	
	TGraphErrors* residuen = new TGraphErrors(n_p,bins,res,nullptr,res_err);
	
	plot->cd();
	plot->cd(2);
	plot->GetPad(2)->SetPad(0.,0.,1.,0.4);	

	residuen->GetXaxis()->SetLabelSize(0.08);
	residuen->GetXaxis()->SetTitle("Channel");
	residuen->GetXaxis()->SetTitleSize(0.08);
	residuen->GetYaxis()->SetTitle("Residuum");
	residuen->GetYaxis()->SetLabelSize(0.08);
	residuen->GetYaxis()->SetTitleSize(0.08);
	residuen->GetXaxis()->SetLimits(low-2,high+2);
	residuen->GetYaxis()->SetTitleOffset(0.4);
	residuen->SetMarkerStyle(8);
	residuen->SetMarkerSize(0.4);

	plot->GetPad(2)->SetFrameFillColor(0);
	plot->GetPad(2)->SetFrameBorderMode(0);

	plot->GetPad(2)->SetBottomMargin(0.17);
	plot->GetPad(2)->SetTopMargin(0.04);
	plot->GetPad(2)->SetRightMargin(0.02);
	plot->GetPad(2)->SetLeftMargin(0.08);

	residuen->Draw("APE");
	
	TLine *line = new TLine(low-2,0,high+2,0);
	line->SetLineColor(kRed);
	line->Draw("same");
	TLegend* leg2= new TLegend(0.8,0.8,0.98,0.92);
	leg2->AddEntry("residuen","Residuum+Fehler","EP");
	leg2->SetFillColor(0);
	leg2->Draw("same");

//	integral der gausskurve als analytischen ausdruck
	cout << PI << endl;
	double Int = fit_fkt->GetParameter(2)*sqrt(PI)*sqrt(2)*
		fit_fkt->GetParameter(3);
	double* int_point=&Int;	
	to_csv(dat_line,1,int_point,"Pb_exp");

}

int peaks(){

//	double params0[5]={600,-0.005,300,750,20};
//	fit("Pb_data/AC_Pb_0.TKA", params0, 100, 1000, "c0",0);

	double params2[5]={600,-0.005,300,750,20};
	fit("Pb_data/AC_Pb_2.TKA", params2, 100, 1000, "c2",1);

	double params4[5]={600,-0.005,300,750,20};
	fit("Pb_data/AC_Pb_4.TKA", params4, 100, 1000, "c4",2);

	double params6[5]={600,-0.005,300,750,20};
	fit("Pb_data/AC_Pb_6.TKA", params6, 100, 1000, "c6",3);

	double params8[5]={600,-0.005,300,750,20};
	fit("Pb_data/AC_Pb_8.TKA", params8, 100, 1000, "c8",4);

	double params10[5]={600,-0.005,300,750,20};
	fit("Pb_data/AC_Pb_10.TKA", params10, 100, 1000, "c10",5);

	double params12[5]={600,-0.005,300,750,20};
	fit("Pb_data/AC_Pb_12.TKA", params12, 100, 1000, "c12",6);

	double params14[5]={600,-0.005,300,750,20};
	fit("Pb_data/AC_Pb_14.TKA", params14, 100, 1000, "c14",7);

	double params16[5]={600,-0.005,300,750,20};
	fit("Pb_data/AC_Pb_16.TKA", params16, 100, 1000, "c16",8);

	double params18[5]={600,-0.005,300,750,20};
	fit("Pb_data/AC_Pb_18.TKA", params18, 100, 1000, "c18",9);

	double params20[5]={600,-0.005,300,750,20};
	fit("Pb_data/AC_Pb_20.TKA", params20, 100, 1000, "c20",10);
return 0;
}


