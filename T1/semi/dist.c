#include "read_mca.h"
#include "TH1D.h"
#include "tohist.h"
#include "TGraphErrors.h"
#include "TROOT.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TLine.h"
#include "TLegend.h"
#include "TLatex.h"
#include "TPDF.h"
#include <string>
#include <vector>
#include "to_csv.h"

double PI = 4*atan(1);


double fkt1(double* vars, double* params){
    return params[0]*TMath::Gaus(vars[0],params[1],params[2]);
}

double d_fkt1(double* vars, double* params){
    return params[0]*(vars[0]-params[1])/pow(params[2],2)
    *TMath::Gaus(vars[0],params[1],params[2]);
}

double* fit(const char* dat_name, double* params, double low, double high,const char* canvas_name,int dat_line){

    gStyle->SetOptStat(0);
    gStyle->SetOptFit(1);

	int n_p=high-low;
    int n_par = 3;
    int n=read_mca(dat_name)[0]-2;//-2 weil auch zeiten im file stehen
    double t_real = read_mca(dat_name)[2];
    double t_dead = read_mca(dat_name)[1];
    double* data=&read_mca(dat_name)[3];

//  normalize data to dead time
    for(int i=0;i<n;i++) data[i] = data[i]*t_dead/t_real;

    TH1D* hist=new TH1D(canvas_name, "Fit;Channel;Counts",n_p,low,high);
    for(int i=0;i<n;i++){
        for(int j=0;j<data[i];j++){
            hist->Fill(i);
        }
    }
    //stumpfes korrigieren des hinzufuegens zu kleiner bins
    hist->SetBinContent(0,hist->GetBinContent(1));

	//////////hier spaeter rumfitten
    TCanvas* plot=new TCanvas(canvas_name,dat_name,900,600);
    plot->Divide(1,2,0,0);

    plot->cd(1);
    plot->GetPad(1)->SetPad(0.,0.4,1.,1.);

    plot->GetPad(1)->SetBottomMargin(0.012);
    plot->GetPad(1)->SetTopMargin(0.07);
    plot->GetPad(1)->SetRightMargin(0.02);
    plot->GetPad(1)->SetLeftMargin(0.08);

    hist->GetXaxis()->SetLabelSize(0.052);
    hist->GetXaxis()->SetTitleSize(0.052);
    hist->GetYaxis()->SetLabelSize(0.052);
    hist->GetYaxis()->SetTitleSize(0.052);
    hist->GetYaxis()->SetTitleOffset(0.8);


    hist->Draw("E");

    //Funktion fuer fit initialisieren
    TF1* fit_fkt=new TF1("fkt",fkt1,low,high,n_par);
    TF1* fit_fkt_d=new TF1("fkt_d",d_fkt1,low,high,n_par);
    fit_fkt->SetLineColor(kRed);
    for(int i=0; i<n_par; i++) fit_fkt->SetParameter(i,params[i]);
//  fit_fkt->FixParameter(2,300);
    auto fit_res=hist->Fit(fit_fkt,"S","q",low,high);
    for(int i=0;i<n_par;i++) fit_fkt_d->SetParameter(i,fit_fkt->GetParameter(i));

    TLegend* leg1= new TLegend(0.4,0.75,0.6,0.935);
    leg1->AddEntry(hist,"Histogram+Fehler","EL");
    leg1->AddEntry(fit_fkt,"FIT","l");
    leg1->SetFillColor(0);
    leg1->Draw("same");


///////
//  jetzt residuen an den start bringen
    double res[n_p];
    double res_err[n_p];
    double bins[n_p];
    for(int i=0;i<n_p;i++){
        res[i]=hist->GetBinContent(i+1)-fit_fkt->Eval(hist->GetBinCenter(i+1));
        bins[i]=hist->GetBinCenter(i+1);
        res_err[i]=sqrt(hist->GetBinContent(i+int(low)+1)+
            pow(fit_fkt_d->Eval(hist->GetBinCenter(i+int(low)+1))*1/sqrt(12),2));
    }

	TGraphErrors* residuen = new TGraphErrors(n_p,bins,res,nullptr,res_err);

    plot->cd();
    plot->cd(2);
    plot->GetPad(2)->SetPad(0.,0.,1.,0.4);

    residuen->GetXaxis()->SetLabelSize(0.08);
    residuen->GetXaxis()->SetTitle("Channel");
    residuen->GetXaxis()->SetTitleSize(0.08);
    residuen->GetYaxis()->SetTitle("Residuum");
    residuen->GetYaxis()->SetLabelSize(0.08);
    residuen->GetYaxis()->SetTitleSize(0.08);
    residuen->GetXaxis()->SetLimits(low-2,high+2);
    residuen->GetYaxis()->SetTitleOffset(0.4);
    residuen->SetMarkerStyle(8);
    residuen->SetMarkerSize(0.4);

    plot->GetPad(2)->SetFrameFillColor(0);
    plot->GetPad(2)->SetFrameBorderMode(0);

    plot->GetPad(2)->SetBottomMargin(0.17);
    plot->GetPad(2)->SetTopMargin(0.04);
    plot->GetPad(2)->SetRightMargin(0.02);
    plot->GetPad(2)->SetLeftMargin(0.08);

	residuen->Draw("APE");

    TLine *line = new TLine(low-2,0,high+2,0);
    line->SetLineColor(kRed);
    line->Draw("same");
    TLegend* leg2= new TLegend(0.8,0.8,0.98,0.92);
    leg2->AddEntry("residuen","Residuum+Fehler","EP");
    leg2->SetFillColor(0);
    leg2->Draw("same");


//	integral ueber integration ueber bins bestimmen -> 3 sigma

	int down = -low+fit_fkt->GetParameter(1)-3*abs(fit_fkt->GetParameter(2));
	int up = -low+fit_fkt->GetParameter(1)+3*abs(fit_fkt->GetParameter(2));

	double Int = hist->Integral(down,up);

	cout << "Int= " << Int << endl;

	double* toreturn = new double[3];
	toreturn[0] = fit_fkt->GetParameter(1);
	toreturn[1] = Int;
	toreturn[2] = fit_res->GetErrors()[1];
	
	return toreturn;
}

int dist(){
	
	double* res0;
	double params0[3]={500,1600,100};
    res0=fit("data/251mm_1-2.TKA", params0, 0, 2000, "c0",8);
	cout << "res0= " << res0[0] << " und " << res0[1] << endl;	

	double* res1;
	double params1[3]={500,1600,100};
    res1=fit("data/251mm_1-2.TKA", params1, 1200, 2000, "c1",8);
	cout << "res1= " << res1[0] << " und " << res1[1] << endl;	

	double* res2;
	double params2[3]={500,1400,100};
    res2=fit("data/243mm_2-2.TKA", params2, 1000, 1800, "c2",8);
	cout << "res2= " << res2[0] << " und " << res2[1] << endl;	

	double* res3;
	double params3[3]={500,1200,100};
    res3=fit("data/236mm_3-2.TKA", params3, 800, 1600, "c3",8);
	cout << "res3= " << res3[0] << " und " << res3[1] << endl;	

	double* res4;
	double params4[3]={500,80,100};
	res4=fit("data/211mm_4-3.TKA", params4, 110, 910, "c4",8);
	cout << "res4= " << res4[0] << " und " << res4[1] << endl;	

	double* res5;
	double params5[3]={500,80,100};
	res5=fit("data/212mm_4-2.TKA", params5, 110, 910, "c5",8);
	cout << "res5= " << res5[0] << " und " << res5[1] << endl;	

	double* res6;
	double params6[3]={500,80,100};
	res6=fit("data/213mm_4-1.TKA", params6, 110, 910, "c6",8);
	cout << "res6= " << res6[0] << " und " << res6[1] << endl;	

	const int n=4;
	double channels[n];
	double ints[n+2];
	double dists_read[n];
	double dists[n];
	double chan_err[n];
	channels[0]=res1[0];
	ints[0]=res1[1];
	dists_read[0]=25.1;
	chan_err[0]=50*res1[2];

	channels[1]=res2[0];
	ints[1]=res2[1];
	dists_read[1]=24.3;	
	chan_err[1]=20*res2[2];

	channels[2]=res3[0];
	ints[2]=res3[1];
	dists_read[2]=23.6;
	chan_err[2]=20*res3[2];

	channels[3]=res5[0];
	ints[4]=res5[1];
	dists_read[3]=21.2;
	chan_err[3]=res5[2];


	ints[3]=res6[1];
	ints[5]=res4[1];
	
	for(int i=0;i<n;i++) dists[i] = 25.4-dists_read[i];
	
	for(int i=0;i<n;i++){
		to_csv(i+1,1,&channels[i],"chans");
		to_csv(i+1,1,&dists[i],"dists");
		to_csv(i+1,1,&chan_err[i],"chan_errs");
	}

	for(int i=0;i<n+2;i++){
		to_csv(i+1,1,&ints[i],"Int_vals");
	}
return 0;
}






