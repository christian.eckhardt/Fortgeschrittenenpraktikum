#include "TROOT.h"
#include "read_mca.h"
#include "tohist.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TLegend.h"
#include "TLine.h"
#include "TMath.h"

using namespace std;

int stop(){
    gStyle->SetOptStat(0);
    gStyle->SetOptFit(1);

//	int n=read_mca("Int_vals")[0];
	double* data = &read_mca("Int_vals")[1];
	double dat_err[6];
	for(int i=0;i<6;i++) dat_err[i]=sqrt(data[i]);
	double dists[6]={0.3,1.0,1.7,4.0,4.1,4.2};
	for(int i=0;i<6;i++){
		data[i]=data[i]*pow(dists[i],2);
		dat_err[i]=dat_err[i]*pow(dists[i],2);
	}

	TGraphErrors* graph = new TGraphErrors(6,dists,data,nullptr,dat_err);
	
	graph->SetTitle("Measured range");

    TCanvas* plot=new TCanvas("Int","Intensities",900,600);

    plot->SetBottomMargin(0.1);
    plot->SetTopMargin(0.1);
    plot->SetRightMargin(0.01);
    plot->SetLeftMargin(0.08);

    graph->GetYaxis()->SetTitle("Integral Value");
    graph->GetYaxis()->SetLabelSize(0.052);
    graph->GetYaxis()->SetTitleSize(0.052);
    graph->GetYaxis()->SetTitleOffset(0.8);

	graph->GetXaxis()->SetTitle("Distance [cm]");
	graph->GetXaxis()->SetLabelSize(0.052);
    graph->GetXaxis()->SetTitleSize(0.052);
    
	graph->Draw();
	
	return 0;
}
	
