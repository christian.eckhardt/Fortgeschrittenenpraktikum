#include "TROOT.h"
#include "read_mca.h"
#include "tohist.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TLegend.h"
#include "TLine.h"
#include "TMath.h"

using namespace std;
int	eff_dist (){
    gStyle->SetOptStat(0);
    gStyle->SetOptFit(1);

	const int n=4;
	double dists_read[n]={250,243,236,212};
	double dists[n];
	double dists_err[n]={0.05,0.05,0.05,0.05};
	double theo_rho[n]={4.015,5.01,5.734,8.613};//5,3 MeV peak rausgelassen da er im 5,0 peak verschwindet - haette 4,671 cm^2/g als wert
	double theo_rho_err[n]={0.1,0.05,0.05,0.1};
	double air_den = 0.0012041;//wikipediawert 20 grad
	double theo[n];
	double theo_err[n];
	for(int i=0;i<n;i++) {
		theo[i]=theo_rho[i]/air_den*0.001;
		theo_err[i]=theo_rho_err[i]/air_den*0.001;
	}
//	calculate our values from read off
	for(int i=0;i<n;i++) dists[i] = 25.4-0.1*dists_read[i];

	TGraphErrors* graph = new TGraphErrors(4,dists,theo,dists_err,theo_err);
	graph->SetTitle("Measured range");

	TCanvas* plot=new TCanvas("dist","Range in Air",900,600);

	plot->Divide(1,2,0,0);
    plot->cd(1);
    plot->GetPad(1)->SetPad(0,0.4,1.,1.);
    plot->GetPad(1)->SetBottomMargin(0.1);
    plot->GetPad(1)->SetTopMargin(0.07);
    plot->GetPad(1)->SetRightMargin(0.01);
    plot->GetPad(1)->SetLeftMargin(0.08);

	graph->GetYaxis()->SetTitle("theory [cm]");
    graph->GetYaxis()->SetLabelSize(0.052);
    graph->GetYaxis()->SetTitleSize(0.052);
    graph->GetYaxis()->SetTitleOffset(0.8);

	TF1* f=new TF1("lin","x*[0]+[1]",0,6);
	f->SetParameter(0,1);
    f->SetParameter(1,1);

    auto Fitpara=graph->Fit(f,"S","q");
    graph->DrawClone("APE");
    f->Draw("Same");

    TLegend* leg1=new TLegend(0.1,0.7,0.3,0.9);
    leg1->AddEntry("graph","Daten","EP");
    leg1->AddEntry(f,"Fit","L");
    leg1->SetFillColor(0);
    leg1->Draw("same");

    double res[n];
	double res_err[n];	
	

    for (int i=0;i<n;i++){
        res[i]=theo[i]-f->Eval(dists[i]);
		res_err[i]=sqrt(pow(dists_err[i]*f->GetParameter(0),2)+pow(theo_err[i],2));
    }
	plot->cd();
    plot->cd(2);

    plot->GetPad(2)->SetPad(0.,0.,1.,0.4);
    TGraphErrors residuen(n,dists,res,nullptr,theo_err);

    plot->GetPad(2)->SetGridy();
    residuen.GetXaxis()->SetLabelSize(0.08);
    residuen.GetXaxis()->SetTitle("distance measure [cm]");
    residuen.GetXaxis()->SetTitleSize(0.08);
    residuen.GetYaxis()->SetTitle("Residuum");
    residuen.GetYaxis()->SetLabelSize(0.08);
    residuen.GetYaxis()->SetTitleSize(0.08);
    residuen.GetYaxis()->SetTitleOffset(0.55);
    residuen.SetMarkerStyle(8);
    residuen.SetMarkerSize(0.4);

    plot->GetPad(2)->SetFrameFillColor(0);
    plot->GetPad(2)->SetFrameBorderMode(0);
    plot->GetPad(2)->SetBottomMargin(0.17);
    plot->GetPad(2)->SetTopMargin(0.04);
    plot->GetPad(2)->SetRightMargin(0.01);
    plot->GetPad(2)->SetLeftMargin(0.08);


    residuen.DrawClone("APE");
    TLine *line = new TLine(0-1,0,6,0);
    line->SetLineColor(kRed);
    line->Draw("same");
    TLegend* leg2= new TLegend(0.4,0.2,0.6,0.4);
    leg2->AddEntry("residuen","Residuum+Fehler","EP");
    leg2->SetFillColor(0);
    leg2->Draw("same");

    Fitpara->Print();


return 0;
} 
