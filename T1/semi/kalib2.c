#include "TROOT.h"
#include "read_mca.h"
#include "tohist.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TLegend.h"
#include "TLine.h"
#include "TMath.h"

using namespace std;

int kalib2(){
	gStyle->SetOptStat(0);
	gStyle->SetOptFit(1);

	const int n2=4;
	double* peaks = &read_mca("chans")[1];
	double* peaks_err=&read_mca("chan_errs")[1];
	double* dists = &read_mca("dists")[1];
	double dist_err[n2] = {0.1,0.1,0.1,0.1};
	double air_stop = 670;
	double air_den = 0.0012041;
	air_stop=air_stop*air_den;
	double eff_dist = 3.013;//wert aus fitprogramm
//	double dE = air_stop*eff_dist;
	for(int i=0;i<n2;i++) dists[i]=dists[i]+eff_dist;		
	for(int i=0;i<n2;i++){
		cout << "dist= " << dists[i] << endl;
		cout << "dist*rho= "<< dists[i]*air_den << endl;
	}

//	energies corresponding to range taken from nist table	

	double E[n2]= {7.8,6.2,5.5,4.8}; 
	double E_err[n2]={0.1,0.1,0.05,0.1};

	TGraphErrors* graph = new TGraphErrors(4,peaks,E,peaks_err,E_err);
    graph->SetTitle("Measured range");

    TCanvas* plot_fit=new TCanvas("peaks","Energies",900,600);

    plot_fit->Divide(1,2,0,0);
    plot_fit->cd(1);
    plot_fit->GetPad(1)->SetPad(0,0.4,1.,1.);
    plot_fit->GetPad(1)->SetBottomMargin(0.1);
    plot_fit->GetPad(1)->SetTopMargin(0.07);
    plot_fit->GetPad(1)->SetRightMargin(0.01);
    plot_fit->GetPad(1)->SetLeftMargin(0.08);


//    graph->GetYaxis()->SetRangeUser(0,2000);
    graph->GetYaxis()->SetTitle("Channel");
    graph->GetYaxis()->SetLabelSize(0.052);
    graph->GetYaxis()->SetTitleSize(0.052);
    graph->GetYaxis()->SetTitleOffset(0.8);

	TF1* f=new TF1("lin","x*[0]+[1]",0,6);
    f->SetParameter(0,0.01);
    f->SetParameter(1,0);

    auto Fitpara=graph->Fit(f,"S","q");
    graph->DrawClone("APE");
    f->Draw("Same");

    TLegend* leg1=new TLegend(0.1,0.7,0.3,0.9);
    leg1->AddEntry("graph","Daten","EP");
    leg1->AddEntry(f,"Fit","L");
    leg1->SetFillColor(0);
    leg1->Draw("same");

    double res[n2];
	double res_err[n2];	

    for (int i=0;i<n2;i++){
        res[i]=E[i]-f->Eval(peaks[i]);
		res_err[i]=sqrt(pow(f->GetParameter(0)*peaks_err[i],2)+pow(E_err[i],2));
    }
    plot_fit->cd();
    plot_fit->cd(2);

    plot_fit->GetPad(2)->SetPad(0.,0.,1.,0.4);
    TGraphErrors residuen(n2,peaks,res,nullptr,res_err);
	
	plot_fit->GetPad(2)->SetFrameFillColor(0);
    plot_fit->GetPad(2)->SetFrameBorderMode(0);
    plot_fit->GetPad(2)->SetBottomMargin(0.17);
    plot_fit->GetPad(2)->SetTopMargin(0.04);
    plot_fit->GetPad(2)->SetRightMargin(0.01);
    plot_fit->GetPad(2)->SetLeftMargin(0.08);


    residuen.DrawClone("APE");
    TLine *line5 = new TLine(0,0,1740,0);
	line5->SetLineColor(kRed);
	line5->Draw("same");
    line5->SetLineColor(kRed);
    line5->Draw("same");
    TLegend* leg2= new TLegend(0.4,0.2,0.6,0.4);
    leg2->AddEntry("residuen","Residuum+Fehler","EP");
    leg2->SetFillColor(0);
    leg2->Draw("same");

    Fitpara->Print();

	

return 0;
}
