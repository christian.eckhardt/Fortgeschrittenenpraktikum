#include "TROOT.h"
#include "read_mca.h"
#include "tohist.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TLegend.h"
#include "TLine.h"
#include "TMath.h"

using namespace std;

int kalib1(){
	gStyle->SetOptStat(0);
	gStyle->SetOptFit(1);

	int n=read_mca("data/253mm_komplettbild.TKA")[0];
    double* dat_show=&read_mca("data/253mm_komplettbild.TKA")[1];
	
	TH1D* hist1 = new TH1D("show", "spectrum at closest distance;Channel;Counts",n,1,n);

	TCanvas* plot=new TCanvas("show","Spectrum clostest distance",900,600);
	
	for(int i=1;i<n+1;i++){
        for(int j=0; j<dat_show[i-1];j++) hist1->Fill(i);
	}

	plot->SetBottomMargin(0.12);
	plot->SetTopMargin(0.07);
	plot->SetRightMargin(0.02);
	plot->SetLeftMargin(0.1);

	hist1->SetLabelSize(0.052);
	hist1->SetTitleSize(0.052);
	hist1->SetLabelSize(0.052);
	hist1->SetTitleSize(0.052);
	hist1->SetTitleOffset(1);

    hist1->Draw("E");

	TLine *line1 = new TLine(350,0,350,1140);
	line1->SetLineColor(kRed);
	line1->Draw("same");

	TLine *line2 = new TLine(660,0,660,1140);
	line2->SetLineColor(kRed);
	line2->Draw("same");

	TLine *line3 = new TLine(940,0,940,1140);
	line3->SetLineColor(kRed);
	line3->Draw("same");
	
	TLine *line4 = new TLine(1660,0,1660,1140);
	line4->SetLineColor(kRed);
	line4->Draw("same");

	const int n2=4;
	double peaks[n2] = {350.,660.,940.,1665.};
	double peaks_err[n2]={20,20,30,20};
	double theo[n2] = {4.78,5.49,6.0,7.69};
	double air_stop = 670;
	double air_den = 0.0012041;
	air_stop=air_stop*air_den;
	double eff_dist = 3.013;//wert aus fitprogramm
	double dE = air_stop*eff_dist;
	cout << "dE= " << dE << endl; 
	for(int i=0;i<n2;i++) theo[i]=theo[i]-dE;		

	TGraphErrors* graph = new TGraphErrors(4,theo,peaks,nullptr,peaks_err);
    graph->SetTitle("Measured range");

    TCanvas* plot_fit=new TCanvas("peaks","Energies",900,600);

    plot_fit->Divide(1,2,0,0);
    plot_fit->cd(1);
    plot_fit->GetPad(1)->SetPad(0,0.4,1.,1.);
    plot_fit->GetPad(1)->SetBottomMargin(0.012);
    plot_fit->GetPad(1)->SetTopMargin(0.07);
    plot_fit->GetPad(1)->SetRightMargin(0.01);
    plot_fit->GetPad(1)->SetLeftMargin(0.08);

    graph->GetYaxis()->SetTitle("Channel");
    graph->GetYaxis()->SetLabelSize(0.052);
    graph->GetYaxis()->SetTitleSize(0.052);
    graph->GetYaxis()->SetTitleOffset(0.8);



	TF1* f=new TF1("lin","x*[0]+[1]",0,6);
    f->SetParameter(0,1);
    f->SetParameter(1,1);

    auto Fitpara=graph->Fit(f,"S","q");
    graph->DrawClone("APE");
    f->Draw("Same");

    TLegend* leg1=new TLegend(0.1,0.7,0.3,0.9);
    leg1->AddEntry("graph","Daten","EP");
    leg1->AddEntry(f,"Fit","L");
    leg1->SetFillColor(0);
    leg1->Draw("same");

    double res[n2];

    for (int i=0;i<n2;i++){
        res[i]=peaks[i]-f->Eval(theo[i]);
    }
    TGraphErrors* residuen=new TGraphErrors(n2,theo,res,nullptr,peaks_err);

    plot_fit->cd();
    plot_fit->cd(2);

    plot_fit->GetPad(2)->SetPad(0.,0.,1.,0.4);
	
	plot_fit->GetPad(2)->SetFrameFillColor(0);
    plot_fit->GetPad(2)->SetFrameBorderMode(0);
    plot_fit->GetPad(2)->SetBottomMargin(0.17);
    plot_fit->GetPad(2)->SetTopMargin(0.04);
    plot_fit->GetPad(2)->SetRightMargin(0.01);
    plot_fit->GetPad(2)->SetLeftMargin(0.08);

    residuen->GetXaxis()->SetLabelSize(0.08);
    residuen->GetXaxis()->SetTitle("Energie Lit [MeV]");
    residuen->GetXaxis()->SetTitleSize(0.08);
    residuen->GetYaxis()->SetTitle("Residuum");
    residuen->GetYaxis()->SetLabelSize(0.08);
    residuen->GetYaxis()->SetTitleSize(0.08);
//	residuen->GetXaxis()->SetLimits(low,high);
    residuen->GetYaxis()->SetTitleOffset(0.4);




    residuen->Draw("APE");
    TLine *line5 = new TLine(theo[0]-0.2,0,theo[n2-1]+0.2,0);
	line5->SetLineColor(kRed);
	line5->Draw("same");
    line5->SetLineColor(kRed);
    line5->Draw("same");
    TLegend* leg2= new TLegend(0.4,0.2,0.6,0.4);
    leg2->AddEntry("residuen","Residuum+Fehler","EP");
    leg2->SetFillColor(0);
    leg2->Draw("same");

    Fitpara->Print();

return 0;
}
