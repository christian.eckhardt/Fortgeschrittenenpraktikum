#include <iostream>
#include "read_mca.h"
#include <cmath>

using namespace std;

//n = Anzahl Werte
double* RMS(int n, double* vals){
	double mean=0;
	double var=0;
	for(int i=0;i<n;i++) mean+=vals[i];
	mean=mean/(n+0.0);
	for(int i=0;i<n;i++)var+=pow((vals[i]-mean),2);
	var=var/(n-1.0);
	double* toret = new double[2];
	toret[0]=mean;
	toret[1]=sqrt(var);
	return toret;
}
/*
double RMS(int n, double* vals, double mean){
	double var=0;
	double sig=0;
	for(int i=0;i<n;i++) var+=pow((vals[i]-mean),2);
	var=var/(n+0.0);
	sig=pow(var,0.5);
	return sig;
}

//Funktion berechnet aus ergebnissen des T9 programms den sysfehler auf sigma
double sig_sys(int n,double* ns_o, double* ns_M, double n_o){
	double L=134.4;//Luminositaet bei 91,33 GeV
	double mean=1/L*n_o/ns_M[0]*ns_o[0];//Mittelwert - originalwerte stehen im 0. Element
	cout << "sig mean = " << mean << endl;
	double* sigs=new double[n-1];
	for(int i=1; i<n; i++){
		sigs[i-1]=1/L*n_o/ns_M[i]*ns_o[i];//sigmas ausrechnen und dann mit RMS sigma bestimmen
		cout << sigs[i] << endl;
	}
	double sys=0;
	sys=RMS(n-1,sigs,mean);
	cout<<"sys= "<<sys<<endl;
	return sys;
}
*/
int main(){
	

	double nat[5]={0.85,0.68,0.82,0.86,1.09};
	double fliese[5]={4.32,4.36,4.31,4.31,4.14};
	double tuch[5]={4.21,4.28,4.37,4.30,4.31};
	double abschirm[5]={1.2,1.36,1.39,1.48,1.45};

	double zange[5]={80.1,75.3,76.5,75.9,67.2};
	double tresor[5]={83.4,87.7,85.2,88.5,85.1};
	
	double* res_nat=RMS(5,nat);
	double* res_fliese=RMS(5,fliese);
	double* res_tuch=RMS(5,tuch);
	double* res_abschirm=RMS(5,abschirm);

	double* res_zange=RMS(5,zange);
	double* res_tresor=RMS(5,tresor);

	cout << "nat: " <<res_nat[0] << " +/- " <<res_nat[1] <<endl;
	cout << "fliese: " <<res_fliese[0] << " +/- " <<res_fliese[1] <<endl;
	cout << "tuch: " <<res_tuch[0] << " +/- " <<res_tuch[1] <<endl;
	cout << "abschirm: " <<res_abschirm[0] << " +/- " <<res_abschirm[1] <<endl;
	cout << "zange: " <<res_zange[0] << " +/- " <<res_zange[1] <<endl;
	cout << "tresor: " <<res_tresor[0] << " +/- " <<res_tresor[1] <<endl;


	return 0;
}

