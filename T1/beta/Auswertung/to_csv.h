#ifndef _READ_ASCII_H_INCLUDED
#define _READ_ASCII_H_INCLUDED

#include <vector>
#include <fstream>
#include <iostream>

using namespace std;

//function to write input data in corresponding line of csv file seperated by commas 

void to_csv(int line,int length, double* input, const char* name){

    vector<string> old_data;
	vector<string>::iterator it;
    ifstream csv_in;
    csv_in.open(name);
    string content;
    while(getline(csv_in,content)){
//      cout<<content<<endl;
        old_data.push_back(content);
    }
    csv_in.close();
	it=old_data.begin();

	ofstream csv_out;
	csv_out.open(name);
	int current_line = 0;
	while(line!=current_line){
		if(it<old_data.end()) csv_out<<*it<<"\n";
		else{csv_out<<"\n";}
		it++;
		current_line++;
	}
	it++;
	for(int i=0;i<length;i++){
		csv_out<<input[i];
		if(i<length-1) csv_out<<",";
	}
	csv_out<<"\n";;
	for(;it<old_data.end();it++){
		csv_out<<*it<<"\n";
	}
	csv_out.close();	

}       
#endif
