#include "TROOT.h"
#include "read_mca.h"
#include "tohist.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TLegend.h"
#include "TLine.h"
#include "TMath.h"

using namespace std;

int beta(){
	

	gStyle->SetOptStat(0);
	gStyle->SetOptFit(1);
	int n = read_mca("../beta")[0];
	n=n-6;
	double* counts = &read_mca("../beta")[7];
	double* belag = &read_mca("../beta_masbel")[7];
//	wurzel n fehler
	double* err = new double[n];
	double* lin = new double[n];
	double* lin_err = new double[n];
	for(int i=0;i<n;i++){
	err[i] = sqrt(counts[i]);
	lin[i] = log(counts[i]);
	lin_err[i]=err[i]/counts[i];
//	cout << "lin dat= "<< lin[i] << endl;
	} 
//	Daten linearisieren
	
//	TGraphErrors* graph = new TGraphErrors(n,belag,lin,nullptr,lin_err);
	TGraphErrors* graph = new TGraphErrors(n,belag,counts,nullptr,err);
	graph->SetTitle("Beta Absorption;Distance x desity; Counts");

    TCanvas* plot=new TCanvas("Beta Absorption","Beta Absorption",900,600);
    plot->Divide(1,2,0,0);
    plot->cd(1);
    plot->GetPad(1)->SetPad(0,0.4,1.,1.);
    plot->GetPad(1)->SetBottomMargin(0.005);//0.012
    plot->GetPad(1)->SetTopMargin(0.07);
    plot->GetPad(1)->SetRightMargin(0.01);
    plot->GetPad(1)->SetLeftMargin(0.08);

    graph->GetYaxis()->SetLabelSize(0.052);
    graph->GetYaxis()->SetTitleSize(0.052);
    graph->GetYaxis()->SetTitleOffset(0.8);

//TF1* f=new TF1("Exponential","[0]*exp(-x*[1])+[3]*exp(-x*[2])",0.,933.0);
//	f->SetParameter(0,1.65727e+05);
//	f->SetParameter(1,4.5e-03);
//	f->SetParameter(2,0.0035);
//	f->SetParameter(3,1.65727e+05);
   
	TF1* f=new TF1("Exponential","[0]*exp(-x*[1])",0., belag[n-1]);
	f->SetParameter(0,310000);
	f->SetParameter(1,0.0049);
//	f->FixParameter(2,50);
	
	auto Fitpara=graph->Fit(f,"S","q");
    graph->DrawClone();
	f->Draw("Same");

    TLegend* leg1=new TLegend(0.1,0.7,0.3,0.9);
    leg1->AddEntry("graph","Daten","EP");
    leg1->AddEntry(f,"Fit","L");
    leg1->SetFillColor(0);
    leg1->Draw("same");

	double res[n];
    double res_lin_err[n];

    for (int i=0;i<n;i++){
//		cout<<channel[i]<<endl;
        res[i]=counts[i]-f->Eval(belag[i]);
    }

    plot->cd();
    plot->cd(2);

    plot->GetPad(2)->SetPad(0.,0.,1.,0.4);
    TGraphErrors residuen(n,belag,res,nullptr,lin_err);

	plot->GetPad(2)->SetGridy();
    residuen.GetXaxis()->SetLabelSize(0.08);
    residuen.GetXaxis()->SetTitle("Distance x desity [mg/cm^2]");
    residuen.GetXaxis()->SetTitleSize(0.08);
    residuen.GetYaxis()->SetTitle("Residuum");
    residuen.GetYaxis()->SetLabelSize(0.08);
    residuen.GetYaxis()->SetTitleSize(0.08);
    residuen.GetYaxis()->SetTitleOffset(0.55);
    residuen.SetMarkerStyle(8);
    residuen.SetMarkerSize(0.4);

	plot->GetPad(2)->SetFrameFillColor(0);
    plot->GetPad(2)->SetFrameBorderMode(0);
    plot->GetPad(2)->SetBottomMargin(0.17);
    plot->GetPad(2)->SetTopMargin(0.04);
    plot->GetPad(2)->SetRightMargin(0.01);
    plot->GetPad(2)->SetLeftMargin(0.08);


    residuen.DrawClone("APE");
    TLine *line = new TLine(belag[0]-1,0,belag[n-1]+1,0);
    line->SetLineColor(kRed);
    line->Draw("same");
    TLegend* leg2= new TLegend(0.4,0.2,0.6,0.4);
    leg2->AddEntry("residuen","Residuum+Fehler","EP");
    leg2->SetFillColor(0);
    leg2->Draw("same");

//	Fitpara->Print();


	TGraphErrors* graph_lin = new TGraphErrors(n,belag,lin,nullptr,lin_err);
	graph_lin->SetTitle("Linearised Plot;Density x Distance; Counts");

    TCanvas* plot1=new TCanvas("Lin","Linearisation",900,600);
//	plot1->Divide(1,2,0,0);
//	plot1->cd(1);
//	plot1->SetPad(0,0.4,1.,1.);
//	plot1->SetBottomMargin(0.012);
//	plot1->SetTopMargin(0.07);
//	plot1->SetRightMargin(0.01);
//	plot1->SetLeftMargin(0.08);

	graph_lin->GetYaxis()->SetLabelSize(0.052);
	graph_lin->GetYaxis()->SetTitleSize(0.052);
 	graph_lin->GetYaxis()->SetTitleOffset(0.8);
 	graph_lin->GetXaxis()->SetLimits(0,3000);
 	graph_lin->GetYaxis()->SetLimits(-1,13);
 	graph_lin->GetYaxis()->SetRangeUser(-1,13);

	plot1->cd();
	TF1* f_lin=new TF1("linear","[0]-x*[1]",0.,3000);
	f_lin->SetParameter(0,log(f->GetParameters()[0]));
	f_lin->SetParameter(1,f->GetParameters()[1]);
    graph_lin->Draw("APE");
	f_lin->Draw("same");

	TLine *line1 = new TLine(belag[0]-1,0,3000,0);
	line1->SetLineColor(kRed);
	line1->Draw("same");


return 0;
}
