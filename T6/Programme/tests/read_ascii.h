#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

double* read_ascii(char const* name){

	ifstream infile;
	infile.open(name);
	string temp;
	vector<double> array;
	while (infile >> temp){
		array.push_back(stod(temp));	
	}
	cout << array[0] << endl;
	double* toreturn = new double;
	copy(array.begin(), array.end(), toreturn);
	return toreturn;
}
