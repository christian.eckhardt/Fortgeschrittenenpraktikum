#include "read_mca.h"
#include "TH1D.h"
#include "tohist.h"
#include "TGraphErrors.h"
#include "TROOT.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TLine.h"
#include "TLegend.h"
#include "TLatex.h"
#include <string>
#include <vector>

double max(double* vars, double* params){
	return params[0]*vars[0]*params[1]*TMath::Gaus(vars[0],params[2],params[3]);
	}



double error(double a ,double sigma_a,double b, double sigma_b,double energy){
	double	sys=sqrt(pow(sigma_a*(energy-b)/a,2)+sigma_b*sigma_b);
	return sys;
	}

double gauss(double* vars, double* params){
	if(params[0]==1){
		return 
		params[1]*TMath::Gaus(vars[0],params[2],params[3]);
	}
	else if(params[0]==2){	
		return
	 	params[1]*TMath::Gaus(vars[0],params[2],params[3])+
		params[4]*TMath::Gaus(vars[0],params[5],params[6]);
	}
	else if(params[0]==3){
		return
		params[1]*TMath::Gaus(vars[0],params[2],params[3])+
		params[4]*TMath::Gaus(vars[0],params[5],params[6])+
		params[7]*TMath::Gaus(vars[0],params[8],params[9]);
	}
	else {
		return
		params[1]*TMath::Gaus(vars[0],params[2],params[3])+
		params[4]*TMath::Gaus(vars[0],params[5],params[6])+
		params[7]*TMath::Gaus(vars[0],params[8],params[9])+
		params[10]*TMath::Gaus(vars[0],params[11],params[12]);
	}
}

double d_gauss(double* vars, double* params){
	if(params[0]==1){
		return 
		(vars[0]-params[2])/pow(params[3],2)*params[1]*TMath::Gaus(vars[0],params[2],params[3]);
	}
	else if(params[0]==2){	
		return
	 	(vars[0]-params[2])/pow(params[3],2)*params[1]*TMath::Gaus(vars[0],params[2],params[3])+
		(vars[0]-params[5])/pow(params[6],2)*params[4]*TMath::Gaus(vars[0],params[5],params[6]);
	}
	else if(params[0]==3){
		return
		(vars[0]-params[2])/pow(params[3],2)*params[1]*TMath::Gaus(vars[0],params[2],params[3])+
		(vars[0]-params[5])/pow(params[6],2)*params[4]*TMath::Gaus(vars[0],params[5],params[6])+
		(vars[0]-params[8])/pow(params[9],2)*params[7]*TMath::Gaus(vars[0],params[8],params[9]);
	}
	else {
		return
		(vars[0]-params[2])/pow(params[3],2)*params[1]*TMath::Gaus(vars[0],params[2],params[3])+
		(vars[0]-params[5])/pow(params[6],2)*params[4]*TMath::Gaus(vars[0],params[5],params[6])+
		(vars[0]-params[8])/pow(params[9],2)*params[7]*TMath::Gaus(vars[0],params[8],params[9])+
		(vars[0]-params[11])/pow(params[12],2)*params[10]*TMath::Gaus(vars[0],params[11],params[12]);
	}
}




void fit(const char* dat_name,double* params,double low, double high,int num,const char* canvas_name,const char* canvas_name_2){

	std::string prefix="../Protokoll/Plots/";
        std::string suffix=".pdf";

	gStyle->SetOptStat(0);
	gStyle->SetOptFit(1);

	double* mc = read_mca(dat_name);
	double* mc_hist=tohist(mc);
	double* transparams=read_mca("../Daten/kalib_params");
	

	TF1* gausse=new TF1("gausse", gauss, low, high,3*num+1);

	gausse->SetLineColor(kRed);
	for(int i=1;i<3*num+1;i++){
		gausse->SetParameter(i,params[i]);
	}
	gausse->FixParameter(0,num);
//	cout<<num<<endl;
//	cout<<gauss->GetParameter(3)<<endl;
	double p0=transparams[1];
	double err_p0=transparams[2];
	double p1=transparams[3];
	double err_p1=transparams[4];	

	double lowerbound=p1+p0;

        double upperbound=(mc[0])*p1+p0;

        for(int i=1;i<=mc_hist[0];i++){
                mc_hist[i]=p0+p1*mc_hist[i];
        }

	TH1D* hist_me=new TH1D(canvas_name,"E;Energie [keV]; Events",4096,lowerbound,upperbound);
	TH1D* hist_me2=new TH1D(canvas_name_2,"E;Energie [keV]; Events",4096,lowerbound,upperbound);


        for (int i=1;i<=mc_hist[0];i++){
                hist_me->Fill(mc_hist[i]);
		hist_me2->Fill(mc_hist[i]);
        }

	hist_me->GetXaxis()->SetRangeUser(low-2*p1,high+2*p1);
        TCanvas* me_plot=new TCanvas(canvas_name,dat_name,900,600);

        me_plot->Divide(1,2,0,0);

        me_plot->cd(1);
        me_plot->GetPad(1)->SetPad(0.,0.4,1.,1.);

        me_plot->GetPad(1)->SetBottomMargin(0.012);
        me_plot->GetPad(1)->SetTopMargin(0.07);
        me_plot->GetPad(1)->SetRightMargin(0.01);
	me_plot->GetPad(1)->SetLeftMargin(0.08);

        hist_me->GetXaxis()->SetLabelSize(0.052);
        hist_me->GetXaxis()->SetTitleSize(0.052);
        hist_me->GetYaxis()->SetLabelSize(0.052);
        hist_me->GetYaxis()->SetTitleSize(0.052);
        hist_me->GetYaxis()->SetTitleOffset(0.8);

        hist_me->Draw("E");
	hist_me2->Draw("same");
	gausse->Draw("same");
	TLegend* leg1= new TLegend(0.4,0.75,0.6,0.935);
	leg1->AddEntry(hist_me2,"Histogram+Fehler","EL");
	leg1->AddEntry("gausse","Fit","l");
	leg1->SetFillColor(0);
	leg1->Draw("same");
	

	int n_points=int((high-low)/p1+1.);
//	int n_points=64/p1;
        double x_err=p1/sqrt(12.);
	auto fitres= hist_me->Fit(gausse,"S","q",low,high);
	TF1* d_gausse =new TF1("d_gausse",d_gauss,low,high,3*num+1);
	
	for(int i=1;i<3*num+1;i++){
		d_gausse->SetParameter(i,gausse->GetParameter(i));
	}
	d_gausse->FixParameter(0,num);
	double res[n_points];
    double bins[n_points];
	double res_err[n_points];
	
        for (int i =0;i<n_points;i++) {
                res[i] = hist_me->GetBinContent(i+int((low-p0)/p1))- gausse->Eval(hist_me->GetBinCenter(i+int((low-p0)/p1)));
                bins[i]=hist_me->GetBinCenter(i+int((low-p0)/p1));
                res_err[i]=sqrt(hist_me->GetBinContent(i+int((low-p0)/p1))+pow(d_gausse->Eval(hist_me->GetBinCenter(i+int((low-p0)/p1)))*x_err,2));
        }

        me_plot->cd();
        me_plot->cd(2);

        me_plot->GetPad(2)->SetPad(0.,0.,1.,0.4);
        TGraphErrors residuen(n_points,bins,res,nullptr,res_err);

	me_plot->GetPad(2)->SetGridy();
	residuen.GetXaxis()->SetLabelSize(0.08);
	residuen.GetXaxis()->SetTitle("Energie [keV]");
	residuen.GetXaxis()->SetTitleSize(0.08);
	residuen.GetYaxis()->SetTitle("Residuum");
	residuen.GetYaxis()->SetLabelSize(0.08);
	residuen.GetYaxis()->SetTitleSize(0.08);
	residuen.GetXaxis()->SetLimits(low-2*p1,high+2*p1);
	residuen.GetYaxis()->SetTitleOffset(0.4);
	residuen.SetMarkerStyle(8);
	residuen.SetMarkerSize(0.4);	

	me_plot->GetPad(2)->SetFrameFillColor(0);
	me_plot->GetPad(2)->SetFrameBorderMode(0);

	me_plot->GetPad(2)->SetBottomMargin(0.17);
	me_plot->GetPad(2)->SetTopMargin(0.04);
	me_plot->GetPad(2)->SetRightMargin(0.01);
	me_plot->GetPad(2)->SetLeftMargin(0.08);
	
	
	residuen.DrawClone("APE");
	TLine *line = new TLine(low-2*p1,0,high+2*p1,0);
	line->SetLineColor(kRed);
	line->Draw("same");
	TLegend* leg2= new TLegend(0.8,0.8,0.98,0.92);
	leg2->AddEntry("residuen","Residuum+Fehler","EP");
	leg2->SetFillColor(0);
        leg2->Draw("same");

        std::string c=canvas_name;
        std::string file=prefix + c +suffix;
        const char* filename=file.c_str();
        me_plot->SaveAs(filename,"pdf");




//      write result to file

        stringstream s;
        s <<"../Daten/"<< "Fit_"<<dat_name[15]<<dat_name[16]<<"_Dirty";
        string out_name=s.str();
    ofstream fitp;
    fitp.open(out_name);
    fitp<<"<<DATA>>\n";

        switch(num){
        case 4 :{fitp << fitres->GetParams()[11]<<"\n"<<gausse->GetParError(11)<<"\n"<<fitres->GetParams()[12]<<"\n"<<gausse->GetParError(12)<<"\n";
		cout<<fitres->GetParams()[11]<<"+/-"<<error(p1,err_p1,p0,err_p0,fitres->GetParams()[11]) <<endl;}
        case 3 :{fitp << fitres->GetParams()[8]<<"\n"<<gausse->GetParError(8)<<"\n"<<fitres->GetParams()[9]<<"\n"<<gausse->GetParError(9)<<"\n";
	            cout<<fitres->GetParams()[8]<<"+/-"<<error(p1,err_p1,p0,err_p0,fitres->GetParams()[8]) <<endl;}
        case 2 :{fitp << fitres->GetParams()[5]<<"\n"<<gausse->GetParError(5)<<"\n"<<fitres->GetParams()[6]<<"\n"<<gausse->GetParError(6)<<"\n";
		            cout<<fitres->GetParams()[5]<<"+/-"<<error(p1,err_p1,p0,err_p0,fitres->GetParams()[5]) <<endl;}
        case 1 :{fitp << fitres->GetParams()[2]<<"\n"<<gausse->GetParError(2)<<"\n"<<fitres->GetParams()[3]<<"\n"<<gausse->GetParError(3)<<"\n";
		  cout<<fitres->GetParams()[2]<<"+/-"<<error(p1,err_p1,p0,err_p0,fitres->GetParams()[2]) <<endl;}
        }
        fitp<<"<<END>>";
	fitp.close();
	
	

}


int fit_unknown(){
//	double params_1[10]={3,600,8.05,0.1,150,8.8,0.1,100,8.95,0.1};
//	fit ("../Daten/Messung1_clean",params_1,7.8,9.1,3,"Messung1_clean","Messung1_clean_2");
//	double params_2[10]={3,600,8.05,0.1,150,8.8,0.1,100,8.95,0.1};
//	fit ("../Daten/Messung2_clean",params_2,7.8,9.1,3,"Messung2_clean","Messung2_clean_2");
//
//	double params_3_a[10]={3,160,6.4,0.1,60,7.1,0.1,40,6.8,3};
//	fit ("../Daten/Leermessung.mca",params_3_a,6.2,7.3,params_3_a[0],"Leermessung_a","Leermessung_2");
//
//	double params_3_b[13]={4,50,25.2,0.1,10,26.4,0.1,20,27.4,0.1,10,28.4,0.1};
//	fit ("../Daten/Leermessung.mca",params_3_b,24.7,28.8,params_3_b[0],"Leermessung_b","Leermessung_2");
//
//	double params_3_c[4]={1,840,59.5,0.3};
//	fit ("../Daten/Leermessung.mca",params_3_c,59,59.85,params_3_c[0],"Leermessung_c","Leermessung_2");

//	double params_4[4]={1,15,51,1};
//	fit ("../Daten/Messung4_Stein_clean",params_4,49,53,params_4[0],"Messung4_clean","Messung4_clean_2");

//	double params_5[10]={3,207,6.47,0.07,50,7.1,0.1,50,7.2,0.1};
//	fit ("../Daten/Messung5_Kronkorken_clean",params_5,6.25,7.36,3,"Messung5_clean","Messung5_clean_2");
//
//	double params_6[7]={2,70,6.4,0.1,30,5.5,0.1};
//	fit ("../Daten/Messung6_Batterie_clean",params_6,5.3,6.6,params_6[0],"Messung6_clean","Messung6_clean_2");
//
//	double params_7[7]={2,2500,8,0.2,500,8.8,0.2};
//	fit ("../Daten/Messung7_klotz_clean",params_7,1,60,2,"Messung7_clean","Messung7_clean_2");

	
//	double params_8_a[10]={3,500,8.05,0.05,100,8.65,0.05,80,8.95,0.1};
//	fit ("../Daten/Messung8_muenzej_clean",params_8_a,7.9,9.1,params_8_a[0],"Messung8_clean_a","Messung8_clean_a_2");
//	double params_8_b[4]={1,92.94,7.502,0.0869};
//	double params_8_b[7]={2,60,7.45,0.07,60,7.55,0.07};
//	fit ("../Daten/Messung8_muenzej_clean",params_8_b,7.35,7.6,params_8_b[0],"Messung8_clean_b","Messung8_clean_b_2");
//
//	double params_9_a[13]={4,50,8.7,0.2,320,10.6,0.2,50,12.3,0.1,350,12.6,0.1};
//	fit ("../Daten/Messung9_Metall_clean",params_9_a,8.5,12.8,params_9_a[0],"Messung9_clean_a","Messung9_clean_a_2");
//
//	double params_9_b[7]={2,15,31.7,0.1,25,32.4,0.1};
//	fit ("../Daten/Messung9_Metall_clean",params_9_b,31.5,32.4,params_9_b[0],"Messung9_clean_b","Messung9_clean_b_2");
//
//	double params_10_a1[10]={3,333,8.05,0.1,100,8.9,0.1,20,9,0.5};
//	fit ("../Daten/Messung10_usb_clean",params_10_a1,7.85,9.25,params_10_a1[0],"Messung10_clean_a1","Messung10_clean_a1_2");
//
//	double params_10_a2[13]={4,150,11.9,0.1,10,12.7,0.1,20,13,0.05,40,13.25,0.05};
//	fit ("../Daten/Messung10_usb_clean",params_10_a2,11.7,13.4,params_10_a2[0],"Messung10_clean_a2","Messung10_clean_a2_2");
//
//	double params_10_b[13]={4,100,25.2,0.2,30,28.4,0.1,60,31.8,0.1,90,32.2,0.1};
//	fit ("../Daten/Messung10_usb_clean",params_10_b,24.8,32.4,params_10_b[0],"Messung10_clean_b","Messung10_clean_2_b");
//
//	double params_10_c[4]={1,160,59.6,0.2};
//	fit ("../Daten/Messung10_usb_clean",params_10_c,59.3,59.8,params_10_c[0],"Messung10_clean_c","Messung10_clean_2_c");
//
//	double params_11[7]={2,2500,8,0.2,500,8.8,0.2};
//	fit ("../Daten/Messung11_tabak_clean",params_11,1,60,2,"Messung11_clean","Messung11_clean_2");


//	double params_Cu[7]={2,2500,8.04,0.1,500,8.9,0.1};
//	fit ("../Daten/Kalib_Cu.mca",params_Cu,7.8,9.2,int(params_Cu[0]),"Kalib_Cu","Kalib_Cu_2");
//
//	double params_Rb[7]={2,11000,13.37,0.2,2000,14.96,0.1};
//	fit ("../Daten/Kalib_Rb.mca",params_Rb,13,15.4,int(params_Rb[0]),"Kalib_Rb","Kalib_Rb_2");
//     
//
//	double params_Ag[10]={3,12000,22.15,0.2,2000,24.93,0.1,500,25.46,0.1};
//	fit ("../Daten/Kalib_Ag.mca",params_Ag,21.5,25.7,int(params_Ag[0]),"Kalib_Ag","Kalib_Ag_2");
//
//	double params_Mo[10]={3,15000,17.44,0.2,2000,19.6,0.1,500,19.965,0.1};
//	fit ("../Daten/Kalib_Mo.mca",params_Mo,16.9,20.2,int(params_Mo[0]),"Kalib_Mo","Kalib_Mo_2");
//
//	double params_Ba[13]={4,2500,31.82,0.1,4000,32.2,0.1,1000,36.34,0.1,400,37.26,0.1};
//	fit ("../Daten/Kalib_Ba.mca",params_Ba,31.2,37.8,int(params_Ba[0]),"Kalib_Ba","Kalib_Ba_2");
//
//	double params_Tb[13]={4,1200,43.745,0.1,2000,44.482,0.1,500,50.31,0.1,150,51.698,0.1};
//	fit ("../Daten/Kalib_Tb.mca",params_Tb,43.1,52.2,int(params_Tb[0]),"Kalib_Tb","Kalib_Tb_2");



	return 0;
}

