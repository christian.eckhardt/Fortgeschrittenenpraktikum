#include "read_mca.h"
#include "TH1D.h"
#include "tohist.h"
#include "TGraphErrors.h"
#include "TROOT.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TLine.h"
#include "TLegend.h"
#include "TLatex.h"
#include "TPDF.h"
#include <string>
#include <vector>

double gauss(double* vars, double* params){
        if(params[0]==1){
                return
                params[1]*TMath::Gaus(vars[0],params[2],params[3]);
        }
        else if(params[0]==2){
                return
                params[1]*TMath::Gaus(vars[0],params[2],params[3])+
                params[4]*TMath::Gaus(vars[0],params[5],params[6]);
        }
        else if(params[0]==3){
                return
                params[1]*TMath::Gaus(vars[0],params[2],params[3])+
                params[4]*TMath::Gaus(vars[0],params[5],params[6])+
                params[7]*TMath::Gaus(vars[0],params[8],params[9]);
        }
        else {
                return
                params[1]*TMath::Gaus(vars[0],params[2],params[3])+
                params[4]*TMath::Gaus(vars[0],params[5],params[6])+
                params[7]*TMath::Gaus(vars[0],params[8],params[9])+
                params[10]*TMath::Gaus(vars[0],params[11],params[12]);
        }
}

double d_gauss(double* vars, double* params){
        if(params[0]==1){
                return
                (vars[0]-params[2])/pow(params[3],2)*params[1]*TMath::Gaus(vars[0],params[2],params[3]);
        }
        else if(params[0]==2){
                return
                (vars[0]-params[2])/pow(params[3],2)*params[1]*TMath::Gaus(vars[0],params[2],params[3])+
                (vars[0]-params[5])/pow(params[6],2)*params[4]*TMath::Gaus(vars[0],params[5],params[6]);
        }
        else if(params[0]==3){
                return
                (vars[0]-params[2])/pow(params[3],2)*params[1]*TMath::Gaus(vars[0],params[2],params[3])+
                (vars[0]-params[5])/pow(params[6],2)*params[4]*TMath::Gaus(vars[0],params[5],params[6])+
                (vars[0]-params[8])/pow(params[9],2)*params[7]*TMath::Gaus(vars[0],params[8],params[9]);
        }
        else {
                return
                (vars[0]-params[2])/pow(params[3],2)*params[1]*TMath::Gaus(vars[0],params[2],params[3])+
                (vars[0]-params[5])/pow(params[6],2)*params[4]*TMath::Gaus(vars[0],params[5],params[6])+
                (vars[0]-params[8])/pow(params[9],2)*params[7]*TMath::Gaus(vars[0],params[8],params[9])+
                (vars[0]-params[11])/pow(params[12],2)*params[10]*TMath::Gaus(vars[0],params[11],params[12]);
        }
}

void fit(const char* dat_name,double* params,double low, double high,int num,const char* canvas_name,const char* canvas_name_2,int w){
	std::string prefix="../Protokoll/Plots/";
	std::string suffix=".pdf";
        gStyle->SetOptStat(0);
        gStyle->SetOptFit(1);

        double* mc = read_mca(dat_name);
        double* mc_hist=tohist(mc);
        TF1* gausse=new TF1("gausse", gauss, low, high,3*num+1);
        gausse->SetLineColor(kRed);
        for(int i=1;i<3*num+1;i++){
                gausse->SetParameter(i,params[i]);
        }
        gausse->FixParameter(0,num);
//      cout<<num<<endl;
//      cout<<gauss->GetParameter(3)<<endl;
        double p0=0.;
        double p1=1.;
        double lowerbound=p1+p0;

        double upperbound=(mc[0])*p1+p0;

        for(int i=1;i<=mc_hist[0];i++){
                mc_hist[i]=p0+p1*mc_hist[i];
        }

        TH1D* hist_me=new TH1D(canvas_name,"Fit Kalibrierung;Channel; Events",4096,lowerbound,upperbound);
        TH1D* hist_me2=new TH1D(canvas_name_2,"Fit Kalibrierung;Channel; Events",4096,lowerbound,upperbound);

        for (int i=1;i<=mc_hist[0];i++){
                hist_me->Fill(mc_hist[i]);
                hist_me2->Fill(mc_hist[i]);
        }
        hist_me->GetXaxis()->SetRangeUser(low-2*p1,high+2*p1);
        TCanvas* me_plot=new TCanvas(canvas_name,dat_name,900,600);
        me_plot->Divide(1,2,0,0);

        me_plot->cd(1);
        me_plot->GetPad(1)->SetPad(0.,0.4,1.,1.);

        me_plot->GetPad(1)->SetBottomMargin(0.012);
        me_plot->GetPad(1)->SetTopMargin(0.07);
        me_plot->GetPad(1)->SetRightMargin(0.01);
        me_plot->GetPad(1)->SetLeftMargin(0.08);

        hist_me->GetXaxis()->SetLabelSize(0.052);
        hist_me->GetXaxis()->SetTitleSize(0.052);
        hist_me->GetYaxis()->SetLabelSize(0.052);
        hist_me->GetYaxis()->SetTitleSize(0.052);
		hist_me->GetYaxis()->SetTitleOffset(0.8);

        hist_me->Draw("E");
        hist_me2->Draw("same");
        gausse->Draw("same");
        TLegend* leg1= new TLegend(0.4,0.75,0.6,0.935);
        leg1->AddEntry(hist_me2,"Histogram+Fehler","EL");
        leg1->AddEntry("gausse","Fit","l");
        leg1->SetFillColor(0);
        leg1->Draw("same");


        int n_points=int((high-low)/p1+1.);
//      int n_points=64/p1;
        double x_err=p1/sqrt(12.);
        auto fitres= hist_me->Fit(gausse,"S","q",low,high);
/*
        TF1* d_gausse =new TF1("d_gausse",d_gauss,low,high,3*num+1);

        for(int i=1;i<3*num+1;i++){
                d_gausse->SetParameter(i,gausse->GetParameter(i));
        }
        d_gausse->FixParameter(0,num);
        double res[n_points];
        double bins[n_points];
        double res_err[n_points];

        for (int i =0;i<n_points;i++) {
                res[i] = hist_me->GetBinContent(i+int((low-p0)/p1))- gausse->Eval(hist_me->GetBinCenter(i+int((low-p0)/p1)));
                bins[i]=hist_me->GetBinCenter(i+int((low-p0)/p1));
                res_err[i]=sqrt(hist_me->GetBinContent(i+int((low-p0)/p1))+pow(d_gausse->Eval(hist_me->GetBinCenter(i+int((low-p0)/p1)))*x_err,2));
        }

        me_plot->cd();
        me_plot->cd(2);

        me_plot->GetPad(2)->SetPad(0.,0.,1.,0.4);
        TGraphErrors residuen(n_points,bins,res,nullptr,res_err);

        me_plot->GetPad(2)->SetGridy();
        residuen.GetXaxis()->SetLabelSize(0.08);
        residuen.GetXaxis()->SetTitle("Channel");
        residuen.GetXaxis()->SetTitleSize(0.08);
        residuen.GetYaxis()->SetTitle("Residuum");
        residuen.GetYaxis()->SetLabelSize(0.08);
        residuen.GetYaxis()->SetTitleSize(0.08);
        residuen.GetXaxis()->SetLimits(low-2*p1,high+2*p1);
		residuen.GetYaxis()->SetTitleOffset(0.4);
        residuen.SetMarkerStyle(8);
        residuen.SetMarkerSize(0.4);

        me_plot->GetPad(2)->SetFrameFillColor(0);
        me_plot->GetPad(2)->SetFrameBorderMode(0);

        me_plot->GetPad(2)->SetBottomMargin(0.17);
        me_plot->GetPad(2)->SetTopMargin(0.04);
        me_plot->GetPad(2)->SetRightMargin(0.01);
        me_plot->GetPad(2)->SetLeftMargin(0.08);

        residuen.DrawClone("APE");
        TLine *line = new TLine(low-2*p1,0,high+2*p1,0);
        line->SetLineColor(kRed);
        line->Draw("same");
        TLegend* leg2= new TLegend(0.8,0.8,0.98,0.92);
        leg2->AddEntry("residuen","Residuum+Fehler","EP");
        leg2->SetFillColor(0);
        leg2->Draw("same");


	std::string c=canvas_name;
	std::string file=prefix + c +suffix;
	const char* filename=file.c_str();
	me_plot->SaveAs(filename,"pdf");
//	write result to file

	stringstream s;
	s <<"../Daten/"<< "Fit_"<<dat_name[15]<<dat_name[16]<<"_clean";
	string out_name=s.str(); 
    ofstream fitp;
    fitp.open(out_name);
    fitp<<"<<DATA>>\n";
	
	switch(num){
	case 4 :{fitp << fitres->GetParams()[11]<<"\n"<<gausse->GetParError(11)<<"\n";}
	case 3 :{fitp << fitres->GetParams()[8]<<"\n"<<gausse->GetParError(8)<<"\n";}
	case 2 :{fitp << fitres->GetParams()[5]<<"\n"<<gausse->GetParError(5)<<"\n";}
	case 1 :{fitp << fitres->GetParams()[2]<<"\n"<<gausse->GetParError(2)<<"\n";}
	}
	fitp<<"<<END>>";
    fitp.close();
*/
/*	
	//Write result to csv for table
	vector<string> input;
	ifstream table_i;
	table_i.open("Fit_known.csv");
	string content;
	while(table_i>>content){
//		cout<<content<<endl;
		input.push_back(content);
	}
	table_i.close();
//	int k=2;
//	cout<<input[0]<<endl;//.at(0)<<endl;

	
	ofstream table_o;
	table_o.open("Fit_known.csv");
	int k=0;
	char* temp_c;
	double temp_d=0;
	bool read=true;
	while(true){
		temp_c=&input[k].at(0);
		string temp_s(temp_c);	
		temp_d=stod(temp_s);

		if (temp_d==w && read){			
		for(int i=0;i<num;i++){
			table_o<<w<<","<<fitres->GetParams()[i+2]<<","<<fitres->GetParams()[i+3]<<"\n";
		read=false;
		}
		k++;
		continue;
		}

		else if(int(temp_d)==0)break;
		else if(w!=temp_d){
			table_o<<input[k]<<"\n";
			k++;
			continue;
		}
//	nur als fehlermeldung 	
		if(k>1e8){
			cerr << "Endlosschleife"<<endl;
			break;
		}
	}
	table_o.close();		
	}		
*/
}
int fit_known(){
        double params_1[7]={2,2500,519,5,550,579,5};
        fit ("../Daten/Kalib_Cu_clean",params_1,508,585,int(params_1[0]),"Kalib_Cu_clean","Kalib_Cu_clean_2",1);


//	double params_6[7]={2,11000,860,10,2000,959,7};
//	fit ("../Daten/Kalib_Rb_clean",params_6,843,975,int(params_6[0]),"Kalib_Rb_clean","Kalib_Rb_clean_2",2);
//       
//	 double params_2[10]={3,12000,1430,10,2000,1600,10,500,1635,10};
//        fit ("../Daten/Kalib_Ag_clean",params_2,1395,1660,int(params_2[0]),"Kalib_Ag_clean","Kalib_Ag_clean_2",3);
//
//        double params_3[10]={3,15000,1125,10,2000,1260,10,500,1280,10};
//        fit ("../Daten/Kalib_Mo_clean",params_3,1100,1295,int(params_3[0]),"Kalib_Mo_clean","Kalib_Mo_clean_2",4);
//
//        double params_4[13]={4,2500,2046,10,4000,2070,10,1000,2340,10,400,2395,10};
//        fit ("../Daten/Kalib_Ba_clean",params_4,2025,2421,int(params_4[0]),"Kalib_Ba_clean","Kalib_Ba_clean_2",5);
//
//        double params_5[13]={4,1200,2820,10,2000,2860,10,500,3235,10,150,3325,10};
//        fit ("../Daten/Kalib_Tb_clean",params_5,2790,3350,int(params_5[0]),"Kalib_Tb_clean","Kalib_Tb_clean_2",6);

	return 0;
}
