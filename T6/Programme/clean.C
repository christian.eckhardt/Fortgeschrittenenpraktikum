#include "read_mca.h"
#include "tohist.h"
#include <fstream>
#include <string>
#include <vector>
#include <stdexcept>
#include "comma.h"
void clean(){

//	histogramme einlesen
	Int_t size1 = read_mca("../Daten/Kalib_Cu.mca")[0];
	Int_t size2 = 2*(size1/2+1);
	const int size2_c = 2*(size1/2+1);
	const int num = 16; //Number of histograms
	const int mit_leer=6; //Anzahl von den nicht Leer abgezogen wird
	const char* names[num] = {"../Daten/Kalib_Tb.mca",
		"../Daten/Kalib_Rb.mca","../Daten/Kalib_Ba.mca",
		"../Daten/Kalib_Cu.mca","../Daten/Kalib_Ag.mca",
		"../Daten/Kalib_Mo.mca",
		"../Daten/Messung1.mca","../Daten/Messung2.mca",
		"../Daten/Messung4_Stein.mca","../Daten/Messung5_Kronkorken.mca",
		"../Daten/Messung6_Batterie.mca","../Daten/Messung7_klotz.mca",
		"../Daten/Messung8_muenzej.mca","../Daten/Messung9_Metall.mca",
		"../Daten/Messung10_usb.mca","../Daten/Messung11_tabak.mca"};
		

	const char* out_names[num] = {"../Daten/Kalib_Tb_clean_mod",
		"../Daten/Kalib_Rb_clean_mod","../Daten/Kalib_Ba_clean_mod",
		"../Daten/Kalib_Cu_clean_mod","../Daten/Kalib_Ag_clean_mod",
        "../Daten/Kalib_Mo_clean_mod",
		"../Daten/Messung1_clean_mod","../Daten/Messung2_clean_mod",
		"../Daten/Messung4_Stein_clean_mod","../Daten/Messung5_Kronkorken_clean_mod",
		"../Daten/Messung6_Batterie_clean_mod","../Daten/Messung7_klotz_clean_mod",
		"../Daten/Messung8_muenzej_clean_mod","../Daten/Messung9_Metall_clean_mod",
		"../Daten/Messung10_usb_clean_mod","../Daten/Messung11_tabak_clean_mod"};

//////////////////////////////////////
//Livetime einlesen
//	string numb("12,5");
	string temp_s;
	double temp_d=0;
	vector<double> lt;
	double lt_arr[num];
	ifstream life;
	for(int i=0; i<num; i++){
		life.open(names[i]);
		for(int j=0;j<22;j++){
		life>>temp_s;
		if(j<21)continue;
		cout<<temp_s<<endl;
		temp_d=comma(temp_s);
		lt.push_back(temp_d);	
		}
		life.close();
	}
	for(int i=0;i<num;i++) lt_arr[i]=lt[i];

//	nochmal fuer die leermessung
	life.open("../Daten/Leermessung.mca");
	double life_leer=0;
	for(int j=0;j<22;j++){
		life>>temp_s;
		if(j<21)continue;
		cout<<temp_s<<endl;
		temp_d=comma(temp_s);
		life_leer=temp_d;
	}
	life.close();
	cout<<life_leer<<endl;
///////////////////////////////////
	double* leer=&read_mca("../Daten/Leermessung.mca")[1];
	double* datas[num];
	double* datas_l[num];
	double* trafos[num];
	for(int i=0; i<num; i++){
		datas[i]=&read_mca(names[i])[1];
		datas_l[i]=new double[size1];
		trafos[i]=new double[size2];
		if(i<mit_leer) continue;
		for(int j=0; j<size1;j++){
			datas_l[i][j]=datas[i][j]-(leer[j]*(lt_arr[i]/life_leer));//Leerdaten abziehen
		}
	}
//	trafos[0]=temp;	
	TVirtualFFT* fft = TVirtualFFT::FFT(1,&size1,"R2C P K");
	TVirtualFFT* fft_back=TVirtualFFT::FFT(1,&size2,"C2R P K");	

//	Trafos
	
	for(int i=0;i<num;i++){
		fft->SetPoints(datas_l[i]);
		fft->Transform();
		fft->GetPoints(trafos[i]);
		cout<<trafos[i][0]<<endl;
		try{
			double t=datas_l[i][0]+0;
		}
		catch(...){
			i=i-1;
			continue;
		}
	}

//	Cutten
	double* cuts [num];
	for(int i=0;i<num;i++){
		cuts[i]= new double[size2];
	}

	const int cut = 475;
	for(int i;i<num;i++){
		for(int j=0;j<size2;j++){
			if(j<cut){
				cuts[i][j]=trafos[i][j];
			}
			else cuts[i][j]=0;
		}
	}
// Ruecktrafo
	for(int i=0;i<num;i++){
		fft_back->SetPoints(cuts[i]);
		fft_back->Transform();
		fft_back->GetPoints(datas_l[i]);
		cout<<datas_l[i][0]<<endl;
		try{
			double t=datas_l[i][0]+0;
			}
		catch(...){
			i=i-1;
			continue;
		}
		if(datas_l[i][0]>10e10) i=i-1;
		
	}

//	durch trafo negative werte die 0 sein sollten und scaling *#bins
//	wird hier korrigiert
	for(int i=0;i<num;i++){
		for(int j=0;j<size1;j++){
			datas_l[i][j]=abs(datas_l[i][j])/size1;
		}
	}

/*	TH1D* hist_test=new TH1D("test_plots","Counts;Channel",size1,1,size1);
	for(int i=0;i<size1;i++){
		for(int j=0;j<datas_l[2][i];j++){
			hist_test->Fill(i);
		}
	}
	auto c = new TCanvas("c1");
	hist_test->Draw();
*/

	double temp=0;
	for(int i=0;i<num;i++){
		ofstream output;
    	output.open(out_names[i]);
//		output << "test"<<"\n";
		output << "<<DATA>> \n";
	    for(int j=0;j<size1;j++){
	        temp =datas_l[i][j];
	        output << temp<< "\n";
			if(j==500) cout << temp << endl;
	    }
	    output << "<<END>> \n";
    	output.close();
	}

	for(int i=0;i<num;i++){ 
		delete cuts[i];
	//	delete trafos[i];
	//	delete datas_l[i];
	}
}




