#include "read_mca.h"
#include "TH1D.h"
#include "tohist.h"
#include "TROOT.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TLegend.h"
#include "TLatex.h"


void vergleich(){
	gStyle->SetOptStat(0);
        double* m1 = read_mca("../Daten/Messung1.mca");
        double* m2 = read_mca("../Daten/Messung2.mca");
  //      double* vg = read_mca("../Daten/vergleich1_2.mca");

        TH1D* hist_m1=new TH1D("Messung1","C;Channel;Events",4096,1,4096);
        TH1D* hist_m2=new TH1D("Messung2","C;Channel;Events",4096,1,4096);
//	TH1D* hist_vg=new TH1D("Vergleich 1-2","C;Channel;Events",4096,1,4096);

	hist_m1->SetLineColor(2);
	hist_m2->SetLineColor(4);
//	hist_vg->SetLineColor(6);
	hist_m1->SetFillColor(2);
	hist_m2->SetFillColor(4);
//	hist_vg->SetFillColor(6);	        
	double* m1_hist=tohist(m1);
	double* m2_hist=tohist(m2);
//	double* vg_hist=tohist(vg);

        for (int i=1;i<=m1_hist[0];i++){
                hist_m1->Fill(m1_hist[i]);
                hist_m2->Fill(m2_hist[i]);
  //      	hist_vg->Fill(vg_hist[i]);
	}


        TCanvas* c= new TCanvas("Vergleich","Vergleich",900,600);
        hist_m2->Draw("same");
        hist_m1->Draw("same");
//	hist_vg->Draw("same");
	
	TLegend* leg=new TLegend(0.1,0.7,0.4,0.9);
	leg->AddEntry(hist_m1,"Shape On","F");
	leg->AddEntry(hist_m2,"Shape Off","F");
	leg->Draw("same");	

	c->SaveAs("../Protokoll/Plots/vergleich.pdf","pdf");
}
