#include "read_mca.h"
#include "tohist.h"
#include <fstream>
#include <cmath>

double n_gauss(double* vars, double* pars){
	return 	pars[0]*TMath::Gaus(vars[0],pars[1],pars[2])+
			pars[3]*TMath::Gaus(vars[0],pars[4],pars[5]);
}

void Tb_clean(){

	Int_t size1 = read_mca("../Daten/Kalib_Tb.mca")[0];
	const int size2 = 2*(size1/2+1);
	double* data = &read_mca("../Daten/Kalib_Tb.mca")[1];
	double* data_t = read_mca("../Daten/Kalib_Tb.mca");
	double* trafo = new double[size2];
	
//	gauss_fit
	
    double * data_h=tohist(data_t);
    TH1D * hist_data=new TH1D("Kalib_Tb","Counts;Channel",4096,1,4096);
	
	for (int i=1;i<=data_h[0];i++){
		hist_data->Fill(data_h[i]);
	}

	auto c2= new TCanvas("c2"); 
	hist_data->Draw();

	TF1 gauss("gauss",n_gauss,1,4096,6);
	double norm1=2000; double mean1=515; double sig1=5;
	double norm2=1500; double mean2=550; double sig2=5;
	gauss.SetParameters(norm1,mean1,sig1,norm2,mean2,sig2);
	auto res1=hist_data->Fit(&gauss,"S","q",450,650);
	res1->Print();
	gauss.Draw("Same");
//	array mit gauss daten	
	double g_arr[size1];
	for(int i=0;i<size1;i++){
		g_arr[i]=gauss(i);
	}
//	FFTs der Daten und des Gauss-Fit

	TVirtualFFT *fft_obj = TVirtualFFT::FFT(1, &size1, "R2C ES K");
	fft_obj->SetPoints(data);
	fft_obj->Transform();
	fft_obj->GetPoints(trafo);

	TH1* h1=0;
	h1=TH1::TransformHisto(fft_obj,h1,"MAG");
	auto c1= new TCanvas("c1");
	h1->Draw();

	double trafo2[size2];
	fft_obj->SetPoints(g_arr);
	fft_obj->Transform();
	fft_obj->GetPoints(g_arr);

	TH1* h2=0;
	h2=TH1::TransformHisto(fft_obj,h2,"MAG");
	auto c3= new TCanvas("c3");
	h2->Draw();
//	Histogram beschneiden
	const int k_cut=700;
	for(int i=0;i<size2;i++){
		if(i<k_cut) continue;
	//	if(i<size1) h1->SetBinContent(i,0);
		trafo[i]=0;
	}
	
	auto c5= new TCanvas("c5");
	h1->Draw();
//	Ruecktrafo
	TVirtualFFT *fft_inv = TVirtualFFT::FFT(1, &size1, "C2R ES K");
	fft_inv->SetPoints(trafo);
	fft_inv->Transform();
	fft_inv->GetPoints(data);
	
	TF1 cnst("const","1",0,4097);

	TH1* h3=0;
	h3=TH1::TransformHisto(fft_inv,h3,"RE");
	h3->Divide(&cnst, size1);

	auto c4 = new TCanvas("c4");
	h3->Draw();

	TF1 gauss2("gauss2",n_gauss,1,4096,6);
	gauss2.SetParameters(norm1,mean1,sig1,norm2,mean2,sig2);
	auto res2=h3->Fit(&gauss2,"S","q",450,650);
	res2->Print();
	
//	write data to file
/*
	ofstream output;
	output.open("../Daten/Kalib_Tb_clean");
	output << "<<DATA>> \n";
	double temp=0;
	for(int i;i<size1;i++){
		temp =(h3->GetBinContent(i)); 
		output << temp<< "\n";
	}
	output << "<<END>> \n";
	output.close();
*/	
}

	

