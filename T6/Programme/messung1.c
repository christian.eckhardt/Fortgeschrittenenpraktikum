#include "read_mca.h"
#include "TH1D.h"
#include "tohist.h"
#include "TGraphErrors.h"
#include "TROOT.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TLine.h"

void messung1(){
        gStyle->SetOptStat(0);
	gStyle->SetOptFit(1);
        double* m = read_mca("../Daten/Messung1.mca");
	double* ml = read_mca("../Daten/Leermessung.mca");
	
	double* m_hist_raw=tohist(m);

        TH1D * hist_m_raw=new TH1D("Messung 1_raw","C;Kanal;Events",4096,1,4097);
        for (int i=1;i<=m_hist_raw[0];i++){
                hist_m_raw->Fill(m_hist_raw[i]);
        }

        TCanvas* m_raw_plot= new TCanvas("hist_Messung_raw 1","Hong_Kong_raw",800,600);
        hist_m_raw->Draw();



        for (int i=1;i<=m[0];i++){
                m[i]=m[i]-ml[i];
                if(m[i]<0){m[i]=0; }
        }

        double* m_hist=tohist(m);

        TH1D * hist_mc=new TH1D("Messung 1-Leermessung","C;Kanal;Events",4096,1,4097);
        for (int i=1;i<=m_hist[0];i++){
                hist_mc->Fill(m_hist[i]);
        }

        TCanvas* mc_plot= new TCanvas("hist_Messung 1-Leermessung","Hong_Kong",800,600);
        hist_mc->Draw();
        double p0=0.;
        double p1=1.;


        double* mc = read_mca("../Daten/Messung1_clean");

        for (int i=1;i<=mc[0];i++){
                mc[i]=mc[i]-ml[i];
                if(mc[i]<0){mc[i]=0; }
        }

        double* mc_hist= tohist(mc);

        double lowerbound=p1+p0;

        double upperbound=4096*p1+p0+1;

        for(int i=1;i<=mc_hist[0];i++){
                mc_hist[i]=p0+p1*mc_hist[i];
        }


        TF1 gaussians("gaussians","[0]*TMath::Gaus(x,[1],[2])+[3]*TMath::Gaus(x,[4],[5])+[6]*TMath::Gaus(x,[7],[8])",lowerbound,upperbound);
        double norm1=100. ; double mean1=480. ; double sig1=10. ;
        double norm2=500. ; double mean2=520. ; double sig2=10. ;
        double norm3=100. ; double mean3=555. ; double sig3=1. ;


        gaussians.SetParameters(norm1,mean1,sig1,norm2,mean2,sig2,norm3,mean3,sig3);
        gaussians.SetParNames("Norm1","Mean1","Sig1","Norm2","Mean2","Sig2","Norm3","Mean3","Sig3");




        TH1D* hist_me=new TH1D("Messung 1_clean","E;Energie [eV]; Events",4096,lowerbound,upperbound);
        for (int i=1;i<=mc_hist[0];i++){
                hist_me->Fill(mc_hist[i]);
	}
	hist_me->GetXaxis()->SetRangeUser(400,600);
	TCanvas* me_plot=new TCanvas("hist_Messung_clean 1","Hong_Kong_clean",800,600);
	me_plot->Divide(1,2,0,0);

        me_plot->cd(1);
	me_plot->GetPad(1)->SetPad(0.,0.4,1.,1.);
	me_plot->GetPad(1)->SetBottomMargin(1);
	me_plot->GetPad(1)->SetTopMargin(0.01);
	me_plot->GetPad(1)->SetRightMargin(0.01);

	hist_me->Draw();

	int lowerfit=450;
	int upperfit=600;
	int n_points=upperfit-lowerfit;

	double x_err=sqrt(12.);

	auto fitres= hist_me->Fit(&gaussians,"S","q",lowerfit,upperfit);

	norm1=gaussians.GetParameter(0); mean1=gaussians.GetParameter(1); sig1=gaussians.GetParameter(2);
	double norm1_err=gaussians.GetParError(0); double mean1_err=gaussians.GetParError(1); double sig1_err=gaussians.GetParError(2);

	norm2=gaussians.GetParameter(3); mean2=gaussians.GetParameter(4); sig2=gaussians.GetParameter(5);
	double norm2_err=gaussians.GetParError(3); double mean2_err=gaussians.GetParError(4); double sig2_err=gaussians.GetParError(5);


	norm3=gaussians.GetParameter(6); mean3=gaussians.GetParameter(7); sig3=gaussians.GetParameter(8);
	double norm3_err=gaussians.GetParError(6); double mean3_err=gaussians.GetParError(7); double sig3_err=gaussians.GetParError(8);

	TF1 dofgaussians("dofgaussians","[0]/pow([2],2)*TMath::Gaus(x,[1],[2])*(x-[1])+[3]/pow([5],2)*TMath::Gaus(x,[4],[5])*(x-[4])+[6]/pow([8],2)*TMath::Gaus(x,[7],[8])*(x-[7])",lowerbound,upperbound);
	dofgaussians.SetParameters(norm1,mean1,sig1,norm2,mean2,sig2,norm3,mean3,sig3);

	double res[n_points];
	double bins[n_points];
	double res_err[n_points];
        for (int i =0;i<n_points;i++) {
		res[i] = hist_me->GetBinContent(i+lowerfit) - gaussians.Eval(hist_me->GetBinCenter(i+lowerfit));
		bins[i]=i+lowerfit;		 
		res_err[i]=sqrt(hist_me->GetBinContent(i+lowerfit)+pow(dofgaussians.Eval(i+lowerfit)*1./sqrt(12),2));
	}
	me_plot->cd();
	me_plot->cd(2);

	me_plot->GetPad(2)->SetPad(0.,0.,1.,0.4);
	TGraphErrors residuen(n_points,bins,res,nullptr,res_err);
        me_plot->GetPad(2)->SetGridy();
	residuen.GetXaxis()->SetTitle("Energie [eV]");
	residuen.GetYaxis()->SetTitle("Residuum");
	residuen.GetXaxis()->SetLimits(400,600);
	me_plot->GetPad(2)->SetFrameFillColor(0);
	me_plot->GetPad(2)->SetFrameBorderMode(0);
	me_plot->GetPad(2)->SetRightMargin(0.01);

	residuen.DrawClone("APE");
	TLine *line = new TLine(400,0,600,0);
	line->SetLineColor(kRed);
	line->Draw("same");

}


