#include "TROOT.h"
#include "read_mca.h"
#include "tohist.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TLegend.h"
#include "TLine.h"

double gewichtung(int l,double* energy,double* intent ){
	double gemittelt;
	double zahler=0;
	double nenner=0;
	for (int i=0;i<l;i++){
		zahler+=energy[i]*intent[i];
		nenner+=intent[i];
	}
	gemittelt=zahler/nenner;
	cout << gemittelt << endl;
	return gemittelt;
	}

double fehler(int l,double* energy,double* intent){
	double error;
	double temp=0;
	double ppm=5e-6;
	double Iges=0;
	for(int i=0;i<l;i++){
		temp+=pow(energy[i]*intent[i],2);
		Iges+=intent[i];
	}
	error=ppm*sqrt(temp)/Iges;
	cout << error << endl;
	return error;
}

void kalib(){



	double* Cu_peaks=read_mca("../Daten/Fit_Cu_clean");
	double* Rb_peaks=read_mca("../Daten/Fit_Rb_clean");
	double* Ag_peaks=read_mca("../Daten/Fit_Ag_clean");
	double* Mo_peaks=read_mca("../Daten/Fit_Mo_clean");
	double* Ba_peaks=read_mca("../Daten/Fit_Ba_clean");	
	double* Tb_peaks=read_mca("../Daten/Fit_Tb_clean");
	gStyle->SetOptStat(0);
	gStyle->SetOptFit(1);
	const int n_points=18;
	double channel[n_points];
	double energy[n_points];

	double channel_err[n_points];
	double energy_err[n_points];
/*
	for (int i=0;i<n_points;i++){
		channel_err[i]=0.2887;
	}
*/

	double energy_Cu1[2]={8.0278,8.0478};
	double energy_Cu2[1]={8.9053};
	
	double inten_Cu1[2]={51,100};
	double inten_Cu2[1]={1};	
	
	double energy_Rb1[2]={13.3358,13.3953};
        double energy_Rb2[3]={14.9517,14.9613,15.185};
        
	double inten_Rb1[2]={52,100};
        double inten_Rb2[3]={7,14,2};
	
	double energy_Mo1[2]={17.3743,17.4793};
        double energy_Mo2[2]={19.5903,19.6083};
        double energy_Mo3[1]={19.9652};
	
	double inten_Mo1[2]={52,100};
        double inten_Mo2[2]={8,15};
	double inten_Mo3[1]={1};	

	double energy_Ag1[2]={21.9903,22.1629};
        double energy_Ag2[2]={24.9115,24.9424};
	double energy_Ag3[1]={25.4564};	

        double inten_Ag1[2]={53,100};
        double inten_Ag2[2]={9,16};
	double inten_Ag3[1]={1};

	double energy_Ba1[1]={31.8171};
        double energy_Ba2[1]={32.1936};
	double energy_Ba3[2]={36.304,36.3782};
	double energy_Ba4[1]={37.257};

        double inten_Ba1[1]={1};
        double inten_Ba2[1]={1};
	double inten_Ba3[2]={10,18};
	double inten_Ba4[1]={1};

	double energy_Tb1[1]={43.7441};
        double energy_Tb2[1]={44.4816};
	double energy_Tb3[2]={50.229,50.382};
	double energy_Tb4[1]={51.698};

        double inten_Tb1[1]={1};
        double inten_Tb2[1]={1};
        double inten_Tb3[2]={1,2};
	double inten_Tb4[1]={1};

	channel[0]=Cu_peaks[3];energy[0]=gewichtung(2,energy_Cu1,inten_Cu1);
	channel[1]=Cu_peaks[1];energy[1]=gewichtung(1,energy_Cu2,inten_Cu2);

	channel[2]=Rb_peaks[3];energy[2]=gewichtung(2,energy_Rb1,inten_Rb1);
	channel[3]=Rb_peaks[1];energy[3]=gewichtung(3,energy_Rb2,inten_Rb2);

	channel[4]=Ag_peaks[5];energy[4]=gewichtung(2,energy_Ag1,inten_Ag1);
	channel[5]=Ag_peaks[3];energy[5]=gewichtung(2,energy_Ag2,inten_Ag2);
        channel[6]=Ag_peaks[1];energy[6]=gewichtung(1,energy_Ag3,inten_Ag3);

	channel[7]=Mo_peaks[5];energy[7]=gewichtung(2,energy_Mo1,inten_Mo1);
        channel[8]=Mo_peaks[3];energy[8]=gewichtung(2,energy_Mo2,inten_Mo2);
	channel[9]=Mo_peaks[1];energy[9]=gewichtung(1,energy_Mo3,inten_Mo3);

	channel[10]=Ba_peaks[7];energy[10]=gewichtung(1,energy_Ba1,inten_Ba1);
	channel[11]=Ba_peaks[5];energy[11]=gewichtung(1,energy_Ba2,inten_Ba2);
	channel[12]=Ba_peaks[3];energy[12]=gewichtung(2,energy_Ba3,inten_Ba3);
	channel[13]=Ba_peaks[1];energy[13]=gewichtung(1,energy_Ba4,inten_Ba4);

	channel[14]=Tb_peaks[7];energy[14]=gewichtung(1,energy_Tb1,inten_Tb1);
	channel[15]=Tb_peaks[5];energy[15]=gewichtung(1,energy_Tb2,inten_Tb2);
	channel[16]=Tb_peaks[3];energy[16]=gewichtung(2,energy_Tb3,inten_Tb3);
	channel[17]=Tb_peaks[1];energy[17]=gewichtung(1,energy_Tb4,inten_Tb4);


	channel_err[0]=1.5;//Cu_peaks[4];//3;
	energy_err[0]=fehler(2,energy_Cu1,inten_Cu1);
	
	channel_err[1]=2;//Cu_peaks[2];//4;
	energy_err[1]=fehler(2,energy_Cu2,inten_Cu2);

	channel_err[2]=2;//Rb_peaks[4];//4;
	energy_err[2]=fehler(2,energy_Rb1,inten_Rb1);
	
	channel_err[3]=1;//Rb_peaks[2];//2;
	energy_err[3]=fehler(2,energy_Rb2,inten_Rb2);

	channel_err[4]=3;//Ag_peaks[6];//6;
	energy_err[4]=fehler(2,energy_Ag1,inten_Ag1);
	
	channel_err[5]=0.5;//;Ag_peaks[4];//1;
	energy_err[5]=fehler(2,energy_Ag2,inten_Ag2);
	
	channel_err[6]=1;//Ag_peaks[2];//2;
	energy_err[6]=fehler(2,energy_Ag3,inten_Ag3);

        channel_err[7]=2.5;//Mo_peaks[6];//5;
	energy_err[7]=fehler(2,energy_Mo1,inten_Mo1);
        
	channel_err[8]=1;//Mo_peaks[4];//2;
	energy_err[8]=fehler(2,energy_Mo2,inten_Mo2);
        
	channel_err[9]=1.5;//Mo_peaks[2];//3;
	energy_err[9]=fehler(2,energy_Mo3,inten_Mo3);

        channel_err[10]=1;//Ba_peaks[8];//2;
	energy_err[10]=fehler(2,energy_Ba1,inten_Ba1);
        
	channel_err[11]=0.5;//Ba_peaks[6];//1;
	energy_err[11]=fehler(2,energy_Ba2,inten_Ba2);
	
	channel_err[12]=1.5;//Ba_peaks[4];//3;
	energy_err[12]=fehler(2,energy_Ba3,inten_Ba3);
        
	channel_err[13]=1.5;//Ba_peaks[2];//3;
	energy_err[13]=fehler(2,energy_Ba4,inten_Ba4);

        channel_err[14]=0.5;//Tb_peaks[8];//1;
	energy_err[14]=fehler(2,energy_Tb1,inten_Tb1);
        
	channel_err[15]=0.5;//Tb_peaks[6];//1;
	energy_err[15]=fehler(2,energy_Tb2,inten_Tb2);
        
	channel_err[16]=1.5;//Tb_peaks[4];//3;
	energy_err[16]=fehler(2,energy_Tb3,inten_Tb3);
        
	channel_err[17]=0.5;//Tb_peaks[2];//1;
	energy_err[17]=fehler(2,energy_Tb4,inten_Tb4);



	TGraphErrors graph(n_points,channel,energy,channel_err,energy_err);
	graph.SetTitle("Channel->Energy;Channel; Energy [keV]");

	TCanvas* trafo=new TCanvas("Kalibration","Kalibration",900,600);
	trafo->Divide(1,2,0,0);
	trafo->cd(1);
	trafo->GetPad(1)->SetPad(0,0.4,1.,1.);
	trafo->GetPad(1)->SetBottomMargin(0.012);
	trafo->GetPad(1)->SetTopMargin(0.07);	
	trafo->GetPad(1)->SetRightMargin(0.01);
	trafo->GetPad(1)->SetLeftMargin(0.08);
	
	graph.GetYaxis()->SetLabelSize(0.052);
	graph.GetYaxis()->SetTitleSize(0.052);
	graph.GetYaxis()->SetTitleOffset(0.8);	

	TF1* f=new TF1("Linear Law","[0]+x*[1]",0.,30.0);
	f->SetParameter(0,0);
	f->SetParameter(1,1);

	auto Fitpara=graph.Fit(f,"S","q");
	graph.DrawClone();
//	f->Draw("Same");

	TLegend* leg1=new TLegend(0.1,0.7,0.3,0.9);
	leg1->AddEntry("graph","Daten","EP");
	leg1->AddEntry(f,"Fit","L");
	leg1->SetFillColor(0);
	leg1->Draw("same");

	
	double res[n_points];
	double res_err[n_points];

	for (int i=0;i<n_points;i++){
		cout<<channel[i]<<endl;
		res[i]=energy[i]-f->Eval(channel[i]);
		res_err[i]=sqrt(pow(energy_err[i],2)+pow(channel_err[i]*f->GetParameter(1),2));
	}

	trafo->cd();
	trafo->cd(2);

	trafo->GetPad(2)->SetPad(0.,0.,1.,0.4);
	TGraphErrors residuen(n_points,channel,res,nullptr,res_err);

        trafo->GetPad(2)->SetGridy();
        residuen.GetXaxis()->SetLabelSize(0.08);
        residuen.GetXaxis()->SetTitle("Channel");
        residuen.GetXaxis()->SetTitleSize(0.08);
        residuen.GetYaxis()->SetTitle("Residuum");
        residuen.GetYaxis()->SetLabelSize(0.08);
        residuen.GetYaxis()->SetTitleSize(0.08);
	residuen.GetYaxis()->SetTitleOffset(0.4);
        residuen.SetMarkerStyle(8);
        residuen.SetMarkerSize(0.4);

        trafo->GetPad(2)->SetFrameFillColor(0);
        trafo->GetPad(2)->SetFrameBorderMode(0);
	trafo->GetPad(2)->SetBottomMargin(0.17);
	trafo->GetPad(2)->SetTopMargin(0.04);
        trafo->GetPad(2)->SetRightMargin(0.01);
	trafo->GetPad(2)->SetLeftMargin(0.08);


        residuen.DrawClone("APE");
        TLine *line = new TLine(channel[0]-1,0,channel[n_points-1]+1,0);
        line->SetLineColor(kRed);
        line->Draw("same");
        TLegend* leg2= new TLegend(0.4,0.2,0.6,0.4);
        leg2->AddEntry("residuen","Residuum+Fehler","EP");
        leg2->SetFillColor(0);
        leg2->Draw("same");


        trafo->SaveAs("../Protokoll/Plots/Kalibration.root","root");


	Fitpara->Print();
	TMatrixDSym covMatrix (Fitpara->GetCovarianceMatrix());
	covMatrix.Print();


	double c0=f->GetParameter(0);
	double c1=f->GetParameter(1);

	double err_c0=f->GetParError(0);
	double err_c1=f->GetParError(1);
	cout<<c0<<"+/-"<<err_c0<<endl;
	cout<<c1<<"+/-"<<err_c1<<endl;
//	output als file

	ofstream fitres;
	fitres.open("../Daten/kalib_params");
	fitres<<"<<DATA>>\n";
	fitres<<c0<<"\n"<<err_c0<<"\n"<<c1<<"\n"<<err_c1<<"\n";
	fitres<<"<<END>>";
	fitres.close();


}


