#include "read_mca.h"
#include "TH1D.h"
#include "tohist.h"
#include "TGraphErrors.h"
#include "TROOT.h"
#include "TF1.h"
#include "TCanvas.h"
#include "RooGlobalFunc.h"
#include "RooRealVar.h"
#include "RooExtendPdf.h"
#include "RooRealVar.h"
#include "RooDataHist.h"
#include "RooDataSet.h"
#include "RooAddPdf.h"
#include "RooPlot.h"
#include "RooFitResult.h"
#include "RooGaussian.h"
#include "RooGenericPdf.h"
#include "RooExponential.h"
#include "RooHist.h"


using namespace RooFit;
void rawdata(){
	 
	gStyle->SetOptStat(0);
	double * ml_graph=  read_mca("../Daten/Leermessung.mca");
	double * ml_hist=  tohist(ml_graph);
;

	TH1D * hist_ml=new TH1D("Leermessung","Channel;Counts",4095,1,4096);
	for (int i=1;i<=ml_hist[0];i++){
		hist_ml->Fill(ml_hist[i]);
		}
	

	RooRealVar channel("channel","number of channel",1,4096);
	RooRealVar mu("mu","gaussian mean",500,0,4096);
	RooRealVar sig("sig","gaussian sigma",500,0,2000);
	
	RooDataHist data_ml("data_ml","Hist for ml",channel,hist_ml);
	
	RooGaussian gaus("gaussian","gaus",channel,mu,sig);

	RooFitResult * rawdata=gaus.fitTo(data_ml,Save());

	TCanvas* ml_plot1= new TCanvas("hist_leermessung","leere Messung",800,600);
	hist_ml->Draw("E1");
	
	TCanvas* ml_plot= new TCanvas("hahahaha","hahahahahahah",800,600);
	ml_plot->Divide(1,2,0,0);
	ml_plot->cd(1);	
	RooPlot* frame=channel.frame();
	data_ml.plotOn(frame,MarkerColor(kBlue),MarkerSize(.4),LineColor(kBlue));
	gaus.plotOn(frame,LineColor(kRed));
	gaus.paramOn(frame);
	frame->Draw();

	ml_plot->cd(2);
	cout<<"chi"<<frame->chiSquare()<<endl;
	RooHist* ml_resid= frame->residHist();
	RooPlot* residframe=channel.frame(Title("Residual Distribution of Gaus-Fit"));
	residframe->addPlotable(ml_resid,"P");
	residframe->Draw();
	ml_plot->GetPad(2)->SetGridy();	
	



}
