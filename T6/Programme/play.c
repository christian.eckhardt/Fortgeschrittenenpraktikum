#include "TROOT.h"
#include "read_mca.h"
#include "tohist.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TGraphErrors.h"
#include <fstream>
#include "TLegend.h"
#include "TLine.h"



void play(){
        double* Cu_peaks=read_mca("../Daten/Fit_Cu_Dirty");
        double* Rb_peaks=read_mca("../Daten/Fit_Rb_Dirty");
        double* Ag_peaks=read_mca("../Daten/Fit_Ag_Dirty");
        double* Mo_peaks=read_mca("../Daten/Fit_Mo_Dirty");
        double* Ba_peaks=read_mca("../Daten/Fit_Ba_Dirty");
        double* Tb_peaks=read_mca("../Daten/Fit_Tb_Dirty");
        gStyle->SetOptStat(0);
        gStyle->SetOptFit(1);
        const int n_points=12;
        double sigma[n_points];
        double energy[n_points];
        double sigma_err[n_points];
        double energy_err[n_points];

//        energy[0]=Cu_peaks[1];
//        energy[1]=Cu_peaks[5];

//        energy[2]=Rb_peaks[1];
//        energy[2]=Rb_peaks[5];

        energy[0]=Ag_peaks[1];
        energy[1]=Ag_peaks[5];
//        energy[6]=Ag_peaks[9];

        energy[2]=Mo_peaks[1];
        energy[3]=Mo_peaks[5];
//        energy[7]=Mo_peaks[9];

        energy[4]=Ba_peaks[1];
        energy[5]=Ba_peaks[5];
        energy[6]=Ba_peaks[9];
        energy[7]=Ba_peaks[13];

        energy[8]=Tb_peaks[1];
        energy[9]=Tb_peaks[5];
        energy[10]=Tb_peaks[9];
        energy[11]=Tb_peaks[13];


//        energy_err[0]=Cu_peaks[2];
//        energy_err[1]=Cu_peaks[6];

//        energy_err[2]=Rb_peaks[2];
//        energy_err[2]=Rb_peaks[6];

        energy_err[0]=Ag_peaks[2];
        energy_err[1]=Ag_peaks[6];
//        energy_err[6]=Ag_peaks[10];

        energy_err[2]=Mo_peaks[2];
        energy_err[3]=Mo_peaks[6];
//        energy_err[7]=Mo_peaks[10];

        energy_err[4]=Ba_peaks[2];
        energy_err[5]=Ba_peaks[6];
        energy_err[6]=Ba_peaks[10];
        energy_err[7]=Ba_peaks[14];

        energy_err[8]=Tb_peaks[2];
        energy_err[9]=Tb_peaks[6];
        energy_err[10]=Tb_peaks[10];
        energy_err[11]=Tb_peaks[14];


//        sigma[0]=Cu_peaks[3];
//        sigma[1]=Cu_peaks[7];

//        sigma[2]=Rb_peaks[3];
//        sigma[2]=Rb_peaks[7];

        sigma[0]=Ag_peaks[3];
        sigma[1]=Ag_peaks[7];
//        sigma[6]=Ag_peaks[11];

        sigma[2]=Mo_peaks[3];
        sigma[3]=Mo_peaks[7];
//        sigma[7]=Mo_peaks[11];

        sigma[4]=Ba_peaks[3];
        sigma[5]=Ba_peaks[7];
        sigma[6]=Ba_peaks[11];
        sigma[7]=Ba_peaks[15];

        sigma[8]=Tb_peaks[3];
        sigma[9]=Tb_peaks[7];
        sigma[10]=Tb_peaks[11];
        sigma[11]=Tb_peaks[15];



//        sigma_err[0]=Cu_peaks[4];
//        sigma_err[1]=Cu_peaks[8];

//        sigma_err[2]=Rb_peaks[4];
//        sigma_err[2]=Rb_peaks[8];

        sigma_err[0]=Ag_peaks[4];
        sigma_err[1]=Ag_peaks[8];
//        sigma_err[6]=Ag_peaks[12];

        sigma_err[2]=Mo_peaks[4];
        sigma_err[3]=Mo_peaks[8];
//        sigma_err[7]=Mo_peaks[12];

        sigma_err[4]=Ba_peaks[4];
        sigma_err[5]=Ba_peaks[8];
        sigma_err[6]=Ba_peaks[12];
        sigma_err[7]=Ba_peaks[16];

        sigma_err[8]=Tb_peaks[4];
        sigma_err[9]=Tb_peaks[8];
        sigma_err[10]=Tb_peaks[12];
        sigma_err[11]=Tb_peaks[16];



        double rel_auf[n_points];
        double rel_auf_err[n_points];
        double x[n_points];
        double x_err[n_points];

        for (int i=0;i<n_points;i++){
                rel_auf[i]=sigma[i]/energy[i];
                rel_auf_err[i]=sqrt(pow(sigma_err[i]/energy[i],2)+pow(sigma[i]*energy_err[i]/(energy[i]*energy[i]),2));
                x[i]=1./sqrt(energy[i]);
                x_err[i]=energy_err[i]/(2*sqrt(energy[i]*energy[i]*energy[i]));
        }


        TGraphErrors graph(n_points,x,rel_auf,x_err,rel_auf_err);
        graph.SetTitle("Energieauflosung; 1/#sqrt{E}  [1/#sqrt{keV}];#DeltaE/E");

        TCanvas* trafo=new TCanvas("Auflosung","Auflosung",900,600);
        trafo->Divide(1,2,0,0);
        trafo->cd(1);
        trafo->GetPad(1)->SetPad(0,0.4,1.,1.);
        trafo->GetPad(1)->SetBottomMargin(0.012);
        trafo->GetPad(1)->SetTopMargin(0.07);
        trafo->GetPad(1)->SetRightMargin(0.01);
	trafo->GetPad(1)->SetLeftMargin(0.08);

	graph.GetYaxis()->SetLabelSize(0.052);
	graph.GetYaxis()->SetTitleSize(0.052);
	graph.GetYaxis()->SetTitleOffset(0.8);


        TF1 f("Linear Law","[0]+x*[1]",0.,30.0);
        f.SetParameter(0,0);
        f.SetParameter(1,1);

        auto Fitpara=graph.Fit(&f,"S","q");
        graph.DrawClone("APE");
        f.DrawClone("Same");

        TLegend* leg1=new TLegend(0.1,0.7,0.3,0.9);



        leg1->AddEntry("graph","Daten","EP");
        leg1->AddEntry("f","Fit","L");
        leg1->SetFillColor(0);
        leg1->Draw("same");


        double res[n_points];
        double res_err[n_points];

        for (int i=0;i<n_points;i++){
                res[i]=rel_auf[i]-f.Eval(x[i]);
                res_err[i]=sqrt(pow(rel_auf_err[i],2)+pow(f.GetParameter(1)*x_err[i],2));
        }

        trafo->cd();
        trafo->cd(2);

        trafo->GetPad(2)->SetPad(0.,0.,1.,0.4);
        TGraphErrors residuen(n_points,x,res,nullptr,res_err);
	residuen.SetTitle(" ");
        trafo->GetPad(2)->SetGridy();
        residuen.GetXaxis()->SetLabelSize(0.08);
        residuen.GetXaxis()->SetTitle("1/#sqrt{E}  [1/#sqrt{keV}]");
        residuen.GetXaxis()->SetTitleSize(0.08);
        residuen.GetYaxis()->SetTitle("Residuum");
        residuen.GetYaxis()->SetLabelSize(0.08);
        residuen.GetYaxis()->SetTitleSize(0.08);
	residuen.GetYaxis()->SetTitleOffset(0.4);
        residuen.SetMarkerStyle(8);
 residuen.SetMarkerSize(0.4);

        trafo->GetPad(2)->SetFrameFillColor(0);
        trafo->GetPad(2)->SetFrameBorderMode(0);

	trafo->GetPad(2)->SetBottomMargin(0.17);
	trafo->GetPad(2)->SetTopMargin(0.04);
        trafo->GetPad(2)->SetRightMargin(0.01);
	trafo->GetPad(2)->SetLeftMargin(0.08);


        residuen.DrawClone("APE");
        TLine *line = new TLine(x[3],0,x[n_points-4],0);
        line->SetLineColor(kRed);
        line->Draw("same");
        TLegend* leg2= new TLegend(0.1,0.1,0.3,0.3);
        leg2->AddEntry("residuen","Residuum+Fehler","EP");
        leg2->SetFillColor(0);
        leg2->Draw("same");


        trafo->SaveAs("../Protokoll/Plots/play.root","root");


        Fitpara->Print();
        TMatrixDSym covMatrix (Fitpara->GetCovarianceMatrix());
        covMatrix.Print();


}

