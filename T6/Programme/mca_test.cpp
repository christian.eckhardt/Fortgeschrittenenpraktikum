#include <iostream>
#include "read_mca.h"
#include "tohist.h"
using namespace std;


int main(){

	const char* source = "../Daten/Kalib_Cu.mca";
	double* data_Cu = read_mca(source);
	double* hist_data = tohist(data_Cu);
//	double t[5] = {1,2,3,4,5};
//	double* tt=tohist(t); 
	for(int i=0; i<data_Cu[0]+1; i++){
		if(i==0) continue;
		cout << hist_data[i] << endl;
	}
	return 0;
}

