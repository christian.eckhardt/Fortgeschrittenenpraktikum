#include "read_mca.h"
#include "TH1D.h"
#include "tohist.h"
#include "TGraphErrors.h"
#include "TROOT.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TPDF.h"

void  plot(const char* dat_name,const char* canvas_name,const char* canvas_name_2,const char* canvas_name_3,const char* canvas_name_4){
	
	std::string prefix="../Protokoll/Plots/";
	std::string suffix=".pdf";

        gStyle->SetOptStat(0);
        double* m = read_mca(dat_name);
	double* mc=read_mca(dat_name);
        double* ml = read_mca("../Daten/Leermessung.mca");

	TH1D* hist_m=new TH1D(canvas_name,"C;Channel;Events",4096,1,4096);
	TH1D* hist_m2=new TH1D(canvas_name_2,"C;Channel;Events",4096,1,4096);

	double* m_hist=tohist(m);

	for (int i=1;i<=m_hist[0];i++){
                hist_m->Fill(m_hist[i]);
        	hist_m2->Fill(m_hist[i]);
	}

        TCanvas* me_plot= new TCanvas(canvas_name,canvas_name,900,600);
        hist_m->Draw("E");
	hist_m2->Draw("same");

        std::string c1=canvas_name;
        std::string file1=prefix + c1 + suffix;
        const char* filename1=file1.c_str();

	me_plot->SaveAs(filename1,"pdf");	


        for (int i=1;i<=m[0];i++){
                mc[i]=mc[i]-ml[i];
                if(mc[i]<0){mc[i]=0; }
        }
        double* mc_hist=tohist(mc);

        TH1D * hist_mc=new TH1D(canvas_name_3,"C",4096,1,4096);
	TH1D * hist_mc2=new TH1D(canvas_name_4,"C",4096,1,4096);

        for (int i=1;i<=mc_hist[0];i++){
                hist_mc->Fill(mc_hist[i]);
		hist_mc2->Fill(mc_hist[i]);
        }

        TCanvas* mc_plot= new TCanvas(canvas_name_3,canvas_name_3,900,600);
        hist_mc->Draw("E");
	hist_mc2->Draw("same");

	std::string c2=canvas_name_3;
	std::string file2=prefix + c2 + suffix;
	const char* filename2=file2.c_str();

	mc_plot->SaveAs(filename2,"pdf");
}

void rohdaten(){
	plot("../Daten/Messung11_tabak.mca","Messung11_mit_Leermessung","Messung11_2","Messung11_ohne_Leermessung","Messung11_3");
	plot("../Daten/Messung10_usb.mca","Messung10_mit_Leermessung","Messung10_2","Messung10_ohne_Leermessung","Messung10_3");

	plot("../Daten/Messung9_Metall.mca","Messung9_mit_Leermessung","Messung9_2","Messung9_ohne_Leermessung","Messung9_3");
	plot("../Daten/Messung8_muenzej.mca","Messung8_mit_Leermessung","Messung8_2","Messung8_ohne_Leermessung","Messung8_3");
	plot("../Daten/Messung7_klotz.mca","Messung7_mit_Leermessung","Messung7_2","Messung7_ohne_Leermessung","Messung7_3");
	plot("../Daten/Messung6_Batterie.mca","Messung6_mit_Leermessung","Messung6_2","Messung6_ohne_Leermessung","Messung6_3");
	plot("../Daten/Messung5_Kronkorken.mca","Messung5_mit_Leermessung","Messung5_2","Messung5_ohne_Leermessung","Messung5_3");
	plot("../Daten/Messung4_Stein.mca","Messung4_mit_Leermessung","Messung4_2","Messung4_ohne_Leermessung","Messung4_3");
	plot("../Daten/Messung2.mca","Messung2_mit_Leermessung","Messung2_2","Messung2_ohne_Leermessung","Messung2_3");
	plot("../Daten/Messung1.mca","Messung1_mit_Leermessung","Messung1_2","Messung1_ohne_Leermessung","Messung1_3");
	plot("../Daten/Leermessung.mca","Leermessung_leer","Leermessung_2","L3","L4");
//plot("../Daten/vergleich1_2.mca","vergleich_1_2","V2","V3","V4");
plot("../Daten/Kalib_Tb.mca","Messung_Tb","Messung_Tb_2","V3","V4");
plot("../Daten/Kalib_Rb.mca","Messung_Rb","Messung_Rb_2","V3","V4");
plot("../Daten/Kalib_Mo.mca","Messung_Mo","Messung_Mo_2","V3","V4");
plot("../Daten/Kalib_Ag.mca","Messung_Ag","Messung_Ag_2","V3","V4");
plot("../Daten/Kalib_Cu.mca","Messung_Cu","Messung_Cu_2","V3","V4");
plot("../Daten/Kalib_Ba.mca","Messung_Ba","Messung_Ba_2","V3","V4");
plot("../Daten/Kalib_Mo_clean","Messung_Mo_clean","Messung_Mo_clean_2","V3","V4");
}
