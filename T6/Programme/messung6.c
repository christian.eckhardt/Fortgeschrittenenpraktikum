#include "read_mca.h"
#include "TH1D.h"
#include "tohist.h"
#include "TGraphErrors.h"
#include "TROOT.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TMath.h"



void messung6(){
        gStyle->SetOptStat(0);
        double* m = read_mca("../Daten/Messung6_Batterie.mca");
        double* ml = read_mca("../Daten/Leermessung.mca");

        for (int i=1;i<=m[0];i++){
                m[i]=m[i]-ml[i];
                if(m[i]<0){m[i]=0; }
        }
        double* m_hist=tohist(m);

        TH1D * hist_mc=new TH1D("Messung 6_raw","C",4096,1,4097);
        for (int i=1;i<=m_hist[0];i++){
                hist_mc->Fill(m_hist[i]);
        }

        TCanvas* mc_plot= new TCanvas("hist_Messung_raw 6","Batterie_raw",800,600);
        hist_mc->Draw();
        double p0=0.;
        double p1=1.;

        double* mc = read_mca("../Daten/Messung6_Batterie.mca");

        for (int i=1;i<=mc[0];i++){
                mc[i]=mc[i]-ml[i];
                if(mc[i]<0){mc[i]=0; }
        }

        double* mc_hist= tohist(mc);

        double lowerbound=p1+p0;

        double upperbound=4096*p1+p0+1;

        for(int i=1;i<=mc_hist[0];i++){
                mc_hist[i]=p0+p1*mc_hist[i];
        }
/*
        TF1 gaussians("gaussians","[0]*TMath::Gaus(x,[1],[2])+[3]*TMath::Gaus(x,[4],[5])+[6]*TMath::Gaus(x,[7],[8])",lowerbound,upperbound);
TF1 gaus("gaus","[0]*TMath::Gaus([1],[2])",lowerbound,upperbound);
        double norm1=100. ; double mean1=480. ; double sig1=10. ;
        double norm2=500. ; double mean2=520. ; double sig2=10. ;
        double norm3=100. ; double mean3=555. ; double sig3=1. ;
        double norm4=100. ; double mean4=575. ; double sig4=1. ;

        gaussians.SetParameters(norm1,mean1,sig1,norm2,mean2,sig2,norm3,mean3,sig3);
        gaus.SetParameters(norm4,mean4,sig4);
        gaussians.SetParNames("Norm1","Mean1","Sig1","Norm2","Mean2","Sig2","Norm3","Mean3","Sig3");;
        gaus.SetParNames("Norm4","Mean4","Sig4");
*/
        TH1D* hist_me=new TH1D("Messung 6_clean","E",4096,lowerbound,upperbound);
        for (int i=1;i<=mc_hist[0];i++){
                hist_me->Fill(mc_hist[i]);
        }

        TCanvas* me_plot=new TCanvas("hist_Messung_clean 6","Batterie_clean",800,600);
        hist_me->Draw();

 //       auto Fitres1 = hist_me->Fit(&gaussians,"S","q",450,566);
//      auto Fitres2 = hist_me->Fit(&gaus,"S","q",565,587);


//      gaussians.Draw("same");
}
